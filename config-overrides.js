const SassRuleRewire = require("react-app-rewire-sass-rule");
const path = require("path");
const rewireAliases = require("react-app-rewire-aliases");

module.exports = function override(config, env) {
  config = rewireAliases.aliasesOptions({
    "@src": path.resolve(__dirname, "src"),
    "@assets": path.resolve(__dirname, "src/@core/assets"),
    "@components": path.resolve(__dirname, "src/@core/components"),
    "@layouts": path.resolve(__dirname, "src/@core/layouts"),
    "@store": path.resolve(__dirname, "src/store"),
    "@styles": path.resolve(__dirname, "src/@core/scss"),
    "@configs": path.resolve(__dirname, "src/configs"),
    "@utils": path.resolve(__dirname, "src/utility/Utils"),
    "@hooks": path.resolve(__dirname, "src/utility/hooks"),
    "@selectors": path.resolve(__dirname, "src/utility/selectors"),
  })(config, env);

  config = new SassRuleRewire()
    .withRuleOptions({
      test: /\.s[ac]ss$/i,
      use: [
        {
          loader: "sass-loader",
          options: {
            sassOptions: {
              includePaths: ["node_modules", "src/assets"],
            },
          },
        },
      ],
    })
    .rewire(config, env);

  return config;
};
