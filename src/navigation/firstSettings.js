export default [
  {
    id: "welcome",
    title: "Welcome",
    navLink: "/first-settings/welcome",
    meta: {
      step: true,
    },
  },
  {
    id: "partners",
    title: "Partners",
    navLink: "/first-settings/partners",
    meta: {
      step: true,
    },
  },
  {
    id: "holidays",
    title: "Holidays",
    navLink: "/first-settings/holidays",
    meta: {
      step: true,
    },
  },
  {
    id: "vacations",
    title: "Vacations",
    navLink: "/first-settings/vacations",
    meta: {
      step: true,
    },
  },
  {
    id: "core-services",
    title: "Core Services",
    navLink: "/first-settings/core-services",
    meta: {
      step: true,
    },
  },
  {
    id: "special-services",
    title: "Special Services",
    navLink: "/first-settings/special-services",
    meta: {
      step: true,
    },
  },
  {
    id: "shifts",
    title: "Shifts",
    navLink: "/first-settings/shifts",
    meta: {
      step: true,
    },
  },
  {
    id: "special-shifts",
    title: "Special Shifts",
    navLink: "/first-settings/special-shifts",
    meta: {
      step: true,
    },
  },
];
