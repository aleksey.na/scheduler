import SVG from "@components/SVG";

export default [
  {
    id: "home",
    title: "Home",
    icon: <SVG.Home />,
    navLink: "/partner/home",
    children: [
        {
          id: "trades",
          title: "Trades",
          navLink: "/partner/home/trades",
        },
      ],
  },
  {
    id: "statistics",
    title: "Statistics",
    icon: <SVG.Statistics />,
    navLink: "/partner/statistics",
    children: [
        {
          id: "selected-shifts",
          title: "Selected shifts",
          navLink: "/partner/statistics/selected-shifts",
        },
      ],
  },
  {
    id: "wellness",
    title: "Wellness",
    icon: <SVG.Suitcase />,
    navLink: "/partner/wellness",
  },
  {
    id: "settings",
    title: "Settings",
    icon: <SVG.Settings />,
    navLink: "/partner/settings",
  },
];
