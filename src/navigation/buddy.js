import SVG from "@components/SVG";

export default [
  {
    id: "schedule",
    title: "Schedule",
    icon: <SVG.Calendar />,
    navLink: "/buddy/home",
    children: [
        {
          id: "trades",
          title: "Trades",
          navLink: "/buddy/home/trades",
        },
      ],
  },
  {
    id: "wellness",
    title: "Wellness",
    icon: <SVG.Suitcase />,
    navLink: "/buddy/wellness",
  },
  {
    id: "statistics",
    title: "Statistics",
    icon: <SVG.Statistics />,
    navLink: "/buddy/statistics",
    children: [
        {
          id: "selected-shifts",
          title: "Selected shifts",
          navLink: "/buddy/statistics/selected-shifts",
        },
      ],
  },
];
