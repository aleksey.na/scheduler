import SVG from "@components/SVG";

export default [
  {
    id: "home",
    title: "Home",
    icon: <SVG.Home />,
    navLink: "/scheduler/home",
  },
  {
    id: "trades",
    title: "Trades",
    icon: <SVG.Home />,
    navLink: "/scheduler/trades",
  },
  {
    id: "schedule",
    title: "Schedule",
    icon: <SVG.Calendar />,
    navLink: "/scheduler/schedule",
  },
  {
    id: "statistics",
    title: "Statistics",
    icon: <SVG.Statistics />,
    navLink: "/scheduler/statistics",
  },
  {
    id: "settings",
    title: "Settings",
    icon: <SVG.Settings />,
    navLink: "/scheduler/settings",
  },
];
