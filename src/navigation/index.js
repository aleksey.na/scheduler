import { getUserRole } from "@utils";

import firstSettings from "./firstSettings";
import scheduler from "./scheduler";
import partner from "./partner";
import buddy from "./buddy";

export default (function (role = getUserRole()) {
  switch (role) {
    case "first-settings":
      return firstSettings;

    case "scheduler":
      return scheduler;

    case "partner":
      return partner;

    case "buddy":
      return buddy;

    default:
      console.log("Default case in side-bar render");
      return [];
  }
})();
