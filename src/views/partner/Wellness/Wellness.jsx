import React, { useState } from "react";
import styles from "./wellness.module.scss";
import { Form, Accordion, Card } from "react-bootstrap";
import { useAccordionButton } from "react-bootstrap/AccordionButton";
import SelectInput from "@components/SelectInput";
import Textarea from "@components/Textarea";
import Button from "@components/Button";
import DatePickerInput from "@components/DatePickerInput";
import SVG from "@components/SVG";
import Input from "@components/Input";
import Modal from "@components/Modal";
import moment from "moment";

const Wellness = ({}) => {
  const [isOpenfilterModal, openFilterModal] = useState(false);
  const [activeItem, setActiveItem] = useState(false);

  //mock requests
  const requests = [
    {
      allowed: 1,
      name: "Vacation",
      start: moment(),
      finish: moment().add(7, "days"),
      duration: 7,
      id: 1,
    },
    {
      allowed: 0,
      name: "Days off",
      start: moment("2021-10-5"),
      finish: moment("2021-10-5").add(4, "days"),
      duration: 4,
      id: 2,
    },
    {
      name: "Wellness",
      start: moment("2021-09-20"),
      finish: moment("2021-09-20").add(2, "days"),
      duration: 2,
      id: 3,
    },
    {
      allowed: 0,
      name: "Days off",
      start: moment("2021-10-20"),
      finish: moment("2021-10-20").add(4, "days"),
      duration: 4,
      id: 4,
    },
  ];

  const options = [
    { label: "Vacation", value: "vacation" },
    { label: "Wellness", value: "wellness" },
    { label: "Parental leave", value: "parentalLeave" },
    { label: "Days off", value: "daysOff" },
  ];

  const filterOptions = [
    { name: "Status", items: ["Approved", "Pending Approval", "Rejected"] },
    { name: "Date", items: ["Descending", "Ascending"] },
    { name: "Duration", items: ["Descending", "Ascending"] },
    {
      name: "Name",
      items: ["Vacation", "Medical Leave", "Parental Leave", "Wellness"],
    },
  ];

  function CustomToggle({ children, eventKey }) {
    const [active, setActive] = useState(false);
    const decoratedOnClick = useAccordionButton(eventKey, () =>
      setActive(!active)
    );

    return (
      <div className={styles.toggleButton} onClick={decoratedOnClick}>
        <div className={active ? styles.closed : ""}>{children}</div>
        <SVG.ArrowDown
          fill={!active ? "#2054d7" : "#151b2b"}
          className={active ? styles.flipArrow : styles.arrow}
        />
      </div>
    );
  }

  return (
    <div className={styles.container}>
      <div className={styles.createContainer}>
        <div>
          <div className={styles.title}>Create personal request</div>
          <div>
            <Form>
              <div className={styles.daysContainer}>
                <span className={styles.label}>Name:</span>
                <div className={styles.selectInput}>
                  <SelectInput className={styles.input} options={options} />
                </div>
              </div>
              <div className={styles.daysTitle}>Choose days</div>
              <div className={styles.chooseDates}>
                <DatePickerInput />
              </div>
              <div className={`${styles.daysTitle} mb-1`}>Add comment</div>
              <Textarea
                name={"comments"}
                // onChange={}
                placeholder={"Type comments"}
                containerClass={styles.textArea}
                rows={9}
              />
            </Form>
          </div>
        </div>
        <div className={styles.requestButtons}>
          <Button className={styles.delete}>Delete</Button>
          <Button className={styles.add}>{!activeItem ? "Add" : "Edit"}</Button>
        </div>
      </div>
      <div className={styles.requestsContainer}>
        <div className={styles.requestsHeader}>
          <h4>Personal Requests</h4>
          <div className="d-flex align-items-center">
            <div className="mr-1">
              <Input
                name="search"
                placeholder="Search"
                icon={<SVG.Search />}
                inputWrapperClass={styles.inputWrapper}
                className={styles.searchInput}
                inputClass={styles.input}
                onChange={() => {}}
              />
            </div>
            <div
              className={styles.filterButton}
              onClick={() => openFilterModal(true)}
            >
              <SVG.Filters fill="#2054D7" />
            </div>
          </div>
        </div>
        <div className={styles.requestBody}>
          {requests.map((request) => (
            <div
              key={request.id}
              className={`${styles.card} ${
                activeItem === request.id ? styles.activeCard : ""
              } ${
                activeItem && activeItem !== request.id
                  ? styles.inactiveCard
                  : ""
              }`}
            >
              <div className="d-flex justify-content-between">
                <div
                  className={
                    "allowed" in request
                      ? !!request.allowed
                        ? styles.approve
                        : styles.reject
                      : styles.pending
                  }
                >
                  {"allowed" in request
                    ? !!request.allowed
                      ? "Approved"
                      : "Rejected"
                    : "Pending Approval"}
                </div>
                <div className="d-flex">
                  <div
                    className={styles.pencil}
                    onClick={() =>
                      activeItem === request.id
                        ? setActiveItem(null)
                        : setActiveItem(request.id)
                    }
                  >
                    <SVG.Pencil fill="#B3C1E3" />
                  </div>
                  <div className={styles.delete}>
                    <SVG.Bin fill="#B3C1E3" />
                  </div>
                </div>
              </div>
              <div className={styles.requestName}>{request.name}</div>
              <div
                className={`d-flex justify-content-between ${styles.durationInfo}`}
              >
                <div>
                  <div className={styles.lightText}>Start</div>
                  <div>{moment(request.start).format("MMM DD, YYYY")}</div>
                </div>
                <div>
                  <div className={styles.lightText}>Finish</div>
                  <div>{moment(request.finish).format("MMM DD, YYYY")}</div>
                </div>
                <div className={styles.duration}>
                  <div className={styles.lightText}>Duration</div>
                  <div>
                    {request.duration} {request.duration == 1 ? "day" : "days"}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Modal
        animation={false}
        show={isOpenfilterModal}
        onHide={openFilterModal}
        containerStyles={`${styles.modalFilters}`}
      >
        <div className={styles.modalHeader}>Filter</div>
        <div>
          {filterOptions.map((item, index) => (
            <Accordion defaultActiveKey={index} key={index}>
              <Card bsPrefix={styles.card}>
                <Card.Header bsPrefix={styles.filterHeading}>
                  <CustomToggle eventKey={index}>{item.name}</CustomToggle>
                </Card.Header>
                <Accordion.Collapse eventKey={index}>
                  <Card.Body bsPrefix={styles.filterBody}>
                    <div>
                      {item.items.map((filter, i) => (
                        <div key={i}>
                          <Form.Check
                            name={item.name}
                            id={i}
                            onChange={(e) =>
                              e.target.checked && console.log(e.target.value)
                            }
                          ></Form.Check>
                          <div>{filter}</div>
                        </div>
                      ))}
                    </div>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          ))}
        </div>
        <div className={styles.filterButtons}>
          <Button onClick={() => {}}>Show</Button>
          <div onClick={() => {}} className={styles.cancelButton}>
            Cancel
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default Wellness;
