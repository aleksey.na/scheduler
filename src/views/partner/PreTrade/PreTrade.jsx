import React, { useState } from "react";
import {
  Dropdown,
  OverlayTrigger,
  Popover,
  PopoverHeader,
  Modal,
} from "react-bootstrap";
import Button from "@components/Button";
import Avatar from "@components/avatar";
import SVG from "@components/SVG";
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";
import styles from "./pretrade.module.scss";
import moment from "moment";

function PreTrade() {
  const weekDays = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const items = [
    {
      date: moment("2021-10-04"),
      shifts: [
        {
          name: "Sh1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 1,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 2,
        },
      ],
    },
    {
      date: moment("2021-10-05"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 3,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 4,
        },
      ],
    },
    {
      date: moment("2021-10-06"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ],
    },
    {
      date: moment("2021-10-07"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 7,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 8,
        },
      ],
    },
    {
      date: moment("2021-10-08"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 9,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 10,
        },
      ],
    },
    {
      date: moment("2021-10-09"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 11,
        },
        {
          name: "Sh1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 12,
        },
      ],
    },
    {
      date: moment("2021-10-10"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 13,
        },
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 14,
        },
      ],
    },
    {
      date: moment("2021-10-11"),
      shifts: [
        {
          name: "Sh2",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 15,
        },
        {
          name: "Sh1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 16,
        },
      ],
    },
  ];
  const [yourShift, setYourShift] = useState(null);
  const [targetShift, setTargetShift] = useState(null);
  const [show, setShow] = useState(false);
  const [rejectShow, setRejectShow] = useState(false);

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={styles.customToggle}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <SVG.Calendar fill="#6B7DAA" />
        {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={styles.flipDown} />
    </div>
  ));

  const CustomSelectedDay = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={styles.selectedDay}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={styles.flipDown} />
    </div>
  ));

  const PopoverTitle = React.forwardRef((props, ref) => {
    const { name, color, person, start, finish, duration } = props.children;
    return (
      <div className={styles.shiftTooltip}>
        <div className="d-flex" style={{ alignItems: "center" }}>
          <div
            style={{ backgroundColor: color }}
            className={styles.verticalLine}
          ></div>
          <div className="mr-2">{name}</div>
        </div>
        <div className={styles.shiftTime}>
          <Avatar
            img={defaultAvatar}
            imgHeight="36"
            imgWidth="36"
            className={styles.shiftPopoverAvatar}
          />
          <div>
            <div>{person.name}</div>
            <div className={styles.time}>
              {start} - {finish} | {duration}
            </div>
          </div>
        </div>
      </div>
    );
  });

  const PopoverTitleInfo = React.forwardRef((props, ref) => {
    const [value, setValue] = useState("");

    return (
      <div>
        See list of all different Partners that the shift can be swapped with
      </div>
    );
  });

  return (
    <div className={styles.tradeContainer}>
      <div className={styles.tradeHeader}>
        <h4 className={styles.tradeHeading}>New trade</h4>
        <div>
          <button
            className={styles.tradeClose}
            onClick={() => setRejectShow(true)}
          >
            Close
          </button>
          <Button className={styles.tradeRequest} onClick={() => setShow(true)}>
            Send request
          </Button>
        </div>
      </div>
      <div className={styles.gridContainer}>
        <div className={`${styles.tradeCard} ${styles.wideCard}`}>
          <div className={styles.weekWrapper}>
            <h4 className={styles.tradeHeading}>Week {moment().format("W")}</h4>
            <Dropdown className={styles.dropdown} style={{ width: "280px" }}>
              <Dropdown.Toggle
                variant="success"
                id="dropdown-basic"
                as={CustomToggle}
              >
                {moment().format("D MMM YYYY")} -{" "}
                {moment().add(7, "days").format("D MMM YYYY")}
              </Dropdown.Toggle>
              {/* <Dropdown.Menu></Dropdown.Menu> */}
            </Dropdown>
          </div>
          <div className={styles.dayContainer}>
            {weekDays.map((day, index) => (
              <div className={styles.dayName} key={index}>{day}</div>
            ))}
            {items.map((item, index) => (
              <div
                key={index}
                className={`${styles.dayCell} ${
                  item.shifts.filter((item) => item.key === yourShift?.key)
                    .length && styles.yourShiftActive
                } ${
                  item.shifts.filter((item) => item.key === targetShift?.key)
                    .length && styles.targetShiftActive
                }`}
              >
                <div className={styles.date}>
                  {moment(item.date).format("D")}
                </div>
                {item.shifts.map((shift, index) => (
                  <OverlayTrigger
                    key={index}
                    trigger={["hover", "focus"]}
                    className="w-100"
                    overlay={
                      <Popover id={`popover-positioned-right`}>
                        <PopoverHeader as={PopoverTitle}>{shift}</PopoverHeader>
                      </Popover>
                    }
                  >
                    <div
                      className={styles.dateShift}
                      key={index}
                      onClick={() =>
                        setYourShift({ ...shift, date: item.date })
                      }
                    >
                      <Avatar
                        img={defaultAvatar}
                        imgHeight="20"
                        imgWidth="20"
                        className={styles.shiftAvatar}
                      />
                      <div>{shift.name}</div>
                    </div>
                  </OverlayTrigger>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div className={styles.tradeCard}>
          <div className={`${styles.tradeHeader} ${styles.yourShiftHeader}`}>
            <h4>Your Shift</h4>
            <div className={styles.selectWrapper}>
              <div>View: </div>
              <Dropdown
                className={`${styles.dropdown} ml-3`}
                style={{ width: "160px" }}
              >
                <Dropdown.Toggle
                  variant="success"
                  id="dropdown-basic"
                  as={CustomSelectedDay}
                  style={{ width: "160px" }}
                >
                  Selected day
                </Dropdown.Toggle>
                {/* <Dropdown.Menu></Dropdown.Menu> */}
              </Dropdown>
            </div>
          </div>
          {yourShift && (
            <div className={`${styles.yourShiftContainer} mt-1`}>
              <div className={styles.yourShiftName}>{yourShift.name}</div>
              <div className={styles.yourShiftDayWrapper}>
                <div>
                  <div style={{ fontSize: "14px" }}>
                    {moment(yourShift.date).format("dddd")}
                  </div>
                  <div style={{ whiteSpace: "nowrap" }}>
                    {moment(yourShift.date).format("DD MMMM, YYYY")}
                  </div>
                </div>
              </div>
              <div className={styles.yourShitUser}>
                <Avatar
                  img={defaultAvatar}
                  imgHeight="50"
                  imgWidth="50"
                  className={styles.avatar}
                />
                <div>
                  <div>{yourShift.person.name}</div>
                  <div style={{ fontSize: "14px" }}>
                    {yourShift.start} - {yourShift.finish} |{" "}
                    {yourShift.duration}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
        <div className={`${styles.tradeCard} pb-0`}>
          <div className={`${styles.tradeHeader} ${styles.yourShiftHeader}`}>
            <div className={styles.tradeInfo}>
              <h4>Target Shift</h4>
              <OverlayTrigger
                trigger={["hover", "focus"]}
                placement="top"
                overlay={
                  <Popover
                    id={`popover-positioned-top`}
                    className={styles.popover}
                    arrowProps={{ style: { borderBottom: 0 } }}
                  >
                    <PopoverHeader as={PopoverTitleInfo}></PopoverHeader>
                  </Popover>
                }
              >
                <div>i</div>
              </OverlayTrigger>
            </div>
            <div className={styles.selectWrapper}>
              <div>View: </div>
              <Dropdown
                className={`${styles.dropdown} ml-3`}
                style={{ width: "160px" }}
              >
                <Dropdown.Toggle
                  variant="success"
                  id="dropdown-basic"
                  as={CustomSelectedDay}
                  style={{ width: "160px" }}
                >
                  Sort by
                </Dropdown.Toggle>
                {/* <Dropdown.Menu></Dropdown.Menu> */}
              </Dropdown>
            </div>
          </div>
          <div className={styles.shiftLine}>
            {items.map((shift, index) =>
            <div key={index}>
              {shift.shifts.map((item, index) => (
                <div
                  key={index}
                  className={`${styles.yourShiftContainer} ${
                    item.key === targetShift?.key
                      ? styles.targetShiftContainer
                      : styles.shiftContainer
                  }`}
                  onClick={() => setTargetShift(item)}
                >
                  <div className={styles.yourShiftName}>{item.name}</div>
                  <div className={styles.yourShiftDayWrapper}>
                    <div>
                      <div
                        className={`${styles.dayName} ${
                          item.key === targetShift?.key
                            ? styles.activeDayName
                            : ""
                        }`}
                      >
                        {moment(item.date).format("dddd")}
                      </div>
                      <div className={styles.fullDate}>
                        {moment(item.date).format("DD MMMM, YYYY")}
                      </div>
                    </div>
                  </div>
                  <div className={styles.yourShitUser}>
                    <Avatar
                      img={defaultAvatar}
                      imgHeight="50"
                      imgWidth="50"
                      className={styles.avatar}
                    />
                    <div>
                      <div>{item.person.name}</div>
                      <div
                        className={`${styles.time} ${
                          item.key === targetShift?.key ? styles.activeTime : ""
                        }`}
                      >
                        {item.start} - {item.finish} | {item.duration}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
              </div>
            )}
          </div>
          <div className={styles.blur}></div>
        </div>
      </div>
      <Modal
        show={show}
        setShow={setShow}
        centered
        dialogClassName="rounded-lg"
      >
        <Modal.Body>
          <div className={styles.modalWrapper}>
            <div className={styles.close} onClick={() => setShow(false)}>
              <SVG.Close fill="#89D1D6" />
            </div>
            <div className={styles.modalHeader}>
              <div className={styles.modalTitle}>
                Scheduler accepted your trade
              </div>
              <div>Schedule will update to reflect shift is gone</div>
            </div>
            <div className={styles.modalBody}>
              {yourShift && (
                <>
                  <div className={styles.modalDate}>
                    <div>{moment(yourShift?.date).format("DD MMMM, YYYY")}</div>
                    <div>{moment(yourShift?.date).format("dddd")}</div>
                  </div>
                  <div className={`${styles.shiftTooltip} w-100 shadow-none`}>
                    <div className="d-flex" style={{ alignItems: "center" }}>
                      <div
                        style={{ backgroundColor: yourShift?.color }}
                        className={styles.verticalLine}
                      ></div>
                      <div className="mr-4">{yourShift?.name}</div>
                    </div>
                    <div className={styles.shiftTime}>
                      <Avatar
                        img={defaultAvatar}
                        imgHeight="50"
                        imgWidth="50"
                        className={styles.avatar}
                      />
                      <div>
                        <div>{yourShift?.person?.name}</div>
                        <div className={styles.time}>
                          {yourShift?.start} - {yourShift?.finish} |{" "}
                          {yourShift?.duration}
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
              <div className={styles.arrows}>
                <SVG.FlipArrows fill="#B3C1E3" />
              </div>
              {targetShift && (
                <>
                  <div className={styles.modalDate}>
                    <div>
                      {moment(targetShift?.date).format("DD MMMM, YYYY")}
                    </div>
                    <div>{moment(targetShift?.date).format("dddd")}</div>
                  </div>
                  <div className={`${styles.shiftTooltip} w-100 shadow-none`}>
                    <div className="d-flex" style={{ alignItems: "center" }}>
                      <div
                        style={{ backgroundColor: targetShift?.color }}
                        className={styles.verticalLine}
                      ></div>
                      <div className="mr-4">{targetShift?.name}</div>
                    </div>
                    <div className={styles.shiftTime}>
                      <Avatar
                        img={defaultAvatar}
                        imgHeight="50"
                        imgWidth="50"
                        className={styles.avatar}
                      />
                      <div>
                        <div>{targetShift?.person?.name}</div>
                        <div className={styles.time}>
                          {targetShift?.start} - {targetShift?.finish} |{" "}
                          {targetShift?.duration}
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
              <div className={styles.reasonWrapper}>
                <div>Reason:</div>
                <div>"Lorem ipsum"</div>
              </div>
            </div>
            <div className={styles.buttonOk}>
              <Button onClick={() => setShow(false)}>Okay</Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        show={rejectShow}
        setShow={setRejectShow}
        centered
        dialogClassName="rounded-lg"
      >
        <Modal.Body>
          <div
            className={`${styles.modalWrapper} ${styles.modalRejectWrapper}`}
          >
            <div className={styles.close} onClick={() => setRejectShow(false)}>
              <SVG.Close fill="#FFA3A3" />
            </div>
            <div className={styles.modalHeader}>
              <div className={styles.modalTitle}>
                Scheduler rejected your trade
              </div>
            </div>
            <div className={styles.modalBody}>
              {yourShift && (
                <>
                  <div className={`${styles.modalDate}`}>
                    <div>{moment(yourShift?.date).format("DD MMMM, YYYY")}</div>
                    <div>{moment(yourShift?.date).format("dddd")}</div>
                  </div>
                  <div className={`${styles.shiftTooltip} w-100 shadow-none`}>
                    <div className="d-flex" style={{ alignItems: "center" }}>
                      <div
                        style={{ backgroundColor: yourShift?.color }}
                        className={styles.verticalLine}
                      ></div>
                      <div className="mr-4">{yourShift?.name}</div>
                    </div>
                    <div className={styles.shiftTime}>
                      <Avatar
                        img={defaultAvatar}
                        imgHeight="36"
                        imgWidth="36"
                      />
                      <div>
                        <div>{yourShift?.person?.name}</div>
                        <div className={styles.time}>
                          {yourShift?.start} - {yourShift?.finish} |{" "}
                          {yourShift?.duration}
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
              <div className={styles.arrows}>
                <SVG.FlipArrows fill="#B3C1E3" />
              </div>
              {targetShift && (
                <>
                  <div className={styles.modalDate}>
                    <div>
                      {moment(targetShift?.date).format("DD MMMM, YYYY")}
                    </div>
                    <div>{moment(targetShift?.date).format("dddd")}</div>
                  </div>
                  <div className={`${styles.shiftTooltip} w-100 shadow-none`}>
                    <div className="d-flex align-items-center">
                      <div
                        style={{ backgroundColor: targetShift?.color }}
                        className={styles.verticalLine}
                      ></div>
                      <div className="mr-4">{targetShift?.name}</div>
                    </div>
                    <div className={styles.shiftTime}>
                      <Avatar
                        img={defaultAvatar}
                        imgHeight="36"
                        imgWidth="36"
                      />
                      <div>
                        <div>{targetShift?.person?.name}</div>
                        <div className={styles.time}>
                          {targetShift?.start} - {targetShift?.finish} |{" "}
                          {targetShift?.duration}
                        </div>
                      </div>
                    </div>
                  </div>
                </>
              )}
              <div className={styles.reasonWrapper}>
                <div>Reason:</div>
                <div>"Lorem ipsum"</div>
              </div>
            </div>
            <div className={styles.buttonOk}>
              <Button onClick={() => setRejectShow(false)}>Okay</Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default PreTrade;
