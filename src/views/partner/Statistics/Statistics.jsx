import React, { useState, useEffect } from "react";
import styles from "./statistics.module.scss";
import { Form } from "react-bootstrap";
import { PieChart, Pie, Cell } from "recharts";

const Statistics = ({}) => {
  //mock statistics
  const statistics = [
    {
      title: "Shift 1",
      hours: 150,
      total: 300,
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ]
    },
    {
      title: "Shift 2",
      hours: 241,
      total: 300,
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ]
    },
    {
      title: "Saturday 24",
      amount: 14,
      total: 20,
    },
    {
      title: "Sunday Shifts",
      amount: 4,
      total: 20,
    },
    {
      title: "Friday 24",
      amount: 11,
      total: 20,
    },
    {
      title: "Vacations",
      amount: 1,
      total: 3,
    },
    {
      title: "Wellness Days",
      amount: 3,
      total: 4,
    },
    {
      title: "Days Off",
      amount: 10,
      total: 14,
    },
  ];

  const filterOptions = [
    "All",
    "Shift 1",
    "Shift 2",
    "Special Shifts",
    "Days Off",
    "Vacations",
  ];

  const [filterValue, setFilterValue] = useState([]);
  const [filtered, setFiltered] = useState(statistics);

  useEffect(() => {
    filterValue.length ?
      setFiltered(
        statistics.filter((item) => filterValue.includes("All") ? statistics : filterValue.includes(item.title))
      ) : 
      setFiltered(statistics) 
  }, [filterValue]);


  return (
    <div className={styles.container}>
              <div className={styles.filterContainer}>
        <div className={styles.filterHeading}>Filter</div>
        <div className={styles.filterBody}>
          {filterOptions.map((option, index) => (
            <div key={index}>
              <Form.Check
                label={option}
                name="options"
                id={index}
                onChange={(e) =>
                  e.target.checked
                    ? setFilterValue([...filterValue, option])
                    : setFilterValue(
                        filterValue.filter((item) => item !== option)
                      )
                }
              ></Form.Check>
            </div>
          ))}
        </div>
      </div>
      <div
        className={styles.graphContainer}
      >
        {filtered?.map((item, index) => (
          <div className={styles.graphic} key={index}>
            <PieChart width={180} height={180}>
            <text x={90} y={70} textAnchor="middle" dominantBaseline="middle" className={styles.text}>
        	    {item.title}
            </text>
            <text x={90} y={105} textAnchor="middle" dominantBaseline="middle" className={styles.graphText}>
        	    {`${item.hours ? item.hours + "h" : item.amount + "/" + item.total}`}
            </text>
              <Pie
                data={[
                  {
                    name: item.title,
                    value: item?.hours || item?.amount,
                  },
                  {
                    name: item.title,
                    value: item?.hours
                      ? item.total - item.hours
                      : item.total - item.amount,
                  },
                ]}
                cx={85}
                cy={85}
                // renderLabelItem={renderCustomizedLabel}
                innerRadius={60}
                outerRadius={80}
                cornerRadius={40}
                fill="#fff"
                dataKey="value"
                startAngle={-270}
              >
              <text x={100} y={100} textAnchor="middle" dominantBaseline="middle">
                    Donut
              </text>
                <Cell key={`cell-${index}`} fill="#F0319D" />
              </Pie>
            </PieChart>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Statistics;
