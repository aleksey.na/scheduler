import React from "react";
import styles from "./partner.module.scss";
import { Row, Table } from "react-bootstrap";
import CalendarTable from "@components/CalendarTable/CalendarTable";
import moment from "moment";
import Avatar from "@components/avatar";
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";

function Home() {
  const shifts = [
    {
      name: "Shift 1",
      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12h",
      color: "#6045FF",
      person: { name: "Patty Partner" },
      id: 1,
    },
    {
      name: "Shift 2",
      start: "8:00 PM",
      finish: "8:00 AM",
      duration: "12h",
      color: "#45C9E4",
      person: { name: "Fred Partner" },
      id: 2,
    },
    {
      name: "Special shift",
      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12h",
      color: "#F0319D",
      person: { name: "James Partner" },
      id: 3,
    },
    { name: "Day off", color: "#B3C1E3", id: 4 },
  ];


  return (
    <div className={styles.gridContainer}>
    <div>
      <ShiftCard shifts={shifts} />
      <MembersCard shifts={shifts} />
    </div>
    <div className={`${styles.card} ml-4`}>
      <CalendarTable shifts={shifts} />
    </div>
  </div>
  );
}

const ShiftCard = ({ shifts }) => {
  return (
    <Row className={`${styles.card} ${styles.shadowCard}`}>
      <div style={{ fontSize: "18px" }}>Legend</div>
      <Table className={styles.table}>
        <thead>
          <tr className={styles.tableHead}>
            <th style={{ paddingLeft: 0 }}>Shift</th>
            <th colSpan={2} style={{ textAlign: "center" }}>
              Work time
            </th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody>
          {shifts.map((shift) => (
            <tr className={styles.tableRow} key={shift.id}>
              <td>
                <span
                  style={{ background: shift.color }}
                  className={styles.shiftColor}
                ></span>
                {shift.name}
              </td>
              <td>
                <div className={styles.smallText}>Start</div>
                {shift?.start ? shift?.start : "-"}
              </td>
              <td>
                <div className={styles.smallText}>Finish</div>
                {shift?.finish ? shift?.finish : "-"}
              </td>
              <td>
                <div className={styles.smallText}>Duration</div>
                {shift?.duration ? shift.duration : "-"}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Row>
  );
};

const MembersCard = ({ shifts }) => {
  const today = new Date();
  return (
    <Row className={`${styles.card} ${styles.shadowCard}`}>
      <div className={styles.peopleHeader}>
        <div className={styles.day}>{moment(today).format("dddd")}</div>
        <div>{moment(today).format("DD MMMM, YYYY")}</div>
      </div>
      <div className={styles.peopleWrapper}>
        {shifts?.length &&
          shifts.map(
            (shift) =>
              shift.start && (
                <div className={styles.peopleRow} key={shift.id}>
                  <div
                    style={{
                      display: "inline-flex",
                      alignItems: "center",
                      width: "90px",
                    }}
                  >
                    <span
                      style={{ background: shift.color }}
                      className={styles.shiftColor}
                    ></span>
                    {shift.name}
                    <div></div>
                  </div>
                  <div className={styles.personWrapper}>
                    <Avatar
                      img={defaultAvatar}
                      imgHeight="36"
                      imgWidth="36"
                      className={styles.avatar}
                    />
                    <div>
                      {shift.person.name}
                      <br />
                      <span>
                        {shift.start} - {shift.finish} | ({shift.duration})
                      </span>
                    </div>
                  </div>
                </div>
              )
          )}
      </div>
    </Row>
  );
};

export default Home;
