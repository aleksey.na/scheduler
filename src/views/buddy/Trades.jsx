import React from "react";
import styles from "./buddy.module.scss";
import { Row, Table } from "react-bootstrap";
import CalendarTable from "@components/CalendarTable/CalendarTable";
import moment from "moment";

function Trades() {
  const shifts = [
    {
      name: "Shift 1",
      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12h",
      color: "#6045FF",
      person: { name: "Patty Partner" },
      tradeAccepted: true,
    },
    {
      name: "Shift 2",
      start: "8:00 PM",
      finish: "8:00 AM",
      duration: "12h",
      color: "#45C9E4",
      person: { name: "Fred Partner" },
      tradeAccepted: "pending"
    },
    {
      name: "Special shift",
      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12h",
      color: "#F0319D",
      person: { name: "James Partner" },
    },
    { name: "Day off", color: "#B3C1E3" },
  ];


  return (
    <div className={styles.gridContainer}>
      <div>
        <ShiftCard shifts={shifts} />
      </div>
      <div className={`${styles.card} ml-4`}>
        <CalendarTable shifts={shifts} isTradeButton={false}/>
      </div>
  </div>
  );
}

const ShiftCard = ({ shifts }) => {
  return (
    <Row className={`${styles.card} ${styles.shadowCard}`}>
      <div style={{ fontSize: "18px" }}>Legend</div>
      <Table className={styles.table}>
        <thead>
          <tr className={styles.tableHead}>
            <th className="p-l-0">Shift</th>
            <th colSpan={2} className="text-center">
              Work time
            </th>
            <th>Duration</th>
          </tr>
        </thead>
        <tbody>
          {shifts.map((shift) => (
            <tr className={styles.tableRow}>
              <td>
                <span
                  style={{ background: shift.color }}
                  className={styles.shiftColor}
                ></span>
                {shift.name}
              </td>
              <td>
                <div className={styles.smallText}>Start</div>
                {shift?.start ? shift?.start : "-"}
              </td>
              <td>
                <div className={styles.smallText}>Finish</div>
                {shift?.finish ? shift?.finish : "-"}
              </td>
              <td>
                <div className={styles.smallText}>Duration</div>
                {shift?.duration ? shift.duration : "-"}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Row>
  );
};

export default Trades;
