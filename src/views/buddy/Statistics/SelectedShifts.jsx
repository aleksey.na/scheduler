import React, { useState, useEffect } from "react";
import styles from "./statistics.module.scss";
import { Form } from "react-bootstrap";
import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from "recharts";
import moment from "moment";
import Avatar from '@components/avatar';
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";

const SelectedShifts = ({}) => {
  //mock statistics
  const statistics = [
    {
      title: "Shift 1",
      hours: 150,
      total: 300,
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ]
    },
    {
      title: "Shift 2",
      hours: 241,
      total: 300,
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ]
    },
    {
      title: "Saturday 24",
      amount: 14,
      total: 20,
    },
    {
      title: "Sunday Shifts",
      amount: 4,
      total: 20,
    },
    {
      title: "Friday 24",
      amount: 11,
      total: 20,
    },
    {
      title: "Vacations",
      amount: 1,
      total: 3,
    },
    {
      title: "Wellness Days",
      amount: 3,
      total: 4,
    },
    {
      title: "Days Off",
      amount: 10,
      total: 14,
    },
  ];

  const filterOptions = [
    "All",
    "Shift 1",
    "Shift 2",
    "Special Shifts",
    "Days Off",
    "Vacations",
  ];

  const [filterValue, setFilterValue] = useState([]);
  const [filtered, setFiltered] = useState(statistics);

  useEffect(() => {
    filterValue.length ?
      setFiltered(
        statistics.filter((item) => filterValue.includes("All") ? statistics : filterValue.includes(item.title))
      ) : 
      setFiltered(statistics) 
  }, [filterValue]);


  return (
    <div className={styles.container}>
      <div className={styles.filterContainer}>
        <div className={styles.filterHeading}>Filter</div>
        <div className={styles.filterBody}>
          {filterOptions.map((option, index) => (
            <div key={index}> 
              <Form.Check
                label={option}
                name="options"
                id={index}
                onChange={(e) =>
                  e.target.checked
                    ? setFilterValue([...filterValue, option])
                    : setFilterValue(
                        filterValue.filter((item) => item !== option)
                      )
                }
              ></Form.Check>
            </div>
          ))}
        </div>
      </div>
      <div className={`${styles.filterContainer} ${styles.shiftList}`}>
        <div>
          <div className={styles.filterHeading}>{filterValue.length ? filterValue?.join(", ") : filterOptions[0]}</div>
        </div>
        <div className={styles.filterBody}>
          {filtered.map((option) => (
            option?.shifts && option?.shifts?.map((shift, index) => 
              <div className={styles.shift} key={index}>
                <div className={styles.date}>
                  <div>{moment(shift.date).format("D MMMM, YYYY")}</div>
                  <div>{moment(shift.date).format("dddd")}</div>
                </div>
                <div className={styles.avatar}>
                  <Avatar img={defaultAvatar} imgHeight="50" imgWidth="50" className={styles.photo}/>
                  <div>
                    <div>{shift.person.name}</div>
                    <div style={{fontSize: "14px"}}>Level 2</div>
                  </div>
                </div>
                <div className={styles.verticalLine}></div>
                <div className={styles.shiftStart}>
                  <div>Start</div>
                  <div className={styles.shiftTime}>{shift.start}</div>
                </div>
                <div>
                  <div>Finish</div>
                  <div className={styles.shiftTime}>{shift.finish}</div>
                </div>
                <div className={styles.verticalLine}></div>
                <div>
                  <div>Duration</div>
                  <div className={styles.shiftTime}>{shift.duration}</div>
                </div>
              </div>
              )
          ))}
        </div>
      </div>
    </div>
  );
};

export default SelectedShifts;
