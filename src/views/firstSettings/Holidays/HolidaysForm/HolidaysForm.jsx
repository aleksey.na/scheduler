import React from "react";

import Input from "@components/Input";
import SelectInput from "@components/SelectInput";
import Form from "@components/Form";

import styles from "./holidaysForm.module.scss";

const HolidaysForm = ({
  state,
  handleInputChange,
  typeOptions,
  excludeOptions,
  deleteHolidayHandler,
  saveHolidayHandler,
  isDeleteDisabled,
}) => {
  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      deleteHandler={deleteHolidayHandler}
      saveHandler={saveHolidayHandler}
      isDeleteDisabled={isDeleteDisabled}
    >
      <Input
        name="holidayName"
        label={"Holiday Name"}
        value={state.holidayName}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <Input
        name="holidayDate"
        label={"Holiday Date"}
        value={state.holidayDate}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        name="holidayType"
        label={"Type"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={typeOptions}
        value={state.holidayType}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        name="holidayExclusion"
        label={"Exclude Special Services"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={excludeOptions}
        value={state.holidayExclusion}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
    </Form>
  );
};

export default HolidaysForm;
