import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";

import holidaysReducer from "@store/entities/Holidays/holidays.reducer";

import DataItem from "@components/DataItem";

import {
  selectorHolidays,
  selectorMajorHolidays,
  selectorMinorHolidays,
} from "@selectors/holidaysSelector";

import Form from "./HolidaysForm/HolidaysForm";

import styles from "./holidays.module.scss";
import common from "../common.module.scss";

const {
  fetchHolidaysRequest,
  saveHolidayRequest,
  updateHolidayRequest,
  deleteHolidayRequest,
} = holidaysReducer.actions;

const Holidays = () => {
  const dispatch = useDispatch();

  const initialState = {
    holidayName: "",
    holidayDate: "",
    holidayType: "",
    holidayExclusion: "",
  };
  const [state, setState] = useState({
    ...initialState,
  });
  const [selectedHolidayId, setSelectedHolidayId] = useState("");

  const majorHolidays = useSelector(selectorMajorHolidays);
  const minorHolidays = useSelector(selectorMinorHolidays);
  const holidays = useSelector(selectorHolidays);

  useEffect(() => {
    if (holidays.length === 0) dispatch(fetchHolidaysRequest());
  }, []);

  const handleInputChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
  };

  const deleteHolidayHandler = () => {
    const holidaysList = holidays.filter((p) => p.id !== selectedHolidayId);
    dispatch(deleteHolidayRequest(holidaysList));
    setSelectedHolidayId(false);
    resetForm();
  };

  const saveHolidayHandler = () => {
    const newList = [...holidays];
    let clickedHoliday = newList.find((h) => h.id === selectedHolidayId);

    if (clickedHoliday) {
      const index = newList.indexOf(clickedHoliday);
      clickedHoliday = { id: clickedHoliday.id, ...state };
      newList.splice(index, 1, clickedHoliday);

      dispatch(updateHolidayRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(saveHolidayRequest(newList));
    }

    setSelectedHolidayId(false);
    resetForm();
  };

  const selectHolidayHandler = (e, id) => {
    e.stopPropagation();
    const clickedHoliday = holidays.find((h) => h.id === id);
    setState({
      ...clickedHoliday,
    });
    setSelectedHolidayId(id);
  };

  const holidayTypes = {
    Major: "Major",
    Minor: "Minor",
  };
  const typeOptions = Object.entries(holidayTypes).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  const excludeTypes = {
    Yes: "Yes",
    No: "No",
  };
  const excludeOptions = Object.entries(excludeTypes).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  // const setclearFormHandler = () => {
  //   setState({
  //     ...initialState,
  //   });
  //   setSelectedHolidayId(false);
  // };

  // const domNode = useClickOutside(() => setclearFormHandler(), formRef);

  return (
    <div className={styles.holidaysWrapper}>
      <Form
        state={state}
        handleInputChange={handleInputChange}
        typeOptions={typeOptions}
        excludeOptions={excludeOptions}
        deleteHolidayHandler={deleteHolidayHandler}
        saveHolidayHandler={saveHolidayHandler}
        isDeleteDisabled={!selectedHolidayId}
      />
      <div className={common.dataContainer}>
        {majorHolidays.length > 0 || minorHolidays.length > 0 ? (
          <div className={styles.typesContainer}>
            {majorHolidays.length > 0 && (
              <div className={styles.holidaysType}>
                <div className={styles.typeTitle}>Major</div>
                {majorHolidays.map((holiday) => (
                  <DataItem
                    key={holiday.id}
                    id={holiday.id}
                    onClickHandler={(e) => selectHolidayHandler(e, holiday.id)}
                    containerClass={styles.holidayContainer}
                    isActive={selectedHolidayId === holiday.id}
                    isBlured={
                      selectedHolidayId && selectedHolidayId !== holiday.id
                    }
                  >
                    <div className={common.section}>
                      <div
                        className={`${common.subtitle} ${styles.nameSubtitle}`}
                      >
                        Holiday
                      </div>
                      <div className={common.itemName}>
                        {holiday.holidayName}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Day</div>
                      <div className={styles.sectionData}>
                        {moment(holiday.holidayDate).format("DD/MM/YYYY")}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Spesial services</div>
                      <div className={styles.sectionData}>
                        {holiday.holidayExclusion}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Type</div>
                      <div
                        className={`${styles.sectionData} ${styles.majorType}`}
                      >
                        {holiday.holidayType}
                      </div>
                    </div>
                  </DataItem>
                ))}
              </div>
            )}

            {minorHolidays.length > 0 && (
              <div className={styles.holidaysType}>
                <div className={styles.typeTitle}>Minor</div>
                {minorHolidays.map((holiday) => (
                  <DataItem
                    key={holiday.id}
                    id={holiday.id}
                    onClickHandler={(e) => selectHolidayHandler(e, holiday.id)}
                    containerClass={styles.holidayContainer}
                    isActive={selectedHolidayId === holiday.id}
                    isBlured={
                      selectedHolidayId && selectedHolidayId !== holiday.id
                    }
                  >
                    <div className={common.section}>
                      <div
                        className={`${common.subtitle} ${styles.nameSubtitle}`}
                      >
                        Holiday
                      </div>
                      <div className={common.itemName}>
                        {holiday.holidayName}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Day</div>
                      <div className={styles.sectionData}>
                        {moment(holiday.holidayDate).format("DD/MM/YYYY")}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Spesial services</div>
                      <div className={styles.sectionData}>
                        {holiday.holidayExclusion}
                      </div>
                    </div>

                    <div className={common.section}>
                      <div className={common.subtitle}>Type</div>
                      <div
                        className={`${styles.sectionData} ${styles.minorType}`}
                      >
                        {holiday.holidayType}
                      </div>
                    </div>
                  </DataItem>
                ))}
              </div>
            )}
          </div>
        ) : (
          <div className={common.noData}>
            <div>No avaliable holidays</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Holidays;
