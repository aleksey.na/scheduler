import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// ** Utils
import { nextLinkHandler } from "@utils";

// ** Selectors
import {
  firstSettingsMenuStepSelector,
  firstSettingsMenuLinksSelector,
} from "@selectors/layoutSelectors";

import Button from "@components/Button/Button";

import styles from "./welcome.module.scss";

import calendar from "@src/assets/images/firstSettings/welcome-screen.png";

const Welcome = () => {
  // ** Store Vars
  const history = useHistory();
  const currentLink = useSelector(firstSettingsMenuStepSelector);
  const menuLinks = useSelector(firstSettingsMenuLinksSelector);

  const nextStepHandler = () => {
    nextLinkHandler(history, currentLink, menuLinks);
  };

  return (
    <div className={styles.welcomeContainer}>
      <div className={styles.imgWrapper}>
        <img src={calendar} alt="calendar" />
      </div>
      <div className={styles.textWrapper}>
        <div className={styles.title}>
          Schedule maker will create a schedule automatically for you!
        </div>
        <div className={styles.subtitle}>
          Add Partners, shift requirements, block requirements and the schedule
          maker will generate schedules every month!
        </div>
        <Button
          onClick={nextStepHandler}
          className={styles.btn}
          disabled={false}
        >
          Let’s start!
        </Button>
      </div>
    </div>
  );
};

export default Welcome;
