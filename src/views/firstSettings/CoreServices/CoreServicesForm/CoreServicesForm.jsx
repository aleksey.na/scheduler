import React from "react";

import Input from "@components/Input";
import Form from "@components/Form";

import styles from "./coreServicesForm.module.scss";

const CoreServicesForm = ({
  state,
  handleInputChange,
  deleteSecviceHandler,
  saveSecviceHandler,
  isSaveDisabled,
  isDeleteDisabled,
}) => {
  const { levels, name, number } = state;
  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      saveHandler={saveSecviceHandler}
      deleteHandler={deleteSecviceHandler}
      isSaveDisabled={isSaveDisabled}
      isDeleteDisabled={isDeleteDisabled}
    >
      <div className={styles.block}>
        <Input
          name={"name"}
          label={"Name of the service"}
          value={name}
          // containerClass={styles.inputBlock}
          onChange={handleInputChange("")}
        />
        <Input
          name={"number"}
          label={"Number of Services "}
          value={number}
          // containerClass={styles.inputBlock}
          onChange={handleInputChange("")}
        />
      </div>

      {levels.map((_, index) => {
        const levelNumber = index + 1;
        return (
          <div key={index} className={`${styles.block} ${styles.columnBlock}`}>
            <div className={styles.titleContainer}>
              <div>{`Level ${levelNumber}`}</div>
              <div>Partners</div>
            </div>
            <div className={styles.inputGroup}>
              <Input
                labelType={"label-secondary"}
                type={"number"}
                name={"minimum"}
                label={"Minimum"}
                value={levels[index]?.minimum}
                containerClass={styles.levelInput}
                onChange={handleInputChange(index)}
              />
              <Input
                labelType={"label-secondary"}
                type={"number"}
                name={"maximum"}
                label={"Maximum"}
                value={levels[index]?.maximum}
                containerClass={styles.levelInput}
                onChange={handleInputChange(index)}
              />
              <Input
                labelType={"label-secondary"}
                type={"number"}
                name={"rotation"}
                label={"Min rotations"}
                value={levels[index]?.rotation}
                containerClass={styles.levelInput}
                onChange={handleInputChange(index)}
              />
            </div>
          </div>
        );
      })}
    </Form>
  );
};

export default CoreServicesForm;
