import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import servicesReducer from "@store/entities/Services/services.reducer";
import { selectorServices } from "@selectors/servicesSelectors";

import Form from "./CoreServicesForm/CoreServicesForm";
import DataItem from "@components/DataItem";

import styles from "./coreServices.module.scss";
import common from "../common.module.scss";

const {
  fetchServicesRequest,
  saveServiceRequest,
  updateServiceRequest,
  deleteServiceRequest,
} = servicesReducer.actions;

const CoreServices = () => {
  const dispatch = useDispatch();
  const levelInitialState = {
    minimum: "",
    maximum: "",
    rotation: "",
  };
  const initialState = {
    name: "",
    number: "",
    levels: Array(4).fill(levelInitialState),
  };
  const [state, setState] = useState({
    ...initialState,
  });

  const [selectedServiceId, setSelectedServiceId] = useState("");
  const services = useSelector(selectorServices);

  useEffect(() => {
    if (services.length === 0) dispatch(fetchServicesRequest());
  }, []);

  const handleInputChange = (level) => (name, value) => {
    if (typeof level === "number") {
      const newArray = [...state.levels];
      const targetLevel = { ...newArray[level] };
      targetLevel[name] = value;
      newArray.splice(level, 1, targetLevel);

      setState({
        ...state,
        levels: newArray,
      });
      return;
    }

    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
  };

  const selectServiceHandler = (e, id) => {
    e.stopPropagation();
    const clickedVacation = services.find((v) => v.id === id);

    setState({
      ...clickedVacation,
    });
    setSelectedServiceId(id);
  };

  const deleteSecviceHandler = () => {
    const servicesList = services.filter((p) => p.id !== selectedServiceId);
    dispatch(deleteServiceRequest(servicesList));
    setSelectedServiceId(false);
    resetForm();
  };

  const saveServiceHandler = () => {
    const newList = [...services];
    let clickedService = newList.find((h) => h.id === selectedServiceId);

    if (clickedService) {
      const index = newList.indexOf(clickedService);
      clickedService = { id: clickedService.id, ...state };
      newList.splice(index, 1, clickedService);

      dispatch(updateServiceRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(saveServiceRequest(newList));
    }

    setSelectedServiceId(false);
    resetForm();
  };

  return (
    <div className={common.contentWrapper}>
      <Form
        state={state}
        handleInputChange={handleInputChange}
        deleteSecviceHandler={deleteSecviceHandler}
        saveSecviceHandler={saveServiceHandler}
        isDeleteDisabled={!selectedServiceId}
      />
      <div
        className={`${common.dataContainer} ${
          services.length === 0 ? common.emptyDataContainer : ""
        }`}
      >
        {services.length > 0 ? (
          services.map((service) => (
            <DataItem
              key={service.id}
              containerClass={styles.dataItem}
              onClickHandler={selectServiceHandler}
              id={service.id}
              isActive={service.id === selectedServiceId}
              isBlured={selectedServiceId && service.id !== selectedServiceId}
            >
              <div className={`${common.section} ${styles.section}`}>
                <div className={`${common.subtitle} ${common.nameSubtitle}`}>
                  Service
                </div>
                <div className={common.itemName}>{service.name}</div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={common.subtitle}>Level 1</div>
                <div className={common.secSection}>
                  {service.levels[0].maximum > 0
                    ? `${service.levels[0].minimum}-${service.levels[0].maximum}`
                    : service.levels[0].maximum}
                </div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={common.subtitle}>Level 2</div>
                <div className={common.thSection}>
                  {service.levels[1].maximum > 0
                    ? `${service.levels[1].minimum}-${service.levels[1].maximum}`
                    : service.levels[1].maximum}
                </div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={common.subtitle}>Level 3</div>
                <div className={common.thSection}>
                  {service.levels[2].maximum > 0
                    ? `${service.levels[2].minimum}-${service.levels[2].maximum}`
                    : service.levels[2].maximum}
                </div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={common.subtitle}>Level 4</div>
                <div className={common.thSection}>
                  {service.levels[3].maximum > 0
                    ? `${service.levels[3].minimum}-${service.levels[3].maximum}`
                    : service.levels[3].maximum}
                </div>
              </div>
            </DataItem>
          ))
        ) : (
          <div className={common.noData}>
            <div>No avaliable core services</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default CoreServices;
