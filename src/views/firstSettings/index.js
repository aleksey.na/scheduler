import Welcome from "./Welcome/Welcome";
import Partners from "./Partners/Partners";
import Holidays from "./Holidays/Holidays";
import Vacations from "./Vacations/Vacations";
import CoreServices from "./CoreServices/CoreServices";
import SpecialServices from "./SpecialServices/SpecialServices";
import Shifts from "./Shifts/Shifts";
import SpecialShifts from "./SpecialShifts/SpecialShifts";

export {
  Welcome,
  Partners,
  Holidays,
  Vacations,
  CoreServices,
  SpecialServices,
  Shifts,
  SpecialShifts,
};
