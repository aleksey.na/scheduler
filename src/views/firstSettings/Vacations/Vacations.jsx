import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import vacationsReducer from "@store/entities/Vacations/vacations.reducer";
import { selectorVacations } from "@selectors/vacationsSelectors";

import Form from "./VacationsForm/VacationsForm";
import DataItem from "@components/DataItem";

import styles from "./vacations.module.scss";
import common from "../common.module.scss";

const {
  fetchVacationsRequest,
  saveVacationRequest,
  updateVacationRequest,
  deleteVacationRequest,
} = vacationsReducer.actions;

const Vacations = () => {
  const dispatch = useDispatch();
  const initialState = {
    name: "",
    type: "",
    duration: "",
    consecutive: "",
    allowed: "",
  };
  const [state, setState] = useState({
    ...initialState,
  });
  const [selectedVacationId, setSelectedVacationId] = useState("");
  const vacations = useSelector(selectorVacations);

  useEffect(() => {
    if (vacations.length === 0) dispatch(fetchVacationsRequest());
  }, []);

  const handleInputChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
  };

  const selectVacationHandler = (e, id) => {
    e.stopPropagation();
    const clickedVacation = vacations.find((v) => v.id === id);
    setState({
      ...clickedVacation,
    });
    setSelectedVacationId(id);
  };

  const deleteVacationHandler = () => {
    const vacationsList = vacations.filter((p) => p.id !== selectedVacationId);
    dispatch(deleteVacationRequest(vacationsList));
    setSelectedVacationId(false);
    resetForm();
  };

  const saveVacationHandler = () => {
    const newList = [...vacations];
    let clickedVacation = newList.find((h) => h.id === selectedVacationId);

    if (clickedVacation) {
      const index = newList.indexOf(clickedVacation);
      clickedVacation = { id: clickedVacation.id, ...state };
      newList.splice(index, 1, clickedVacation);

      dispatch(updateVacationRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(saveVacationRequest(newList));
    }

    setSelectedVacationId(false);
    resetForm();
  };

  const types = {
    Day: "Day",
    Week: "Week",
    Month: "Month",
  };
  const typesOptions = Object.entries(types).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  const durationOptions = new Array(31).fill("").map((_, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  const consecutiveTypes = {
    Yes: "Yes",
    No: "No",
  };
  const consecutiveOptions = Object.entries(consecutiveTypes).map(
    ([key, value]) => ({
      value: key,
      label: value,
    })
  );

  const allowedOptions = new Array(31).fill("").map((level, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  return (
    <div className={common.contentWrapper}>
      <Form
        state={state}
        handleInputChange={handleInputChange}
        deleteVacationHandler={deleteVacationHandler}
        saveVacationHandler={saveVacationHandler}
        typesOptions={typesOptions}
        durationOptions={durationOptions}
        consecutiveOptions={consecutiveOptions}
        allowedOptions={allowedOptions}
        isDeleteDisabled={!selectedVacationId}
      />
      <div
        className={`${common.dataContainer} ${
          vacations.length === 0 ? common.emptyDataContainer : ""
        }`}
      >
        {vacations.length > 0 ? (
          vacations.map((vacation) => (
            <DataItem
              key={vacation.id}
              onClickHandler={selectVacationHandler}
              id={vacation.id}
              isActive={vacation.id === selectedVacationId}
              isBlured={
                selectedVacationId && vacation.id !== selectedVacationId
              }
            >
              <div className={common.section}>
                <div className={`${common.subtitle} ${common.nameSubtitle}`}>
                  Item
                </div>
                <div className={common.itemName}>{vacation.name}</div>
              </div>

              <div className={`${common.section} ${styles.secSection}`}>
                <div className={common.subtitle}>Consecutive</div>
                <div className={common.secSection}>
                  {`${vacation.duration} ${
                    vacation.consecutive === "Yes" ? "consecutive" : ""
                  } ${
                    vacation.consecutive === "Yes"
                      ? vacation.type + "s"
                      : vacation.type + " only"
                  } `}
                </div>
              </div>

              <div className={common.section}>
                <div className={common.subtitle}>Spesial services</div>
                <div className={common.thSection}>
                  {`${vacation.allowed} allowed`}
                </div>
              </div>
            </DataItem>
          ))
        ) : (
          <div className={common.noData}>
            <div>No avaliable personal items</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Vacations;
