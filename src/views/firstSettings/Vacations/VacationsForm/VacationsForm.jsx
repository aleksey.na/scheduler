import React from "react";

import Input from "@components/Input";
import SelectInput from "@components/SelectInput";
import Form from "@components/Form";

import styles from "./vacationsForm.module.scss";

const VacationsForm = ({
  state,
  handleInputChange,
  deleteVacationHandler,
  saveVacationHandler,
  isSaveDisabled,
  isDeleteDisabled,
  typesOptions,
  durationOptions,
  consecutiveOptions,
  allowedOptions,
}) => {
  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      saveHandler={saveVacationHandler}
      deleteHandler={deleteVacationHandler}
      isSaveDisabled={isSaveDisabled}
      isDeleteDisabled={isDeleteDisabled}
    >
      <Input
        name={"name"}
        label={"Item name"}
        value={state.name}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        view={"row"}
        name={"type"}
        label={"Type"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={typesOptions}
        value={state.type}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        view={"row"}
        name={"duration"}
        label={"Duration"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={durationOptions}
        value={state.duration}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        view={"row"}
        name={"consecutive"}
        label={"Consecutive"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={consecutiveOptions}
        value={state.consecutive}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
      <SelectInput
        view={"row"}
        name={"allowed"}
        label={"Allowed"}
        labelDescription={
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
        }
        options={allowedOptions}
        value={state.allowed}
        containerClass={styles.inputBlock}
        onChange={handleInputChange}
      />
    </Form>
  );
};

export default VacationsForm;
