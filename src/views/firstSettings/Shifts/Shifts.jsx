import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

// Third party libraries
import moment from "moment";

// ** Reducers & Store
import shiftsReducer from "@store/entities/Shifts/shifts.reducer";
import { selectorShifts } from "@selectors/shiftsSelectors";

// ** Components
import Form from "./ShiftsForm/ShiftsForm";
import DataItem from "@components/DataItem";

// ** Styles
import styles from "./shifts.module.scss";
import common from "../common.module.scss";

const {
  fetchShiftsRequest,
  updateShiftRequest,
  saveShiftRequest,
  deleteShiftRequest,
} = shiftsReducer.actions;

function Shifts() {
  const dispatch = useDispatch();
  const maxHoursNumber = 12;

  const initialState = {
    name: "",
    duration: "",
    // start: "2021-11-04T15:07:05.879Z",
    // finish: "2021-11-04T23:11:05.879Z",
    start: "",
    finish: "",
    comments: "",
  };
  const [state, setState] = useState({
    ...initialState,
  });
  const [selectedShiftId, setSelectedShiftId] = useState("");
  const shifts = useSelector(selectorShifts);

  useEffect(() => {
    if (shifts.length === 0) dispatch(fetchShiftsRequest());
  }, []);

  const handleInputChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
  };

  const deleteShiftHandler = () => {
    const shiftsList = shifts.filter((sh) => sh.id !== selectedShiftId);
    dispatch(deleteShiftRequest(shiftsList));
    setSelectedShiftId(false);
    resetForm();
  };

  const saveShiftHandler = () => {
    const newList = [...shifts];
    let clickedShift = newList.find((h) => h.id === selectedShiftId);

    if (clickedShift) {
      const index = newList.indexOf(clickedShift);
      clickedShift = { id: clickedShift.id, ...state };
      newList.splice(index, 1, clickedShift);

      dispatch(updateShiftRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(saveShiftRequest(newList));
    }

    setSelectedShiftId(false);
    resetForm();
  };

  const selectShiftsHandler = (e, id) => {
    e.stopPropagation();
    const clickedShift = shifts.find((sh) => sh.id === id);
    setState({
      ...clickedShift,
    });
    setSelectedShiftId(id);
  };

  const durationOptions = new Array(maxHoursNumber)
    .fill("")
    .map((_, index) => ({
      value: `${index + 1}`,
      label: `${index + 1} h`,
    }));

  return (
    <div className={common.contentWrapper}>
      <Form
        state={state}
        setState={setState}
        handleInputChange={handleInputChange}
        saveShiftHandler={saveShiftHandler}
        deleteShiftHandler={deleteShiftHandler}
        durationOptions={durationOptions}
        isDeleteDisabled={!selectedShiftId}
        isSaveDisabled={false}
      />
      <div
        className={`${common.dataContainer} ${
          shifts.length === 0 ? common.emptyDataContainer : ""
        }`}
      >
        {shifts.length > 0 ? (
          shifts.map((shift) => {
            const format = "LT";
            const start = moment(shift.start).format(format);
            const finish = moment(shift.finish).format(format);
            return (
              <DataItem
                key={shift.id}
                containerClass={styles.dataItem}
                onClickHandler={selectShiftsHandler}
                id={shift.id}
                isActive={selectedShiftId === shift.id}
                isBlured={selectedShiftId && selectedShiftId !== shift.id}
              >
                <div className={`${common.section} ${styles.section}`}>
                  <div className={`${common.subtitle} ${common.nameSubtitle}`}>
                    Shifts name
                  </div>
                  <div className={common.itemName}>{shift.name}</div>
                </div>

                <div className={`${common.section} ${styles.section}`}>
                  <div className={`${common.subtitle}`}>Start</div>
                  <div className={styles.data}>{start}</div>
                </div>

                <div className={`${common.section} ${styles.section}`}>
                  <div className={`${common.subtitle}`}>Finish</div>
                  <div className={styles.data}>{finish}</div>
                </div>

                <div className={`${common.section} ${styles.section}`}>
                  <div className={`${common.subtitle}`}>Duration</div>
                  <div className={styles.data}>{`${shift.duration} h`}</div>
                </div>
              </DataItem>
            );
          })
        ) : (
          <div className={common.noData}>
            <div>No avaliable shifts</div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Shifts;
