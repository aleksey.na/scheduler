import React from "react";

// ** Components
import Form from "@components/Form";
import Input from "@components/Input";
import TimeRangePicker from "@components/TimeRangePicker";
import TimeRangeLine from "@components/TimeRangeLine";
import SelectInput from "@components/SelectInput";
import Textarea from "@components/Textarea";

import styles from "./shiftsForm.module.scss";

const ShiftsForm = ({
  state,
  setState,
  handleInputChange,
  saveShiftHandler,
  deleteShiftHandler,
  durationOptions,
  isSaveDisabled,
  isDeleteDisabled,
}) => {
  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      saveHandler={saveShiftHandler}
      deleteHandler={deleteShiftHandler}
      isSaveDisabled={isSaveDisabled}
      isDeleteDisabled={isDeleteDisabled}
    >
      <div className={`${styles.block} ${styles.firstRow}`}>
        <Input
          name={"name"}
          label={"Shifts name"}
          value={state.name}
          onChange={handleInputChange}
        />
        <SelectInput
          name={"duration"}
          label={"Duration:"}
          value={state.duration}
          options={durationOptions}
          onChange={handleInputChange}
        />
      </div>

      <div className={`${styles.block}`}>
        <TimeRangePicker
          state={state}
          onChange={handleInputChange}
          mainLabel={"Edit Restrictions"}
          onStateChange={setState}
        />
      </div>

      <div className={`${styles.lineRange}`}>
        <TimeRangeLine />
      </div>

      <div className={`${styles.block}`}>
        <Textarea
          name={"comments"}
          label={"Comments"}
          value={state.comments}
          placeholder={"Type comments"}
          containerClass={styles.textArea}
          onChange={handleInputChange}
          rows={9}
        />
      </div>
    </Form>
  );
};

export default ShiftsForm;
