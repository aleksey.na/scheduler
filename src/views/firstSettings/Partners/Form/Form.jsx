import React from "react";

import Button from "@components/Button";
import Input from "@components/Input";
import ContentContainer from "@components/ContentContainer";
import SelectInput from "@components/SelectInput";
import Textarea from "@components/Textarea";

import SVG from "@components/SVG";

import styles from "./form.module.scss";

import userAvatar from "@src/assets/images/avatars/2.png";
import userAvatarPlaceholder from "@src/assets/images/icons/avatar-placeholder.png";

const Form = ({
  state,
  levelOptions,
  handleInputChange,
  selectedPartner,
  savePartnerHandler,
  deletePartnerHandler,
  changeAvatarHandler,
  handleCopyLink,
}) => {
  return (
    <ContentContainer className={styles.formWrapper}>
      <form className={styles.partnerForm}>
        <div className={styles.avatar}>
          {/* <img
      src={selectedPartner ? state.photo : userAvatarPlaceholder}
      alt="user avatar"
    /> */}
          {/* <img
          src={selectedPartner ? userAvatar : userAvatarPlaceholder}
          alt="user avatar"
        /> */}
          {/* {isAvatarLoading ? (
          <div>...Loading</div>
        ) : ( */}
          <>
            <input name="avatar" type="file" onChange={changeAvatarHandler} />
            {/* {state.photo ? (
            <img src={`${state.photo}`} alt="user avatar" />
          ) : ( */}
            <img
              src={selectedPartner ? userAvatar : userAvatarPlaceholder}
              alt="user avatar"
            />

            {/* { )} */}
          </>
          {/* )} */}
        </div>
        <div className={styles.fieldsContainer}>
          <div className={`${styles.name} ${styles.block}`}>
            <Input
              name="name"
              label={"Partner"}
              placeholder={"Partner name"}
              value={state.name}
              onChange={handleInputChange}
              // error={errors.listingTitle}
            />
            <SelectInput
              name="level"
              label={"Level"}
              placeholder={"Select level"}
              value={state.level}
              onChange={handleInputChange}
              options={levelOptions}
              // error={errors.category}
              width={100}
              required
            />
          </div>
          <div className={`${styles.description} ${styles.block}`}>
            <Textarea
              name="description"
              label={"Description"}
              value={state.description}
              onChange={handleInputChange}
              rows={9}
              // error={errors.listingDescription}
            />
          </div>
          <div className={`${styles.invite} ${styles.block}`}>
            <div className={styles.linkContainer}>
              <div className={styles.label}>Invite Partner</div>
              <div
                onClick={() => handleCopyLink(state.link)}
                className={styles.field}
              >
                <div className={styles.link}>{state.link}</div>
                <div className={styles.iconWrapper}>
                  <SVG.Link className={styles.icon} />
                </div>
              </div>
            </div>
            <div className={styles.buttons}>
              <Button
                variant={"secondary"}
                isDisabled={!selectedPartner}
                onClick={() => deletePartnerHandler(selectedPartner)}
              >
                Delete
              </Button>
              <Button
                variant={"primary"}
                onClick={() => savePartnerHandler(selectedPartner)}
              >
                Save
              </Button>
            </div>
          </div>
        </div>
      </form>
    </ContentContainer>
  );
};

export default Form;
