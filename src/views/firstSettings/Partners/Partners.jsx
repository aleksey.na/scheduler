import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import partnersReducer from "@store/entities/Partners/partners.reducer";
import { selectorPartners } from "@selectors/partnersSelectors";

// ** Componrnts
import Slider from "./Slider/Slider";
import Form from "./Form/Form";
// import Spinner from "@components/ui-loader";
import { Spinner } from "reactstrap";

import styles from "./partners.module.scss";

const {
  deletePartnerRequest,
  savePartnerRequest,
  updatePartnerRequest,
  fetchPartnersRequest,
} = partnersReducer.actions;

function Partners() {
  const dispatch = useDispatch();
  const initialState = {
    name: "",
    level: "",
    description: "",
    photo: "",
    link: "https://www.flaticon.com/copy-link-here",
  };

  const [state, setState] = useState({
    ...initialState,
  });
  const [selectedPartnerId, setSelectedPartnerId] = useState("");
  const [isFormShown, setIsFormShown] = useState(false);

  const partners = useSelector(selectorPartners);

  useEffect(() => {
    if (partners.length === 0) dispatch(fetchPartnersRequest());
  }, []);

  const handleInputChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const addNewPartnerHandler = () => {
    if (isFormShown && !selectedPartnerId) {
      setIsFormShown(false);
    } else {
      setIsFormShown(true);
    }
    setSelectedPartnerId(false);
    setState(initialState);
  };

  const selectPartnerHandler = (id) => {
    const clickedPartner = partners.find((p) => p.id === id);
    const { name, description, level, photo, link } = clickedPartner;

    setSelectedPartnerId(id);
    setState({
      name,
      description,
      level,
      photo,
      link,
    });
    setIsFormShown(true);
  };

  const savePartnerHandler = (id) => {
    const newList = [...partners];
    let clickedPartner = newList.find((p) => p.id === id);

    if (clickedPartner) {
      const index = newList.indexOf(clickedPartner);
      clickedPartner = { id: clickedPartner.id, ...state };
      newList.splice(index, 1, clickedPartner);

      dispatch(updatePartnerRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(savePartnerRequest(newList));
    }

    setSelectedPartnerId(false);
    setIsFormShown(false);
  };

  const deletePartnerHandler = (id) => {
    const partnersList = partners.filter((p) => p.id !== id);
    dispatch(deletePartnerRequest(partnersList));
    setSelectedPartnerId(false);
    setIsFormShown(false);
  };

  const changeAvatarHandler = (e) => {
    if (e.target.files.length) {
      const formData = new FormData();
      formData.append("photo", e.target.files[0]);
      setState({
        ...state,
        photo: e.target.files[0],
      });
      // loadToApi(e.target.files[0]);
      // #TODO: load photo to API, set loader, read after loaded
    }
  };

  const copyLinkHandler = (text) => {
    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }
    navigator.clipboard.writeText(text).then(
      function () {
        console.log("Async: Copying to clipboard was successful!");
      },
      function (err) {
        console.error("Async: Could not copy text: ", err);
      }
    );
  };

  const levelOptions = new Array(4).fill("").map((level, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  return (
    <div className={styles.partnersContainer}>
      <Slider
        partnersList={partners}
        selectedPartner={selectedPartnerId}
        addNewPartnerHandler={addNewPartnerHandler}
        selectPartnerHandler={selectPartnerHandler}
      />

      {isFormShown && (
        <Form
          state={state}
          levelOptions={levelOptions}
          handleInputChange={handleInputChange}
          selectedPartner={selectedPartnerId}
          savePartnerHandler={savePartnerHandler}
          deletePartnerHandler={deletePartnerHandler}
          changeAvatarHandler={changeAvatarHandler}
          handleCopyLink={copyLinkHandler}
        />
      )}
    </div>
  );
}

export default Partners;
