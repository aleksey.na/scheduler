import React from "react";

import Slider from "react-slick";
import SVG from "@components/SVG";

import styles from "./slider.module.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import userAvatar from "@src/assets/images/avatars/2.png";
import plusIcon from "@src/assets/images/icons/plus-icon.png";

function NextArrow(props) {
  const { className, customClass, disabledClass, onClick } = props;

  const isSlidesFinished = props.currentSlide + 1 >= props.slideCount;
  return (
    <div
      className={`${customClass} ${
        isSlidesFinished ? disabledClass : ""
      } ${className}`}
      style={{ right: "-50px", paddingLeft: "2px" }}
      onClick={onClick}
    >
      <SVG.ArrowDown
        height={12}
        width={6}
        style={{ transform: "rotate(180deg)" }}
      />
    </div>
  );
}

function PrevArrow(props) {
  const { className, customClass, disabledClass, onClick } = props;

  const isSlidesFinished = props.currentSlide === 0;
  return (
    <div
      className={`${className} ${customClass} ${
        isSlidesFinished ? disabledClass : ""
      }`}
      style={{ left: "-50px", paddingRight: "2px" }}
      onClick={onClick}
    >
      <SVG.ArrowDown height={12} width={6} />
    </div>
  );
}

function PartnersSlider({
  partnersList,
  selectedPartner,
  addNewPartnerHandler,
  selectPartnerHandler,
}) {
  const settings = {
    nextArrow: (
      <NextArrow
        customClass={styles.customArrow}
        disabledClass={styles.disabled}
      />
    ),
    prevArrow: (
      <PrevArrow
        customClass={styles.customArrow}
        disabledClass={styles.disabled}
      />
    ),
    dots: true,
    dotsClass: styles.customDots,
    customPaging: (_) => <div className={styles.customDot}></div>,
    appendDots: (dots) => (
      <div>
        <ul className={styles.dotsUl}>
          {dots.map((dot, index) => (
            <div
              key={index}
              className={`${dot.props.className && styles.activeDot} ${
                styles.dotWrapper
              }`}
            >
              {dot}
            </div>
          ))}
        </ul>
      </div>
    ),
    // responsive: [
    //   {
    //     breakpoint: 1024,
    //     settings: {
    //       slidesToShow: 3,
    //       slidesToScroll: 3,
    //       infinite: true,
    //       dots: true
    //     }
    //   },
    //   {
    //     breakpoint: 600,
    //     settings: {
    //       slidesToShow: 2,
    //       slidesToScroll: 2,
    //       initialSlide: 2
    //     }
    //   },
    //   {
    //     breakpoint: 480,
    //     settings: {
    //       slidesToShow: 1,
    //       slidesToScroll: 1
    //     }
    //   }
    // ],

    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: false,
    rows: 2,
    slidesPerRow: 7,
  };

  const list = partnersList.map((partner, i) =>
    partner.type ? (
      <div key={i}>
        <div
          onClick={addNewPartnerHandler}
          className={`${styles.cardStatic} ${styles.card}`}
        >
          <div className={styles.plusIcon}>
            <img src={plusIcon} alt="add new partner" />
          </div>
          <div className={styles.text}>Add New Partner</div>
        </div>
      </div>
    ) : (
      <div key={i}>
        <div
          onClick={() => selectPartnerHandler(partner.id)}
          className={`${styles.card} ${styles.cardUser}
          ${selectedPartner === partner.id && styles.selected}`}
        >
          <div className={styles.image}>
            <img src={userAvatar} alt="user avatar" />
          </div>
          <div className={styles.name}>{partner.name}</div>
          <div className={styles.level}>Level {partner.level}</div>
        </div>
      </div>
    )
  );

  return (
    <Slider className={styles.customWrapper} {...settings}>
      {list}
    </Slider>
  );
}

export default PartnersSlider;
