import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import servicesReducer from "@store/entities/Services/services.reducer";
import {
  selectorServices,
  selectorSpecialServices,
} from "@selectors/servicesSelectors";

import Form from "./SpecialServicesForm/SpecialServicesForm";
import DataItem from "@components/DataItem";

import styles from "./specialServices.module.scss";
import common from "../common.module.scss";

const {
  fetchServicesRequest,
  fetchSpecialServicesRequest,
  saveServiceRequest,
  saveSpecialServiceRequest,
  updateSpecialServiceRequest,
  deleteSpecialServiceRequest,
} = servicesReducer.actions;

function SpecialServices() {
  const dispatch = useDispatch();
  const levelsQuantity = 4;
  const levelInitialState = {
    valid: "",
    validService: "",
    exclude: "",
    excludeServices: "",
    // #TODO: multi-select
    // excludeServices: [],
  };
  const initialState = {
    name: "",
    number: "",
    attachedCore: "",
    levelsData: Array(levelsQuantity).fill(levelInitialState),
  };
  const [state, setState] = useState({
    ...initialState,
  });
  const [extendedLevelId, setExtendedLevelId] = useState("");

  const [selectedCoreService, setSelectedCoreService] = useState("");
  const [selectedSpecialServiceId, setSelectedSpecialServiceId] = useState("");

  const coreServices = useSelector(selectorServices);
  const specialServices = useSelector(selectorSpecialServices);

  useEffect(() => {
    if (coreServices.length === 0) dispatch(fetchServicesRequest());
    if (specialServices.length === 0) dispatch(fetchSpecialServicesRequest());
  }, []);

  const handleInputChange = (level) => (name, value) => {
    if (name === "name") {
      const service = coreServices.find((s) => s.id === value);
      setSelectedCoreService(service);
    }

    if (typeof level === "number") {
      const newLevelsList = [...state.levelsData];
      const targetLevel = { ...newLevelsList[level] };
      targetLevel[name] = value;
      newLevelsList.splice(level, 1, targetLevel);

      setState({
        ...state,
        levelsData: newLevelsList,
      });
      return;
    }

    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
    setSelectedCoreService("");
  };

  const saveLevelHandler = (index, action) => {
    const newSelectedObj = { ...selectedCoreService };
    const levels = [...newSelectedObj.levels];

    if (action === "save") {
      const selectedLevel = { ...levels[index] };
      selectedLevel.exclusions = true;
      levels.splice(index, 1, selectedLevel);
      newSelectedObj.levels = levels;

      setSelectedCoreService(newSelectedObj);
      setExtendedLevelId("");
      return;
    }

    setExtendedLevelId(index);
  };

  const selectServiceHandler = (e, id) => {
    e.stopPropagation();
    setExtendedLevelId("");
    const clickedSpecialService = specialServices.find((v) => v.id === id);
    const selectedCoreService = coreServices.find(
      (s) => s.id === clickedSpecialService.attachedCore
    );
    setSelectedCoreService(selectedCoreService);
    setState({
      ...clickedSpecialService,
    });
    setSelectedSpecialServiceId(clickedSpecialService.id);
  };

  const deleteSecviceHandler = () => {
    const servicesList = specialServices.filter(
      (p) => p.id !== selectedSpecialServiceId
    );
    dispatch(deleteSpecialServiceRequest(servicesList));
    setSelectedSpecialServiceId(false);
    resetForm();
  };

  const saveServiceHandler = () => {
    const newList = [...specialServices];
    let clickedService = newList.find((s) => s.id === selectedSpecialServiceId);

    if (clickedService) {
      const index = newList.indexOf(clickedService);
      const newServiceObj = { id: clickedService.id, ...state };
      newList.splice(index, 1, newServiceObj);

      dispatch(updateSpecialServiceRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
        attachedCore: selectedCoreService.id,
      });
      dispatch(saveSpecialServiceRequest(newList));
    }

    // #TODO: refactor after api appears, core must be connected with special
    // update coreService
    const newCoreList = [...coreServices];
    let clickedCoreService = newCoreList.find(
      (h) => h.id === selectedCoreService.id
    );
    const index = newCoreList.indexOf(clickedCoreService);
    clickedCoreService = { ...selectedCoreService };
    newCoreList.splice(index, 1, clickedCoreService);
    dispatch(saveServiceRequest(newCoreList));
    //

    setSelectedSpecialServiceId(false);
    resetForm();
  };

  const servicesOptions = coreServices.map((service) => ({
    value: service.id,
    label: service.name,
  }));

  const boolTypes = {
    Yes: "Yes",
    No: "No",
  };
  const boolOptions = Object.entries(boolTypes).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  const renderServiceName = (id) => {
    // #TODO: we got wrong Special service name, find out the connection between Core and Special
    if (typeof id === "number") {
      const coreServiceName = coreServices.find((s) => s.id === id).name;
      return coreServiceName;
    }
    return "No";
  };

  return (
    <div className={common.contentWrapper}>
      <Form
        state={state}
        selectedCoreService={selectedCoreService}
        extendedLevelId={extendedLevelId}
        saveLevelHandler={saveLevelHandler}
        handleInputChange={handleInputChange}
        saveSecviceHandler={saveServiceHandler}
        deleteSecviceHandler={deleteSecviceHandler}
        renderServiceName={renderServiceName}
        servicesOptions={servicesOptions}
        boolOptions={boolOptions}
        isDeleteDisabled={!selectedSpecialServiceId}
        isSaveDisabled={!selectedCoreService}
      />
      <div
        className={`${common.dataContainer} ${
          specialServices.length === 0 ? common.emptyDataContainer : ""
        }`}
      >
        {specialServices.length > 0 ? (
          specialServices.map((service) => (
            <DataItem
              key={service.id}
              containerClass={styles.dataItem}
              onClickHandler={selectServiceHandler}
              id={service.id}
              isActive={service.id === selectedSpecialServiceId}
              isBlured={
                selectedSpecialServiceId &&
                service.id !== selectedSpecialServiceId
              }
            >
              <div className={`${styles.titleContainer}`}>
                <div className={`${common.section}`}>
                  <div className={`${common.subtitle} ${common.nameSubtitle}`}>
                    Service
                  </div>
                  <div
                    className={`${common.itemName} ${
                      service.id === selectedSpecialServiceId
                        ? styles.active
                        : ""
                    }`}
                  >
                    {renderServiceName(service.id)}
                  </div>
                </div>

                <div className={`${common.section}`}>
                  <div
                    className={`${common.subtitle} ${styles.numberSubtitle}`}
                  >
                    Number of Services
                  </div>
                  <div className={styles.numberData}>{service.number}</div>
                </div>
              </div>

              <div className={`${styles.specialDataContainer}`}>
                <div className={styles.dataTitle}>Exclusions</div>
                <div className={`${styles.data}`}>
                  {service.levelsData.map((levelData, index) => (
                    <div className={`${common.section} ${styles.dataSection}`}>
                      <div className={common.subtitle}>{`Level ${
                        index + 1
                      }`}</div>
                      <div className={common.secSection}>
                        {renderServiceName(levelData.excludeServices)}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </DataItem>
          ))
        ) : (
          <div className={common.noData}>
            <div>No avaliable core services</div>
          </div>
        )}
      </div>
    </div>
  );
}

export default SpecialServices;
