import React from "react";

import Input from "@components/Input";
import SelectInput from "@components/SelectInput";
import Form from "@components/Form";
import Button from "@components/Button";
import { Collapse } from "react-bootstrap";

import styles from "./specialServicesForm.module.scss";
import common from "../../common.module.scss";

const SpecialServicesForm = ({
  state,
  selectedCoreService,
  extendedLevelId,
  saveLevelHandler,
  handleInputChange,
  saveSecviceHandler,
  deleteSecviceHandler,
  renderServiceName,
  servicesOptions,
  boolOptions,
  isSaveDisabled,
  isDeleteDisabled,
}) => {
  const { levels: coreServiceLevels } = selectedCoreService;
  const { levelsData: specialServicesLevelData } = state;

  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      saveHandler={saveSecviceHandler}
      deleteHandler={deleteSecviceHandler}
      isSaveDisabled={isSaveDisabled}
      isDeleteDisabled={isDeleteDisabled}
    >
      <div className={`${styles.block} ${styles.firstRow}`}>
        <SelectInput
          name={"name"}
          label={"Name of the service"}
          options={servicesOptions}
          value={state.name}
          // containerClass={styles.inputBlock}
          onChange={handleInputChange("")}
        />
        <Input
          name={"number"}
          label={"Number of Services "}
          value={state.number}
          // containerClass={styles.inputBlock}
          onChange={handleInputChange("")}
        />
      </div>

      {coreServiceLevels?.length > 0 &&
        coreServiceLevels.map((level, index) => {
          const levelNumber = index + 1;
          const buttonText =
            level.exclusions && extendedLevelId !== index
              ? "Edit"
              : extendedLevelId === index
              ? "Save"
              : !level.exclusions && extendedLevelId !== index
              ? "Add exclusions"
              : "";

          return (
            <div
              key={index}
              className={`${styles.block} ${styles.columnBlock}`}
            >
              <div
                className={`${styles.titleContainer} ${
                  extendedLevelId === index ? styles.extendedTitle : ""
                }`}
              >
                <div className={styles.levelInfo}>
                  <div className={styles.title}>
                    <div>{`Level ${levelNumber}`}</div>
                    <div>Partners</div>
                  </div>
                  <div className={styles.sectionsContainer}>
                    <div className={styles.section}>
                      <div>Minimum:</div>
                      <div>{level.minimum}</div>
                    </div>
                    <div className={styles.section}>
                      <div>Maximum:</div>
                      <div>{level.maximum}</div>
                    </div>
                    <div className={styles.section}>
                      <div>Min rotations:</div>
                      <div>{level.rotation}</div>
                    </div>
                  </div>
                </div>
                <div>
                  <Button
                    variant={"primary"}
                    isDisabled={false}
                    className={styles.exclusionBtn}
                    onClick={() =>
                      saveLevelHandler(index, buttonText.toLocaleLowerCase())
                    }
                  >
                    {buttonText}
                  </Button>
                </div>
              </div>

              {level.exclusions && extendedLevelId !== index && (
                <div className={styles.exclusionContainer}>
                  <div className={styles.title}>Exclusions</div>
                  <div className={styles.dataItem}>
                    <div className={`${common.section} ${styles.section}`}>
                      <div
                        className={`${common.subtitle} ${common.sectionSubtitle}`}
                      >
                        Valid for Certain Services
                      </div>
                      <div className={`${common.itemName} ${styles.value}`}>
                        {specialServicesLevelData[index].valid || "None"}
                      </div>
                    </div>
                    <div className={`${common.section} ${styles.section}`}>
                      <div
                        className={`${common.subtitle} ${common.sectionSubtitle}`}
                      >
                        Services
                      </div>
                      <div className={`${common.itemName} ${styles.value}`}>
                        {renderServiceName(
                          specialServicesLevelData[index].validService
                        ) || "None"}
                      </div>
                    </div>
                    <div className={`${common.section} ${styles.section}`}>
                      <div
                        className={`${common.subtitle} ${common.sectionSubtitle}`}
                      >
                        Exclude Services
                      </div>
                      <div className={`${common.itemName} ${styles.value}`}>
                        {specialServicesLevelData[index].exclude || "None"}
                      </div>
                    </div>
                    <div className={`${common.section} ${styles.section}`}>
                      <div
                        className={`${common.subtitle} ${common.sectionSubtitle}`}
                      >
                        Services
                      </div>
                      <div className={`${common.itemName} ${styles.value}`}>
                        {renderServiceName(
                          specialServicesLevelData[index].excludeServices
                        ) || "None"}
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <Collapse in={extendedLevelId === index}>
                <div className={styles.inputGroup}>
                  <div className={styles.column}>
                    <SelectInput
                      name={"valid"}
                      label={"Valid for Certain Services"}
                      value={specialServicesLevelData[index].valid}
                      options={boolOptions}
                      containerClass={styles.levelSelect}
                      labelClass={styles.label}
                      onChange={handleInputChange(index)}
                    />
                    <SelectInput
                      name={"exclude"}
                      label={"Exclude Services"}
                      value={specialServicesLevelData[index].exclude}
                      options={boolOptions}
                      containerClass={styles.levelSelect}
                      labelClass={styles.label}
                      onChange={handleInputChange(index)}
                    />
                  </div>
                  <div className={styles.column}>
                    <SelectInput
                      name={"validService"}
                      label={"Services"}
                      value={specialServicesLevelData[index].validService}
                      options={servicesOptions}
                      containerClass={styles.levelSelect}
                      labelClass={styles.label}
                      onChange={handleInputChange(index)}
                    />
                    <SelectInput
                      name={"excludeServices"}
                      label={"Services"}
                      value={specialServicesLevelData[index].excludeServices}
                      options={servicesOptions}
                      containerClass={styles.levelSelect}
                      labelClass={styles.label}
                      onChange={handleInputChange(index)}
                      isDisabled={
                        specialServicesLevelData[index].exclude === "No" && true
                      }
                    />
                  </div>
                </div>
              </Collapse>
            </div>
          );
        })}
    </Form>
  );
};

export default SpecialServicesForm;
