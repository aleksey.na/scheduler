import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import shiftsReducer from "@store/entities/Shifts/shifts.reducer";
import { selectorSpecialShifts } from "@selectors/shiftsSelectors";

import Form from "./ShiftsForm/SpecialShiftsForm";
import DataItem from "@components/DataItem";

import styles from "./specialShifts.module.scss";
import common from "../common.module.scss";

const {
  fetchSpecialShiftsRequest,
  updateSpecialShiftRequest,
  saveSpecialShiftRequest,
  deleteSpecialShiftRequest,
} = shiftsReducer.actions;

const SpecialShifts = () => {
  const dispatch = useDispatch();
  const maxHoursNumber = 24;

  const initialState = {
    name: "",
    duration: "",
    day: "",
    timeOff: "",
    start: "2021-11-04T15:07:05.879Z",
    finish: "2021-11-04T23:11:05.879Z",
    // start: "",
    // finish: "",
    comments: "",
  };
  const [state, setState] = useState({
    ...initialState,
  });
  const [selectedShiftId, setSelectedShiftId] = useState("");
  const specialShifts = useSelector(selectorSpecialShifts);

  useEffect(() => {
    if (specialShifts.length === 0) dispatch(fetchSpecialShiftsRequest());
  }, []);

  const handleInputChange = (name, value) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const resetForm = () => {
    setState({
      ...initialState,
    });
  };

  const deleteShiftHandler = () => {
    const shiftsList = specialShifts.filter((sh) => sh.id !== selectedShiftId);
    dispatch(deleteSpecialShiftRequest(shiftsList));
    setSelectedShiftId(false);
    resetForm();
  };

  const saveShiftHandler = () => {
    const newList = [...specialShifts];
    let clickedShift = newList.find((h) => h.id === selectedShiftId);

    if (clickedShift) {
      const index = newList.indexOf(clickedShift);
      clickedShift = { id: clickedShift.id, ...state };
      newList.splice(index, 1, clickedShift);

      dispatch(updateSpecialShiftRequest(newList));
    } else {
      newList.push({
        ...state,
        id: Math.random(),
      });
      dispatch(saveSpecialShiftRequest(newList));
    }

    setSelectedShiftId(false);
    resetForm();
  };

  const selectShiftsHandler = (e, id) => {
    e.stopPropagation();
    const clickedShift = specialShifts.find((sh) => sh.id === id);
    setState({
      ...clickedShift,
    });
    setSelectedShiftId(id);
  };

  const durationOptions = new Array(maxHoursNumber)
    .fill("")
    .map((_, index) => ({
      value: `${index + 1}`,
      label: `${index + 1} h`,
    }));
  const timeOffOptions = new Array(maxHoursNumber).fill("").map((_, index) => ({
    value: `${index + 1}`,
    label: `${index + 1} h after`,
  }));

  const days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  const daysOptions = days.map((day, index) => ({
    value: index + 1,
    label: day,
  }));

  return (
    <div className={common.contentWrapper}>
      <Form
        state={state}
        onStateChange={setState}
        handleInputChange={handleInputChange}
        saveShiftHandler={saveShiftHandler}
        deleteShiftHandler={deleteShiftHandler}
        durationOptions={durationOptions}
        timeOffOptions={timeOffOptions}
        daysOptions={daysOptions}
        isDeleteDisabled={!selectedShiftId}
        isSaveDisabled={false}
      />
      <div
        className={`${common.dataContainer} ${
          specialShifts.length === 0 ? common.emptyDataContainer : ""
        }`}
      >
        {specialShifts.length > 0 ? (
          specialShifts.map((shift) => (
            <DataItem
              key={shift.id}
              containerClass={styles.dataItem}
              onClickHandler={selectShiftsHandler}
              id={shift.id}
              isActive={selectedShiftId === shift.id}
              isBlured={selectedShiftId && selectedShiftId !== shift.id}
            >
              <div className={`${common.section} ${styles.section}`}>
                <div className={`${common.subtitle} ${common.nameSubtitle}`}>
                  Special Shifts
                </div>
                <div className={common.itemName}>{shift.name}</div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={`${common.subtitle}`}>Start</div>
                <div className={styles.data}>{`${shift.start}:00 AM`}</div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={`${common.subtitle}`}>Finish</div>
                <div className={styles.data}>{`${shift.finish}:00 PM`}</div>
              </div>

              <div className={`${common.section} ${styles.section}`}>
                <div className={`${common.subtitle}`}>Duration</div>
                <div className={styles.data}>{`${shift.finish} h`}</div>
              </div>
            </DataItem>
          ))
        ) : (
          <div className={common.noData}>
            <div>No avaliable special shifts</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default SpecialShifts;
