import React from "react";

import Form from "@components/Form";
import Input from "@components/Input";
import SelectInput from "@components/SelectInput";
import TimeRangePicker from "@components/TimeRangePicker";
import TimeRangeLine from "@components/TimeRangeLine";
import Textarea from "@components/Textarea";

import styles from "./specialShiftsForm.module.scss";

const ShiftsForm = ({
  state,
  setState,
  handleInputChange,
  saveShiftHandler,
  deleteShiftHandler,
  durationOptions,
  timeOffOptions,
  daysOptions,
  isSaveDisabled,
  isDeleteDisabled,
}) => {
  return (
    <Form
      containerClass={styles.formContainer}
      buttonsContainerClass={styles.buttons}
      saveHandler={saveShiftHandler}
      deleteHandler={deleteShiftHandler}
      isSaveDisabled={isSaveDisabled}
      isDeleteDisabled={isDeleteDisabled}
    >
      <div className={`${styles.row} ${styles.firstRow}`}>
        <Input
          name={"name"}
          label={"Special shifts name"}
          value={state.name}
          onChange={handleInputChange}
        />
        <SelectInput
          name={"duration"}
          label={"Duration:"}
          value={state.duration}
          options={durationOptions}
          onChange={handleInputChange}
        />
      </div>

      <div className={`${styles.row} ${styles.firstRow}`}>
        <SelectInput
          name={"day"}
          label={"Select day"}
          value={state.day}
          options={daysOptions}
          onChange={handleInputChange}
        />
        <SelectInput
          name={"timeOff"}
          label={"Time Off"}
          value={state.timeOff}
          options={timeOffOptions}
          onChange={handleInputChange}
        />
      </div>

      <div className={`${styles.row}`}>
        <TimeRangePicker
          state={state}
          onChange={handleInputChange}
          mainLabel={"Edit Restrictions"}
          onStateChange={setState}
        />
      </div>

      <div className={`${styles.lineRange}`}>
        <TimeRangeLine />
      </div>

      <div className={`${styles.row}`}>
        <Textarea
          name={"comments"}
          label={"Comments"}
          value={state.comments}
          placeholder={"Type comments"}
          containerClass={styles.textArea}
          onChange={handleInputChange}
          rows={9}
        />
      </div>
    </Form>
  );
};

export default ShiftsForm;
