import React, { useState } from "react";

// ** Third Party libraries
import moment from "moment";

// ** Components
import Button from "@components/Button";
import Avatar from "@components/avatar";
import SVG from "@components/SVG";
import Modal from "./Modal/Modal";
import ShiftPlate from "../components/Shift/ShiftPlate";
import { Dropdown } from "react-bootstrap";

// ** Styles
import styles from "./home.module.scss";
import common from "../../common.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-9.jpg";

const WeekCard = ({ week, days, daySelected }) => {
  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={common.customToggle}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <SVG.Calendar fill="#6B7DAA" />
      {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={common.flipDown} />
    </div>
  ));

  return (
    <div className={`${common.shadow} ${styles.weekCard}`}>
      <div className={`${common.titleContainer} ${styles.title}`}>
        <div className={`${common.title}`}>Week {moment().format("W")}</div>
        <div>
          <Dropdown className={common.dropdown}>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
              as={CustomToggle}
            >
              {moment().format("D MMM")} -{" "}
              {moment().add(14, "days").format("D MMM")}
            </Dropdown.Toggle>
            {/* <Dropdown.Menu></Dropdown.Menu> */}
          </Dropdown>
        </div>
      </div>
      <div className={styles.weekContainer}>
        <div className={styles.head}>
          {week.map((day, index) => (
            <div
              key={index}
              className={`${styles.day} ${
                daySelected === index ? styles.active : ""
              }`}
            >
              {day}
            </div>
          ))}
        </div>
        <div className={styles.dates}>
          {days.map((date, index) => (
            <div
              key={index}
              className={`${styles.date} ${
                daySelected === index ? styles.active : ""
              }`}
            >
              {index + 1}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

const ShiftsCard = ({ shifts, onShowModal }) => {
  return (
    <div className={`${common.shadow} ${styles.shiftsCard}`}>
      <div className={`${common.titleContainer} ${styles.title}`}>
        <div>
          <div className={styles.subtitle}>Tuesday</div>
          <div className={`${common.title}`}>6 April, 2021</div>
        </div>
        <Button onClick={onShowModal} className={styles.addBtn}>
          <div>
            <SVG.Close />
          </div>
          <div>Add Custom Shift</div>
        </Button>
      </div>
      <div className={styles.shiftsContainer}>
        {shifts.map((shift) => (
          <ShiftPlate shift={shift} key={shift.id} />
        ))}
      </div>
    </div>
  );
};

const NotificationsCard = ({ notifications }) => {
  return (
    <div className={`${common.shadow} ${styles.notesCard}`}>
      <div className={`${common.titleContainer} ${styles.noteTitle}`}>
        <div className={`${common.title}`}>Notifications</div>
        <div className={`${styles.btns}`}>
          <Button
            variant={"secondary"}
            // onClick={() => ()}
          >
            Pending Approvals
          </Button>
          <Button
            variant={"secondary"}
            isDisabled={true}
            // onClick={() => ()}
          >
            Resolved
          </Button>
        </div>
      </div>
      <div className={styles.notesContainer}>
        {notifications.map((note, index) => (
          <div key={note.id} className={styles.card}>
            <Avatar
              className={styles.avatar}
              img={defaultAvatar}
              imgHeight="78"
              imgWidth="78"
            />
            <div className={styles.info}>
              {note.type === "trade" ? (
                <div className={styles.type}>Trade</div>
              ) : (
                <div className={`${styles.type} ${styles.error}`}>Error</div>
              )}
              <div className={styles.userData}>
                {note.type === "trade" ? (
                  <>
                    <span>{note.person} </span>
                    <span>opened a trade</span>
                  </>
                ) : (
                  <span>Schedules have conflicts</span>
                )}
              </div>
              <div className={styles.shiftData}>
                8:00 AM - 8:00 AM | Next day (24h)
              </div>
            </div>
            <div className={styles.nav}>
              <SVG.ArrowLeft
                width={18}
                height={12}
                style={{ transform: "rotate(180deg)" }}
                fill={"#b3c1e3"}
              />
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

function Home() {
  const [isModalShown, setModalShown] = useState(false);
  const daySelected = 3;

  const handleShowModal = () => {
    setModalShown(!isModalShown);
  };

  const weekList = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  const daysOptions = weekList.map((day, index) => ({
    value: index + 1,
    label: day,
  }));

  return (
    <div className={common.pageContainer}>
      <div>
        <WeekCard week={week} days={days} daySelected={daySelected} />
        <ShiftsCard shifts={shifts} onShowModal={handleShowModal} />
      </div>
      <NotificationsCard notifications={notifications} />
      <Modal
        isShown={isModalShown}
        onShow={handleShowModal}
        daysOptions={daysOptions}
      />
    </div>
  );
}

const week = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
const days = new Array(14).fill("");
const shifts = [
  {
    name: "Shift 1",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 1,
  },
  {
    name: "Shift 2",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 2,
  },
  {
    name: "Special shift",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 3,
  },
  {
    name: "Shift 1",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 4,
  },
  {
    name: "Shift 2",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 5,
  },
  {
    name: "Special shift",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 6,
  },
  {
    name: "Shift 1",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 7,
  },
  {
    name: "Shift 2",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 8,
  },
  {
    name: "Special shift",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 9,
  },
  {
    name: "Shift 1",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 10,
  },
  {
    name: "Shift 2",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 11,
  },
  {
    name: "Special shift",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 12,
  },
];
const notifications = [
  {
    type: "error",
    person: "Patty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 1,
  },
  {
    type: "trade",
    person: "Bob Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 2,
  },
  {
    type: "error",
    person: "Kitty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 3,
  },
  {
    type: "trade",
    person: "Marry Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 4,
  },
  {
    type: "error",
    person: "John Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 5,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 6,
  },
  {
    type: "error",
    person: "Pat Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 7,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 8,
  },
  {
    type: "error",
    person: "Patty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 9,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 10,
  },
];

export default Home;
