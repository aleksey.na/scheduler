import React from "react";

// ** Components
import Avatar from "@components/avatar";
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import TimeRangePicker from "@components/TimeRangePicker";
import TimeRangeLine from "@components/TimeRangeLine";
import SelectInput from "@components/SelectInput";
import SVG from "@components/SVG";
import { OverlayTrigger, Popover } from "react-bootstrap";

// ** Styles
import styles from "./modal.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-13.jpg";
import AddPlate from "../../components/AddPlate/AddPlate";

function Modal({ isShown, onShow, daysOptions }) {
  return (
    <ModalFrame
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      <div className={styles.header}>Add a Custom Shift</div>

      <div className={styles.inputs}>
        <Input
          name={"shift"}
          label={"Name of the special shift"}
          value={"Thursday 24 HR"}
          containerClass={styles.shiftInput}
          className={styles.input}
          // onChange={handleInputChange}
        />
        <SelectInput
          name={"day"}
          label={"Select day"}
          value={"Thursday"}
          options={daysOptions}
          containerClass={styles.dayInput}
          // onChange={handleInputChange}
        />
      </div>

      <div className={`${styles.timePecker}`}>
        <TimeRangePicker
          // state={state}
          // onChange={handleInputChange}
          mainLabel={"Edit Restrictions"}
          btnDisabled={true}
          // onStateChange={setState}
        />
      </div>

      <div className={`${styles.lineRange}`}>
        <TimeRangeLine />
      </div>

      <div className={styles.statistics}>
        <div className={styles.titleContainer}>
          <div>Replacement Shift for Statistics</div>
          <OverlayTrigger
            trigger={["hover", "focus"]}
            placement="top"
            overlay={
              <Popover id={`popover-positioned-top`}>Some information</Popover>
            }
          >
            <div className={styles.tradeMarker}>
              <SVG.Information />
            </div>
          </OverlayTrigger>
        </div>

        <div className={styles.blocks}>
          <div className={styles.block}>
            <div className={`${styles.shiftName} ${styles.row}`}>
              <div>Friday 24</div>
              <SVG.Dots style={{ cursor: "pointer" }} />
            </div>

            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Start:</div>
              <div>06:00 AM</div>
            </div>
            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Finish:</div>
              <div>
                06:00 AM
                <br />
                next day
              </div>
            </div>
            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Duration:</div>
              <div>24h</div>
            </div>
          </div>

          <div className={styles.block}>
            <div className={`${styles.shiftName} ${styles.row}`}>
              <div>Saturday 24</div>
              <SVG.Dots style={{ cursor: "pointer" }} />
            </div>

            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Start:</div>
              <div>06:00 AM</div>
            </div>
            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Finish:</div>
              <div>
                06:00 AM
                <br />
                next day
              </div>
            </div>
            <div className={`${styles.smallText} ${styles.row}`}>
              <div>Duration:</div>
              <div>24h</div>
            </div>
          </div>
        </div>
      </div>

      <div className={styles.partners}>
        <div className={`${styles.partnersTitle}`}>
          <div className={`${styles.titleContainer}`}>
            <div>Partners</div>
            <OverlayTrigger
              trigger={["hover", "focus"]}
              placement="top"
              overlay={
                <Popover id={`popover-positioned-top`}>
                  Some information
                </Popover>
              }
            >
              <div className={styles.tradeMarker}>
                <SVG.Information />
              </div>
            </OverlayTrigger>
          </div>

          <div className={styles.titleBtns}>
            <div className={styles.titleBtn}>
              <SVG.Search />
            </div>
            <div className={styles.titleBtn}>
              <SVG.Filters width={20} height={20} />
            </div>
          </div>
        </div>

        <div className={`${styles.blocks} ${styles.partnersBlocks}`}>
          <AddPlate />

          {partners.map((partner) => (
            <div key={partner.id} className={`${styles.card}`}>
              <Avatar
                className={styles.avatar}
                img={defaultAvatar}
                imgHeight="50"
                imgWidth="50"
              />
              <div>
                <div className={styles.name}>{partner.name}</div>
                <div className={styles.level}>Level {partner.level}</div>
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className={styles.buttons}>
        <Button onClick={() => onShow()}>Save</Button>
        <div onClick={() => onShow()} className={styles.cancelButton}>
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

const partners = [
  {
    name: "John Doe",
    avatar: "",
    level: 2,
    id: 1,
  },
  {
    name: "Mike Wayne",
    avatar: "",
    level: 3,
    id: 2,
  },
];

export default Modal;
