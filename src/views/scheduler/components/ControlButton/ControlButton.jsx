import React from "react";
import PropTypes from "prop-types";

// ** Styles
import styles from "./controlButton.module.scss";

function ControlButton({ children, className, handleClick }) {
  return (
    <div onClick={handleClick} className={`${className} ${styles.container}`}>
      {children}
    </div>
  );
}

ControlButton.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  isLoading: PropTypes.bool,
  handleClick: PropTypes.func,
};

ControlButton.defaultProps = {
  children: null,
  className: "",
  isLoading: false,
  handleClick: () => {},
};

export default ControlButton;
