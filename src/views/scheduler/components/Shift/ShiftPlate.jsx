import React from "react";

// ** Components
import Avatar from "@components/avatar";

// ** Styles
import styles from "./shiftPlate.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-9.jpg";

function ShiftPlate({ shift }) {
  return (
    <div className={styles.shiftCard}>
      <div style={{ background: shift.color }} className={styles.color} />
      <div className={`${styles.firstSection} ${styles.section}`}>
        <div className={styles.name}>{shift.name}</div>

        <div className={styles.userData}>
          <Avatar
            className={styles.avatar}
            img={shift?.person?.avatar || defaultAvatar}
            imgHeight="50"
            imgWidth="50"
            status={shift?.person?.status}
          />
          <div className={styles.info}>
            <div className={styles.name}>{shift?.person?.name}</div>
            <div className={styles.level}>Level {shift?.person?.level}</div>
          </div>
        </div>
      </div>

      <div className={`${styles.section}`}>
        <div className={styles.smallText}>Start</div>
        {shift?.start ? shift?.start : "-"}
      </div>
      <div className={`${styles.section}`}>
        <div className={styles.smallText}>Finish</div>
        {shift?.finish ? shift?.finish : "-"}
      </div>
      <div className={`${styles.section}`}>
        <div className={styles.smallText}>Duration</div>
        {shift?.duration ? shift.duration + " h" : "-"}
      </div>
    </div>
  );
}

export default ShiftPlate;
