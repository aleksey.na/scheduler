import React from "react";

// ** Components
import SVG from "@components/SVG";

// ** Styles
import styles from "./addPlate.module.scss";

function AddPlate({ text, handleClick }) {
  return (
    <div onClick={handleClick} className={`${styles.container}`}>
      <div className={`${styles.button} ${text ? styles.margin : ""}`}>
        <SVG.Close className={styles.icon} />
      </div>
      <div>{text}</div>
    </div>
  );
}

export default AddPlate;
