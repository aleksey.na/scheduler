import React from "react";
import { PieChart, Pie, Cell } from "recharts";

import styles from "./statistics.module.scss";

function Graphs({ data, setActiveGraph }) {
  return (
    <div className={`${styles.graphs} flex-md-grow-2`}>
      {data?.map((item, index) => (
        <div
          className={styles.graphic}
          key={index}
          onClick={() => setActiveGraph(item)}
        >
          <PieChart width={180} height={180}>
            <text
              x={90}
              y={70}
              textAnchor="middle"
              dominantBaseline="middle"
              className={styles.text}
            >
              {item.title}
            </text>
            <text
              x={90}
              y={105}
              textAnchor="middle"
              dominantBaseline="middle"
              className={styles.graphText}
            >
              {`${
                item.hours ? item.hours + "h" : item.amount + "/" + item.total
              }`}
            </text>
            <Pie
              data={[
                {
                  name: item.title,
                  value: item?.hours || item?.amount,
                },
                {
                  name: item.title,
                  value: item?.hours
                    ? item.total - item.hours
                    : item.total - item.amount,
                },
              ]}
              cx={85}
              cy={85}
              // renderLabelItem={renderCustomizedLabel}
              innerRadius={60}
              outerRadius={80}
              cornerRadius={40}
              fill="#fff"
              dataKey="value"
              startAngle={-270}
            >
              <Cell key={`cell-${index}`} fill={item.color} />
            </Pie>
          </PieChart>
        </div>
      ))}
    </div>
  );
}

export default Graphs;
