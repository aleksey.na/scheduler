import React from "react";
import { Dropdown } from "react-bootstrap";
import Avatar from "@components/avatar";
import Input from "@components/Input";
import SVG from "@components/SVG";
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";
import styles from "./statistics.module.scss";
import moment from "moment";

function PartnerShifts({ data }) {
  const { person, shifts } = data;

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={styles.customToggle}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <SVG.Calendar fill="#6B7DAA" />
      {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={styles.flipDown} />
    </div>
  ));

  return (
    <>
      <div className={`${styles.partnerHeader} d-flex align-items-center`}>
        <Avatar
          img={defaultAvatar}
          imgHeight="100"
          imgWidth="100"
          className={`${styles.userAvatar} mr-1`}
        />
        <div className="ml-1">
          <div className={styles.personName}>{person.name}</div>
          <div className={styles.personLevel}>Level {person.level}</div>
        </div>
      </div>
      <div className="d-flex align-items-center justify-content-between">
        <h4>Week {moment().format("W")}</h4>
        <div>
          <Dropdown className={styles.dropdown}>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
              as={CustomToggle}
            >
              {moment().format("D MMM")} -{" "}
              {moment().add(14, "days").format("D MMM")}
            </Dropdown.Toggle>
            {/* <Dropdown.Menu></Dropdown.Menu> */}
          </Dropdown>
        </div>
      </div>
      <Input
        name="search"
        placeholder="Search"
        icon={<SVG.Search />}
        inputWrapperClass={styles.inputWrapper}
        className={`${styles.searchInput} mt-1 mb-3`}
        inputClass={styles.input}
        onChange={() => {}}
      />
      <div className={styles.shifts}>
        {shifts.map((item, index) => (
          <>
            <div className="mb-1">
              <span className={styles.date}>
                {moment(item.date).format("D MMMM, YYYY")}
              </span>
              <span className={styles.day}>
                {moment(item.date).format("dddd")}
              </span>
            </div>
            <div key={index} className={`${styles.shiftContainer} mr-1 pr-1`}>
              <div style={{background: item.color}} className={`${styles.colorLine} flex-grow-0`}></div>
              <div className={`${styles.shiftName} flex-grow-1 w-25`}>{item.name}</div>
              <div className={`${styles.start} flex-grow-1`}>
                <div className={styles.title}>Start</div>
                <div className={styles.value}>{item.start}</div>
              </div>
              <div className={`${styles.finish} flex-grow-1`}>
                <div className={styles.title}>Finish</div>
                <div className={styles.value}>{item.finish}</div>
              </div>
              <div className={`${styles.duration} flex-grow-1`}>
                <div className={styles.title}>Duration</div>
                <div className={styles.value}>{item.finish}</div>
              </div>
            </div>
          </>
        ))}
      </div>
    </>
  );
}

export default PartnerShifts;
