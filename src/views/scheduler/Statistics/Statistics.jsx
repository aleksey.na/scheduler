import React, { useState } from "react";
import { Form, Dropdown } from "react-bootstrap";
import { PieChart, Pie, Cell } from "recharts";
// import Breadcrumbs from "@components/Breadcrumbs";
import SVG from "@components/SVG";
import Input from "@components/Input";
import Avatar from "@components/avatar";
import Calendar from "@components/Calendar";
import Button from "@components/Button";
import Modal from "@components/Modal";
import PartnerShifts from "./PartnerShifts";
import Graphs from "./Graphs";
import styles from "./statistics.module.scss";
import moment from "moment";
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";
import { active } from "sortablejs";

function Statistics() {
  const [activePartner, setActivePartner] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [activeView, setActiveView] = useState("partner");
  const [activeGraph, setActiveGraph] = useState(null);
  const [activeDate, setActiveDate] = useState(null);

  const handleDateClick = (date) => {
    setIsModalOpen(true);
    setActiveDate(date);
  };

  //mock statistics
  const statistics = [
    {
      title: "Shift 1",
      hours: 150,
      total: 300,
      color: "#6045FF",
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ],
    },
    {
      title: "Shift 2",
      hours: 241,
      total: 300,
      color: "#45C9E4",
      shifts: [
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 5,
        },
        {
          title: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          date: new Date(),
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
          key: 6,
        },
      ],
    },
    {
      color: "#6045FF",
      title: "Saturday 24",
      amount: 14,
      total: 20,
    },
    {
      title: "Sunday Shifts",
      color: "#F0319D",
      amount: 4,
      total: 20,
    },
    {
      title: "Friday 24",
      color: "#F0319D",
      amount: 11,
      total: 20,
    },
    {
      title: "Vacations",
      color: "#F0319D",
      amount: 1,
      total: 3,
    },
    {
      title: "Wellness Days",
      color: "#B3C1E3",
      amount: 3,
      total: 4,
    },
    {
      title: "Days Off",
      color: "#B3C1E3",
      amount: 10,
      total: 14,
    },
  ];

  //mock members
  const members = ["Petty Partner", "Fred Partner", "James Partner"];

  //mock shifts
  const shifts = [
    {
      date: new Date(),
      shifts: [
        {
          name: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
        },
        {
          name: "Special Shift",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#F0319D",
          person: { name: "Fred Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "James Partner" },
        },
      ],
    },
    {
      date: moment().add(1, "days"),
      shifts: [
        {
          name: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "Fred Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "James Partner" },
        },
      ],
    },
    {
      date: moment().add(2, "days"),
      shifts: [
        {
          name: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "Fred Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "James Partner" },
        },
      ],
    },
    {
      date: moment().add(3, "days"),
      shifts: [
        {
          name: "Shift 1",
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
          person: { name: "Patty Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "Fred Partner" },
        },
        {
          name: "Shift 2",
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
          person: { name: "James Partner" },
        },
      ],
    },
  ];

  //mock shifts for person
  const personShifts = [
    {
      person: { name: "Petty Partner", level: 2 },
      shifts: [
        {
          name: "Shift 1",
          date: new Date(),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Shift 1",
          date: moment().add(1, "days"),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Shift 1",
          date: moment().add(2, "days"),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Special Shift",
          date: moment().add(3, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#F0319D",
        },
        {
          name: "Shift 2",
          date: moment().add(4, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
        },
      ],
    },
    {
      person: { name: "Fred Partner", level: 1 },
      shifts: [
        {
          name: "Shift 2",
          date: new Date(),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Shift 1",
          date: moment().add(2, "days"),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Special Shift",
          date: moment().add(3, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#F0319D",
        },
        {
          name: "Shift 2",
          date: moment().add(4, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
        },
      ],
    },
    {
      person: { name: "James Partner", level: 3 },
      shifts: [
        {
          name: "Shift 1",
          date: new Date(),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Shift 1",
          date: moment().add(2, "days"),
          start: "8:00 AM",
          finish: "8:00 PM",
          duration: "12h",
          color: "#6045FF",
        },
        {
          name: "Sunday shift",
          date: moment().add(3, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#F0319D",
        },
        {
          name: "Shift 2",
          date: moment().add(4, "days"),
          start: "8:00 PM",
          finish: "8:00 AM",
          duration: "12h",
          color: "#45C9E4",
        },
      ],
    },
  ];

  const items = [
    "Statistics",
    `${activePartner ? activePartner?.person?.name : "All"}`,
    `${activePartner && activeGraph ? activeGraph?.title : ""}`,
  ];

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={styles.customToggle}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <SVG.Calendar fill="#6B7DAA" />
      {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={styles.flipDown} />
    </div>
  ));

  return (
    <div>
      {/* <Breadcrumbs items={items} /> */}
      <div className={styles.container}>
        <div className={styles.filters}>
          <div className="d-flex justify-content-between mb-1">
            <h4>Filter</h4>
            <div className="d-flex">
              <div
                onClick={() => setActiveView("clock")}
                className={`${styles.buttonSvg} ${
                  activeView === "clock" ? styles.activeButton : ""
                }`}
              >
                <SVG.Clock />
              </div>
              <div
                onClick={() => setActiveView("partner")}
                className={`${styles.buttonSvg} ${
                  activeView === "partner" ? styles.activeButton : ""
                }`}
              >
                <SVG.Profile fill="#B3C1E3" />
              </div>
            </div>
          </div>
          {activeView === "partner" ? (
            <div className={styles.animatedView} key="partners">
              <Input
                name="search"
                placeholder="Search"
                icon={<SVG.Search />}
                inputWrapperClass={styles.inputWrapper}
                className={styles.searchInput}
                inputClass={styles.input}
                onChange={() => {}}
              />
              <div className={styles.members}>
                <Form>
                  {members.map((member, index) => (
                    <div key={index}>
                      <Form.Check
                        name={member}
                        id={index + 1}
                        checked={activePartner?.person?.name == member}
                        onChange={(e) =>
                          e.target.checked
                            ? setActivePartner(
                                personShifts.find(
                                  (item) => item.person.name === e.target.name
                                )
                              )
                            : setActivePartner(null)
                        }
                      ></Form.Check>
                      <label for={index + 1} className={styles.name}>
                        {member}
                      </label>
                    </div>
                  ))}
                </Form>
              </div>
            </div>
          ) : (
            <div
              key="calendar"
              className={`${styles.animatedView} d-flex flex-column justify-content-between align-items-center h-100`}
            >
              <Calendar />
              <Button className={styles.selectButton}>Select</Button>
            </div>
          )}
        </div>
        {activeGraph && activePartner ? (
          <div className="d-flex flex-grow-1">
            <div className={styles.leftColumn}>
              <div className={styles.graphicBig}>
                <PieChart width={240} height={240}>
                  <text
                    x={120}
                    y={105}
                    textAnchor="middle"
                    dominantBaseline="middle"
                    className={styles.text}
                  >
                    {activeGraph.title}
                  </text>
                  <text
                    x={120}
                    y={140}
                    textAnchor="middle"
                    dominantBaseline="middle"
                    className={styles.graphText}
                  >
                    {`${
                      activeGraph.hours
                        ? activeGraph.hours + "h"
                        : activeGraph.amount + "/" + activeGraph.total
                    }`}
                  </text>
                  <Pie
                    data={[
                      {
                        name: activeGraph.title,
                        value: activeGraph?.hours || activeGraph?.amount,
                      },
                      {
                        name: activeGraph.title,
                        value: activeGraph?.hours
                          ? activeGraph.total - activeGraph.hours
                          : activeGraph.total - activeGraph.amount,
                      },
                    ]}
                    cx={115}
                    cy={115}
                    innerRadius={85}
                    outerRadius={110}
                    cornerRadius={40}
                    fill="#fff"
                    dataKey="value"
                    startAngle={-270}
                  >
                    <Cell fill={activeGraph.color} />
                  </Pie>
                </PieChart>
              </div>
              <div className={styles.activeGraphInfo}>
                <div className={styles.title}>{activeGraph.title}</div>
                <div className="d-flex justify-content-between align-items-center mt-1">
                  <div className={styles.dateTitle}>Date:</div>
                  <Dropdown className={`${styles.dropdown} mr-0`}>
                    <Dropdown.Toggle
                      variant="success"
                      id="dropdown-basic"
                      as={CustomToggle}
                    >
                      {moment().format("D MMMM")}
                    </Dropdown.Toggle>
                  </Dropdown>
                </div>
                <div className={styles.titleReq}>Requirements for Partners</div>
                <div className="d-flex">
                  <div>
                    <div className={styles.inputHeading}>Min</div>
                    <Input
                      className={`${styles.searchInput} ${styles.widthSmall} mr-1`}
                      inputClass={styles.input}
                    />
                  </div>
                  <div>
                    <div className={styles.inputHeading}>Date Added</div>
                    <Input
                      icon={<SVG.Calendar />}
                      inputWrapperClass={styles.inputWrapper}
                      className={`${styles.searchInput} ${styles.widthSmall}`}
                      inputClass={styles.input}
                    />
                  </div>
                </div>
                <div className="d-flex mt-2">
                  <Button className={styles.cancelButton} variant={"secondary"}>
                    Cancel
                  </Button>
                  <Button>Save</Button>
                </div>
              </div>
            </div>
            <div className={styles.rightColumn}>
              <div
                className={`${styles.partnerHeader} d-flex align-items-center`}
              >
                <Avatar
                  img={defaultAvatar}
                  imgHeight="100"
                  imgWidth="100"
                  className={`${styles.userAvatar} mr-1`}
                />
                <div className="ml-1">
                  <div className={styles.personName}>
                    {activePartner?.person?.name}
                  </div>
                  <div className={styles.personLevel}>
                    Level {activePartner?.person?.level}
                  </div>
                </div>
              </div>
              <div>
                {activePartner?.shifts.map((shift, index) => (
                  <div
                    key={index}
                    className={styles.shiftContainer}
                    onClick={() => handleDateClick(shift.date)}
                  >
                    <div>
                      <div className={styles.title}>
                        {moment(shift.date).format("dddd")}
                      </div>
                      <div className={styles.dateValue}>
                        {moment(shift.date).format("D MMMM, YYYY")}
                      </div>
                    </div>
                    <div className={styles.start}>
                      <div className={styles.title}>Start</div>
                      <div className={styles.timeValue}>{shift.start}</div>
                    </div>
                    <div className={styles.end}>
                      <div className={styles.title}>Finish</div>
                      <div className={styles.timeValue}>{shift.finish}</div>
                    </div>
                    <div>
                      <div className={styles.title}>Duration</div>
                      <div className={styles.timeValue}>{shift.duration}</div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : (
          <>
            <Graphs data={statistics} setActiveGraph={setActiveGraph} />
            <div className={styles.peopleList}>
              {activePartner ? (
                <PartnerShifts data={activePartner} />
              ) : (
                <>
                  <div className="d-flex justify-content-between flex-wrap align-items-center mb-1">
                    <h4>Week {moment().format("W")}</h4>
                    <div>
                      <Dropdown className={styles.dropdown}>
                        <Dropdown.Toggle
                          variant="success"
                          id="dropdown-basic"
                          as={CustomToggle}
                        >
                          {moment().format("D MMM")} -{" "}
                          {moment().add(14, "days").format("D MMM")}
                        </Dropdown.Toggle>
                        {/* <Dropdown.Menu></Dropdown.Menu> */}
                      </Dropdown>
                    </div>
                  </div>
                  <Input
                    name="search"
                    placeholder="Search"
                    icon={<SVG.Search />}
                    inputWrapperClass={styles.inputWrapper}
                    className={styles.searchInput}
                    inputClass={styles.input}
                    onChange={() => {}}
                  />
                  <div className={styles.partnersWrapper}>
                    {shifts.map((shift, index) => (
                      <div key={index} className="mb-2">
                        <div className="mb-1">
                          <span className={styles.date}>
                            {moment(shift.date).format("D MMMM, YYYY")}
                          </span>
                          <span className={styles.day}>
                            {moment(shift.date).format("dddd")}
                          </span>
                        </div>
                        {shift.shifts.map((item, index) => (
                          <div
                            key={index}
                            className={`${styles.shiftWrapper} mr-1 pr-1`}
                          >
                            <div
                              className={styles.colorLine}
                              style={{ background: item.color }}
                            ></div>
                            <div className={styles.shiftName}>{item.name}</div>
                            <div className={styles.partnerData}>
                              <Avatar
                                img={defaultAvatar}
                                imgHeight="36"
                                imgWidth="36"
                                className={`${styles.shiftPopoverAvatar} mr-1`}
                              />
                              <div>
                                <div className={styles.personName}>
                                  {item.person.name}
                                </div>
                                <div className={styles.personTime}>
                                  {item.start} - {item.finish} | {item.duration}
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    ))}
                  </div>
                </>
              )}
            </div>
          </>
        )}
      </div>
      <Modal
        show={isModalOpen}
        onHide={setIsModalOpen}
        contentStyles={styles.statisticsModal}
      >
        <div className={styles.modalHeader}>
          <div className={styles.day}>{moment(activeDate).format("dddd")}</div>
          <div className={styles.date}>
            {moment(activeDate).format("D MMMM, YYYY")}
          </div>
        </div>
        <Input
          name="search"
          placeholder="Search"
          icon={<SVG.Search />}
          inputWrapperClass={`${styles.inputWrapper}`}
          className={`${styles.searchInput} mt-1 mb-3`}
          inputClass={styles.input}
          onChange={() => {}}
        />
        <div>
          {/* show mock data for first date in array */}
          {shifts[0]?.shifts.map((item, index) => (
            <div key={index} className={`${styles.shiftWrapper} pr-1`}>
              <div
                className={styles.colorLine}
                style={{ background: item.color }}
              ></div>
              <div className={styles.shiftName}>{item.name}</div>
              <div className={styles.partnerData}>
                <Avatar
                  img={defaultAvatar}
                  imgHeight="36"
                  imgWidth="36"
                  className={`${styles.shiftPopoverAvatar} mr-1`}
                />
                <div>
                  <div className={styles.personName}>{item.person.name}</div>
                  <div className={styles.personTime}>
                    {item.start} - {item.finish} | ({item.duration})
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className={styles.cancelButton}>
          <Button onClick={() => setIsModalOpen(false)}>Cancel</Button>
        </div>
      </Modal>
    </div>
  );
}

export default Statistics;
