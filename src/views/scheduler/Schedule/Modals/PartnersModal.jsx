import React, { useEffect, useState } from "react";

// ** Components
import Avatar from "@components/avatar";
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import Textarea from "@components/Textarea";
import SelectInput from "@components/SelectInput";
import SVG from "@components/SVG";
import ControlButton from "../../components/ControlButton/ControlButton";
import AddPlate from "../../components/AddPlate/AddPlate";

// ** Styles
import styles from "./modal.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-16.jpg";
import userAvatarPlaceholder from "@src/assets/images/icons/avatar-placeholder.png";

const Plate = ({ item, onEditNav, id }) => {
  return (
    <div className={`${styles.partnerPlate} ${styles.plate}`}>
      <div className={styles.userData}>
        <Avatar
          img={defaultAvatar}
          imgHeight="50"
          imgWidth="50"
          status="online"
          className={styles.avatar}
        />
        <div className={styles.text}>
          <div>{item.name}</div>
          <div>Level {item.level}</div>
        </div>
      </div>
      <div className={styles.controls}>
        <ControlButton
          handleClick={() => onEditNav(id)}
          className={styles.control}
        >
          <SVG.Pencil width={17} height={17} />
        </ControlButton>
        <ControlButton className={styles.control}>
          <SVG.Bin width={17} height={17} />
        </ControlButton>
      </div>
    </div>
  );
};

const EditContent = ({ onBackNav, state }) => {
  const levelOptions = new Array(4).fill("").map((level, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  return (
    <>
      <div className={styles.headerContainer}>
        <div className={styles.header}>
          <div>Add/Remove Partners</div>
          {state ? <div>{`"${state?.name}"`}</div> : ""}
        </div>
        <div onClick={onBackNav} className={styles.backBtn}>
          <div className={styles.arrow}>
            <SVG.ArrowDown />
          </div>
          Back
        </div>
      </div>
      <div className={styles.contentContainer}>
        <div className={styles.editAvatar}>
          <input name="avatar" type="file" onChange={() => {}} />
          <img src={userAvatarPlaceholder} alt="user avatar" />
        </div>
        <Input
          name="name"
          label={"Partner"}
          placeholder={"Partner name"}
          value={""}
          className={styles.vacationName}
        />
        <SelectInput
          name="level"
          label={"Level"}
          placeholder={"Select level"}
          value={""}
          options={levelOptions}
          width={100}
          required
          className={styles.vacationName}
        />
        <Textarea
          name="description"
          label={"Description"}
          value={""}
          rows={9}
        />
        <div className={styles.linkContainer}>
          <div className={styles.label}>Invite Partner</div>
          <div
            // onClick={() => handleCopyLink(state.link)}
            onClick={() => {}}
            className={styles.field}
          >
            <div className={styles.link}>
              https://www.flaticon.com/copy-link-here
            </div>
            <div className={styles.iconWrapper}>
              <SVG.Link className={styles.icon} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

function Modal({
  list,
  path,
  isShown,
  onShow,
  onEditNav,
  onAddNav,
  onBackNav,
  uid,
}) {
  const [state, setState] = useState("");
  const isEditMode = uid;

  useEffect(() => {
    if (uid) {
      const target = list?.find((elem) => elem.id === +uid);
      if (target) setState(target);
    }
  }, [uid]);

  const handleAddElem = () => {
    setState("");
    onAddNav();
  };

  return (
    <ModalFrame
      name={path}
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      {isEditMode ? (
        // pass state here if we open Edit mode
        <EditContent onBackNav={onBackNav} state={state} />
      ) : (
        <div className={styles.contentContainer}>
          <div className={styles.header}>Add/Remove Partners</div>
          <div className={styles.scrollContainer}>
            {list.map((partner) => (
              <Plate
                key={partner.id}
                item={partner}
                onEditNav={onEditNav}
                id={partner.id}
              />
            ))}
            <AddPlate text={"Add Partners"} handleClick={handleAddElem} />
          </div>
        </div>
      )}

      <div className={styles.buttons}>
        <Button onClick={isEditMode ? onBackNav : () => onShow(path)}>
          Save
        </Button>
        <div
          onClick={isEditMode ? onBackNav : () => onShow(path)}
          className={styles.cancelButton}
        >
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

export default Modal;
