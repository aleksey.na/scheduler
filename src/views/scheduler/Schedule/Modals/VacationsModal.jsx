import React, { useEffect, useState } from "react";

// ** Components
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import Textarea from "@components/Textarea";
import SelectInput from "@components/SelectInput";
import SVG from "@components/SVG";
import ControlButton from "../../components/ControlButton/ControlButton";
import AddPlate from "../../components/AddPlate/AddPlate";

// ** Styles
import styles from "./modal.module.scss";
import common from "../schedule.module.scss";

const Plate = ({ elem, onEditNav, id }) => {
  return (
    <div className={`${styles.vacationPlate} ${styles.plate}`}>
      <div className={common.section}>
        <div className={`${common.subtitle} ${common.nameSubtitle}`}>Item</div>
        <div className={common.itemName}>{elem.name}</div>
      </div>

      <div className={`${common.section} ${common.secSection}`}>
        <div className={common.subtitle}>Consecutive</div>
        <div className={common.secSection}>
          {`${elem.consecutive} ${
            elem.consecutive > 1 ? "consecutive days" : "day only"
          }`}
        </div>
      </div>

      <div className={common.section}>
        <div className={common.subtitle}>Count allowed</div>
        <div className={common.thSection}>{elem.allowed} allowed</div>
      </div>

      <div className={`${styles.holidayControls}`}>
        <ControlButton
          handleClick={() => onEditNav(id)}
          className={styles.control}
        >
          <SVG.Pencil width={17} height={17} />
        </ControlButton>
        <ControlButton className={styles.control}>
          <SVG.Bin width={17} height={17} />
        </ControlButton>
      </div>
    </div>
  );
};

const EditContent = ({ onBackNav, state }) => {
  const types = {
    Day: "Day",
    Week: "Week",
    Month: "Month",
  };
  const typesOptions = Object.entries(types).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  const durationOptions = new Array(31).fill("").map((_, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  const consecutiveTypes = {
    Yes: "Yes",
    No: "No",
  };
  const consecutiveOptions = Object.entries(consecutiveTypes).map(
    ([key, value]) => ({
      value: key,
      label: value,
    })
  );

  const allowedOptions = new Array(31).fill("").map((level, index) => ({
    value: index + 1,
    label: index + 1,
  }));

  return (
    <>
      <div className={styles.headerContainer}>
        <div className={styles.header}>
          <div>Edition personal item</div>
          {state ? <div>{`"${state?.name}"`}</div> : ""}
        </div>
        <div onClick={onBackNav} className={styles.backBtn}>
          <div className={styles.arrow}>
            <SVG.ArrowDown />
          </div>
          Back
        </div>
      </div>
      <div className={styles.contentContainer}>
        <Input
          name="name"
          label={"Item name"}
          value={""}
          className={styles.vacationName}
          // onChange={handleInputChange}
        />
        <SelectInput
          name="holidayType"
          label={"Type"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={typesOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />

        <SelectInput
          name="holidayDuration"
          label={"Duration"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={durationOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />

        <SelectInput
          name="consecutive"
          label={"Consecutive"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={consecutiveOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />
        <SelectInput
          name="holidayAllowed"
          label={"Allowed"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={allowedOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />
      </div>
    </>
  );
};

function Modal({
  list,
  path,
  isShown,
  onShow,
  onEditNav,
  onAddNav,
  onBackNav,
  uid,
}) {
  const [state, setState] = useState("");
  const isEditMode = uid;

  useEffect(() => {
    if (uid) {
      const target = list?.find((elem) => elem.id === +uid);
      if (target) setState(target);
    }
  }, [uid]);

  const handleAddElem = () => {
    setState("");
    onAddNav();
  };

  return (
    <ModalFrame
      name={path}
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      {isEditMode ? (
        // pass state here if we open Edit mode
        <EditContent onBackNav={onBackNav} state={state} />
      ) : (
        <div className={styles.contentContainer}>
          <div className={styles.header}>Holidays</div>
          <div className={styles.scrollContainer}>
            {list.map((elem) => (
              <Plate
                key={elem.id}
                elem={elem}
                onEditNav={onEditNav}
                id={elem.id}
                // location={location}
              />
            ))}
            <AddPlate
              text={"Add New Personal Item"}
              handleClick={handleAddElem}
            />
          </div>
        </div>
      )}

      <div className={styles.buttons}>
        <Button onClick={isEditMode ? onBackNav : () => onShow(path)}>
          Save
        </Button>
        <div
          onClick={isEditMode ? onBackNav : () => onShow(path)}
          className={styles.cancelButton}
        >
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

export default Modal;
