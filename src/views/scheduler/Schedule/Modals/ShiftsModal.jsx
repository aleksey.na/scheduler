import React, { useEffect, useState } from "react";

// ** Components
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import SelectInput from "@components/SelectInput";
import Textarea from "@components/Textarea";
import TimeRangeLine from "@components/TimeRangeLine";
import TimeRangePicker from "@components/TimeRangePicker";
import SVG from "@components/SVG";
import ControlButton from "../../components/ControlButton/ControlButton";
import AddPlate from "../../components/AddPlate/AddPlate";

// ** Styles
import styles from "./modal.module.scss";
import shiftStyles from "../../components/Shift/shiftPlate.module.scss";

const Plate = ({ elem, onEditNav, id }) => {
  return (
    <div className={`${shiftStyles.shiftCard} ${styles.shiftCard}`}>
      <div style={{ background: elem.color }} className={shiftStyles.color} />
      <div className={shiftStyles.name}>{elem.name}</div>

      <div className={`${shiftStyles.section}`}>
        <div className={shiftStyles.smallText}>Start</div>
        {elem?.start ? elem?.start : "-"}
      </div>
      <div className={`${shiftStyles.section}`}>
        <div className={shiftStyles.smallText}>Finish</div>
        {elem?.finish ? elem?.finish : "-"}
      </div>
      <div className={`${shiftStyles.section}`}>
        <div className={shiftStyles.smallText}>Duration</div>
        {elem?.duration ? elem.duration + " h" : "-"}
      </div>

      <div className={`${styles.holidayControls} ${styles.shiftsControls}`}>
        <ControlButton
          handleClick={() => onEditNav(id)}
          className={styles.control}
        >
          <SVG.Pencil width={17} height={17} />
        </ControlButton>
        <ControlButton className={styles.control}>
          <SVG.Bin width={17} height={17} />
        </ControlButton>
      </div>
    </div>
  );
};

const EditContent = ({ onBackNav, state, type, uid }) => {
  return (
    <>
      <div className={styles.headerContainer}>
        <div className={styles.header}>
          <div>
            {type === "shift"
              ? `${uid.length > 1 ? "Add" : "Edition"} shift`
              : `${uid.length > 1 ? "Add" : "Edition"} special shift`}
          </div>
          {state ? <div>{`"${state?.name}"`}</div> : ""}
        </div>
        <div onClick={onBackNav} className={styles.backBtn}>
          <div className={styles.arrow}>
            <SVG.ArrowDown />
          </div>
          Back
        </div>
      </div>
      <div className={styles.contentContainer}>
        <Input
          name="holidayName"
          label={"Holiday Name"}
          value={""}
          className={styles.vacationName}
        />

        {type !== "shift" && (
          <div className={styles.inputRow}>
            <SelectInput
              name="day"
              label={"Select day"}
              options={[]}
              value={""}
            />
            <SelectInput
              name="timeOff"
              label={"Time Off"}
              options={[]}
              value={""}
            />
          </div>
        )}

        <div className={styles.timePicker}>
          <TimeRangePicker mainLabel={"Edit Restrictions"} btnDisabled={true} />
        </div>

        <div className={styles.timeLinePicker}>
          <TimeRangeLine />
        </div>

        <Textarea
          name="comments"
          label={"Comments"}
          placeholder={"Type comments "}
          value={""}
          rows={type !== "shift" ? 4 : 9}
        />
      </div>
    </>
  );
};

function Modal({
  list,
  path,
  isShown,
  onShow,
  onEditNav,
  onAddNav,
  onBackNav,
  uid,
}) {
  const [state, setState] = useState("");
  const [type, setType] = useState("shift");
  const isEditMode = uid;

  useEffect(() => {
    if (uid) {
      const target = list?.find((elem) => elem.id === +uid);
      if (target) {
        setType(target?.type);
        setState(target);
      }
    }
  }, [uid]);

  const handleAddElem = (type) => {
    if (type) setType(type);
    setState("");
    onAddNav();
  };

  const renderList = (listElems, type) => {
    let firstList = [],
      secondList = [];
    listElems.forEach((e) => {
      if (e.type === "shift") {
        firstList.push(e);
      } else {
        secondList.push(e);
      }
    });

    if (type) {
      return (
        <>
          <div className={styles.header}>Add/Remove Shifts</div>
          <div className={styles.scrollContainer}>
            <div className={styles.nestedContainer}>
              {firstList.map((elem) => (
                <Plate
                  key={elem.id}
                  elem={elem}
                  onEditNav={onEditNav}
                  id={elem.id}
                />
              ))}
              <AddPlate
                text={"Add New Shifts"}
                handleClick={() => handleAddElem("shift")}
              />
            </div>

            <div className={styles.header}>Add/Remove Special Shifts</div>
            <div className={styles.nestedContainer}>
              {secondList.map((elem) => (
                <Plate
                  key={elem.id}
                  elem={elem}
                  onEditNav={onEditNav}
                  id={elem.id}
                />
              ))}
              <AddPlate
                text={"Add Special Shift "}
                handleClick={handleAddElem}
              />
            </div>
          </div>
        </>
      );
    }

    return (
      <>
        <div className={styles.header}>Name</div>
        <div className={styles.scrollContainer}>
          {listElems.map((elem) => (
            <Plate
              key={elem.id}
              elem={elem}
              onEditNav={onEditNav}
              id={elem.id}
            />
          ))}
          <AddPlate text={"Add Holidays"} handleClick={handleAddElem} />
        </div>
      </>
    );
  };

  return (
    <ModalFrame
      name={path}
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      {isEditMode ? (
        // pass state here if we open Edit mode
        <EditContent
          onBackNav={onBackNav}
          state={state}
          type={type}
          uid={uid}
        />
      ) : (
        <div className={styles.contentContainer}>{renderList(list, type)}</div>
      )}

      <div className={styles.buttons}>
        <Button onClick={isEditMode ? onBackNav : () => onShow(path)}>
          Save
        </Button>
        <div
          onClick={isEditMode ? onBackNav : () => onShow(path)}
          className={styles.cancelButton}
        >
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

export default Modal;
