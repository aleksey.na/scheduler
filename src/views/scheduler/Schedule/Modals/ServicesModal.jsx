import React, { useState, useEffect } from "react";

// ** Components
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import SVG from "@components/SVG";
import ControlButton from "../../components/ControlButton/ControlButton";
import AddPlate from "../../components/AddPlate/AddPlate";

// ** Styles
import styles from "./modal.module.scss";
import common from "../schedule.module.scss";

const Plate = ({ item, onEditNav, id }) => {
  return (
    <div className={`${common.service} ${styles.servicePlate}`}>
      <div className={`${common.section}`}>
        <div className={`${common.subtitle} ${common.nameSubtitle}`}>
          Service
        </div>
        <div className={common.itemName}>{item.name}</div>
      </div>

      <div className={`${common.section}`}>
        <div className={common.subtitle}>Level 1</div>
        <div className={common.secSection}>{item.level_1}</div>
      </div>

      <div className={`${common.section}`}>
        <div className={common.subtitle}>Level 2</div>
        <div className={common.thSection}>{item.level_2}</div>
      </div>

      <div className={`${common.section}`}>
        <div className={common.subtitle}>Level 3</div>
        <div className={common.thSection}>{item.level_3}</div>
      </div>

      <div className={`${common.section}`} style={{ borderRight: "0px" }}>
        <div className={common.subtitle}>Level 4</div>
        <div className={common.thSection}>{item.level_4}</div>
      </div>
      <div>
        <div className={`${styles.holidayControls} ${styles.shiftsControls}`}>
          <ControlButton
            handleClick={() => onEditNav(id, "service")}
            className={`${styles.servicesControls}`}
          >
            <SVG.Pencil width={17} height={17} />
          </ControlButton>
          <ControlButton className={`${styles.servicesControl}`}>
            <SVG.Bin width={17} height={17} />
          </ControlButton>
        </div>
      </div>
    </div>
  );
};

const EditContent = ({ onBackNav, state }) => {
  return (
    <>
      <div className={styles.headerContainer}>
        <div className={`${styles.header} mb-1`}>
          <div>Edition Service</div>
          {state ? <div>{`"${state?.name}"`}</div> : ""}
        </div>
        <div onClick={onBackNav} className={styles.backBtn}>
          <div className={styles.arrow}>
            <SVG.ArrowDown />
          </div>
          Back
        </div>
      </div>
      <div className={styles.contentContainer}>
        <div className={styles.headerInput}>
          <Input
            label="Name of the service"
            value={state.name}
            containerClass={styles.input}
          />
          <Input label="Number of Services" value={13} />
        </div>
        {[...Array(4)].map((e, i) => (
          <div className={styles.servicesWrapper}>
            <div className={styles.title}>
              Level {4 - i}
              {"   "}
              <span className={styles.subTitle}>Partners</span>
            </div>
            <div className={styles.inputGroup}>
              <Input label="Minimum" value={1} containerClass={styles.input} />
              <Input label="Maximum" value={3} containerClass={styles.input} />
              <Input label="Min rotations" value={2} />
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

function ServicesModal({
  list,
  path,
  isShown,
  onShow,
  onEditNav,
  onAddNav,
  onBackNav,
  uid,
}) {
  const [state, setState] = useState("");
  const [type, setType] = useState("shift");
  const isEditMode = uid;

  useEffect(() => {
    if (uid) {
      const target = list?.find((elem) => elem.id === +uid);
      if (target) {
        setType(target?.type);
        setState(target);
      }
    }
  }, [uid]);

  const handleAddElem = (type) => {
    if (type) setType(type);
    setState("");
    onAddNav();
  };

  return (
    <ModalFrame
      name={path}
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      {isEditMode ? (
        <EditContent onBackNav={onBackNav} state={state} />
      ) : (
        <div className={styles.contentContainer}>
          <div className={styles.scrollContainer}>
            <div className={styles.header}>Add/Remove Services</div>
            {list.map((service) => (
              <Plate
                key={service.id}
                item={service}
                onEditNav={onEditNav}
                id={service.id}
              />
            ))}
            <AddPlate
              text={"Add New Block Requirement"}
              handleClick={handleAddElem}
            />
          </div>
        </div>
      )}
      <div className={styles.buttons}>
        <Button onClick={isEditMode ? onBackNav : () => onShow(path)}>
          Save
        </Button>
        <div
          onClick={isEditMode ? onBackNav : () => onShow(path)}
          className={styles.cancelButton}
        >
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

export default ServicesModal;
