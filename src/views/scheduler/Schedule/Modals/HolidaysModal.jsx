import React, { useEffect, useState } from "react";

// ** Components
import ModalFrame from "@components/Modal";
import Button from "@components/Button";
import Input from "@components/Input";
import Textarea from "@components/Textarea";
import SelectInput from "@components/SelectInput";
import SVG from "@components/SVG";
import ControlButton from "../../components/ControlButton/ControlButton";
import AddPlate from "../../components/AddPlate/AddPlate";

// ** Styles
import styles from "./modal.module.scss";
import common from "../schedule.module.scss";

const Plate = ({ elem, onEditNav, id }) => {
  return (
    <div className={`${styles.holidayPlate} ${styles.plate}`}>
      <div className={common.section}>
        <div className={`${common.subtitle} ${common.nameSubtitle}`}>Item</div>
        <div className={common.itemName}>{elem.name}</div>
      </div>

      <div className={`${common.section} ${common.secSection}`}>
        <div className={common.subtitle}>Day</div>
        <div className={common.secSection}>{elem.date}</div>
      </div>

      <div className={common.section}>
        <div className={common.subtitle}>Type</div>
        <div
          className={common.thSection}
          style={{
            color: elem.type === "major" ? "#2054d7" : "#29c510",
            textTransform: "capitalize",
          }}
        >
          {elem.type}
        </div>
      </div>

      <div className={`${styles.holidayControls}`}>
        <ControlButton
          handleClick={() => onEditNav(id)}
          className={styles.control}
        >
          <SVG.Pencil width={17} height={17} />
        </ControlButton>
        <ControlButton className={styles.control}>
          <SVG.Bin width={17} height={17} />
        </ControlButton>
      </div>
    </div>
  );
};

const EditContent = ({ onBackNav, state }) => {
  const holidayTypes = {
    Major: "Major",
    Minor: "Minor",
  };
  const typeOptions = Object.entries(holidayTypes).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  const excludeTypes = {
    Yes: "Yes",
    No: "No",
  };
  const excludeOptions = Object.entries(excludeTypes).map(([key, value]) => ({
    value: key,
    label: value,
  }));

  return (
    <>
      <div className={styles.headerContainer}>
        <div className={styles.header}>
          <div>Edition holiday</div>
          {state ? <div>{`"${state?.name}"`}</div> : ""}
        </div>
        <div onClick={onBackNav} className={styles.backBtn}>
          <div className={styles.arrow}>
            <SVG.ArrowDown />
          </div>
          Back
        </div>
      </div>
      <div className={styles.contentContainer}>
        <div className={styles.firstInputRow}>
          <Input
            name="holidayName"
            label={"Holiday Name"}
            value={""}
            // onChange={handleInputChange}
          />
          <SelectInput
            name="holidayDate"
            label={"Holiday Date"}
            options={[]}
            value={""}
            // onChange={handleInputChange}
            onChange={() => {}}
          />
        </div>

        <SelectInput
          name="holidayType"
          label={"Type"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={typeOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />

        <SelectInput
          name="holidayExclude"
          label={"Exclude Special Services"}
          labelDescription={
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
          }
          options={excludeOptions}
          value={""}
          containerClass={styles.inputRowBlock}
          // onChange={handleInputChange}
          onChange={() => {}}
        />
        <Textarea
          name="сomments"
          label={"Comments"}
          value={""}
          placeholder={"Type comments"}
          // onChange={handleInputChange}
          rows={9}
        />
      </div>
    </>
  );
};

function Modal({
  list,
  path,
  isShown,
  onShow,
  onEditNav,
  onAddNav,
  onBackNav,
  uid,
}) {
  const [state, setState] = useState("");
  const isEditMode = uid;

  useEffect(() => {
    if (uid) {
      const target = list?.find((elem) => elem.id === +uid);
      if (target) setState(target);
    }
  }, [uid]);

  const handleAddElem = () => {
    setState("");
    onAddNav();
  };

  return (
    <ModalFrame
      name={path}
      show={isShown}
      onHide={onShow}
      containerStyles={styles.containerWrapper}
      contentStyles={styles.contentWrapper}
    >
      {isEditMode ? (
        // pass state here if we open Edit mode
        <EditContent onBackNav={onBackNav} state={state} />
      ) : (
        <div className={styles.contentContainer}>
          <div className={styles.header}>Holidays</div>
          <div className={styles.scrollContainer}>
            {list.map((elem) => (
              <Plate
                key={elem.id}
                elem={elem}
                onEditNav={onEditNav}
                id={elem.id}
                // location={location}
              />
            ))}
            <AddPlate text={"Add Holidays"} handleClick={handleAddElem} />
          </div>
        </div>
      )}

      <div className={styles.buttons}>
        <Button onClick={isEditMode ? onBackNav : () => onShow(path)}>
          Save
        </Button>
        <div
          onClick={isEditMode ? onBackNav : () => onShow(path)}
          className={styles.cancelButton}
        >
          Cancel
        </div>
      </div>
    </ModalFrame>
  );
}

export default Modal;
