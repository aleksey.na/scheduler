import React, { useState, useEffect } from "react";
import { Link, useParams, useLocation, useHistory } from "react-router-dom";

// ** Third party libraries
// import moment from "moment";

// ** Components
import Avatar from "@components/avatar";
import ControlButton from "../components/ControlButton/ControlButton";
import Button from "@components/Button";
import SVG from "@components/SVG";
import {
  PartnersModal,
  HolidaysModal,
  VacationsModal,
  ShiftsModal,
  ServicesModal,
} from "./Modals";

// ** Styles
import styles from "./schedule.module.scss";
import common from "../../common.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-15.jpg";

function ShiftPlate({ shift }) {
  return (
    <div className={`${styles.shiftPlate}`}>
      <div
        style={{ background: shift.color }}
        className={`${styles.color} ${styles.shiftColor}`}
      />
      <div className={`${styles.firstBlock} ${styles.shiftBlock}`}>
        <div>{shift.name}</div>
      </div>

      <div className={`${styles.shiftBlock}`}>
        <div className={styles.smallText}>Start</div>
        {shift?.start ? shift?.start : "-"}
      </div>
      <div className={`${styles.shiftBlock}`}>
        <div className={styles.smallText}>Finish</div>
        {shift?.finish ? shift?.finish : "-"}
      </div>
      <div className={`${styles.shiftBlock}`}>
        <div className={styles.smallText}>Duration</div>
        {shift?.duration ? shift.duration : "-"}
      </div>
    </div>
  );
}

function Schedule() {
  const initialModalsState = {
    partners: false,
    services: false,
    shifts: false,
    vacations: false,
    holidays: false,
  };
  const [modalsState, setModalsState] = useState({ ...initialModalsState });

  const location = useLocation();
  const history = useHistory();
  const { modalPath: urlModalPath, uid: urlUid } = useParams();

  // ** Checks url & updates active modal
  useEffect(() => {
    if (urlModalPath) {
      handleModalToggle(urlModalPath);
    }
  }, [urlModalPath]);

  const urlGeneratorBack = (url, twoStepsBack) => {
    const parts = url.split("/");

    if (twoStepsBack) {
      parts.splice(parts.length - 2, 2);
    } else {
      parts.pop();
    }

    return parts.join("/");
  };

  const handleModalToggle = (path) => {
    if (path) {
      const isOpen = modalsState[path];
      setModalsState({
        ...modalsState,
        [path]: !isOpen,
      });
      if (isOpen) history.push(urlGeneratorBack(location.pathname, urlUid));
    }
  };

  const handleAddModeNav = () => {
    history.push(`${location.pathname}/add`);
  };

  const handleEditModeNav = (id) => {
    history.push(`${location.pathname}/${id}`);
  };

  const handlePrevMode = () => {
    history.push(urlGeneratorBack(location.pathname));
  };

  return (
    <div>
      <div className={styles.pageTitle}>
        <div>Schedule Maker</div>
        <Button>Generate Schedule</Button>
      </div>

      <div className={styles.pageContainer}>
        <div className={styles.contentColumn}>
          <div
            className={`${common.shadow} ${styles.card} ${styles.smallCard}`}
          >
            <div className={styles.cardTitle}>
              <div className={styles.text}>Partners</div>
              <div className={styles.controls}>
                <ControlButton>
                  <SVG.Filters width={17} height={17} fill="#2054d7" />
                </ControlButton>
                <Link to={`${location.pathname}/partners`}>
                  <Button
                    onClick={() => handleModalToggle(urlModalPath)}
                    className={styles.editBtn}
                  >
                    Edit
                  </Button>
                </Link>
              </div>
            </div>
            <div className={styles.partnerContent}>
              {partners.map((partner, index) => (
                <div key={index} className={styles.partnerPlate}>
                  <Avatar
                    img={defaultAvatar}
                    imgHeight="50"
                    imgWidth="50"
                    status="online"
                  />
                  <div className={styles.info}>
                    <div>{partner.name}</div>
                    <div>Level {partner.level}</div>
                  </div>
                </div>
              ))}
            </div>
            <div className={styles.blur} />
          </div>

          <div
            className={`${common.shadow} ${styles.card} ${styles.smallCard}`}
          >
            <div className={styles.cardTitle}>
              <div className={styles.text}>Services</div>
              <div className={styles.controls}>
                <ControlButton>
                  <SVG.Filters width={17} height={17} fill="#2054d7" />
                </ControlButton>
                <Link to={`${location.pathname}/services`}>
                  <Button
                    onClick={() => handleModalToggle(urlModalPath)}
                    className={styles.editBtn}
                  >
                    Edit
                  </Button>
                </Link>
              </div>
            </div>
            <div className={styles.serviceContent}>
              {services.map((service, index) => (
                <div key={index} className={styles.service}>
                  <div className={`${styles.section}`}>
                    <div
                      className={`${styles.subtitle} ${styles.nameSubtitle}`}
                    >
                      Service
                    </div>
                    <div className={styles.itemName}>{service.name}</div>
                  </div>

                  <div className={`${styles.section} ${styles.section}`}>
                    <div className={styles.subtitle}>Level 1</div>
                    <div className={styles.secSection}>{service.level_1}</div>
                  </div>

                  <div className={`${styles.section} ${styles.section}`}>
                    <div className={styles.subtitle}>Level 2</div>
                    <div className={styles.thSection}>{service.level_2}</div>
                  </div>

                  <div className={`${styles.section} ${styles.section}`}>
                    <div className={styles.subtitle}>Level 3</div>
                    <div className={styles.thSection}>{service.level_3}</div>
                  </div>

                  <div className={`${styles.section} ${styles.section}`}>
                    <div className={styles.subtitle}>Level 4</div>
                    <div className={styles.thSection}>{service.level_4}</div>
                  </div>
                </div>
              ))}
            </div>
            <div className={styles.blur} />
          </div>
        </div>

        <div className={`${common.shadow} ${styles.card} ${styles.largeCard}`}>
          <div className={styles.cardTitle}>
            <div className={styles.text}>Shifts</div>
            <div className={styles.controls}>
              <ControlButton>
                <SVG.Filters width={17} height={17} fill="#2054d7" />
              </ControlButton>
              <Link to={`${location.pathname}/shifts`}>
                <Button
                  onClick={() => handleModalToggle(urlModalPath)}
                  className={styles.editBtn}
                >
                  Edit
                </Button>
              </Link>
            </div>
          </div>
          <div className={styles.platesContainer}>
            {shifts.map((shift) => (
              <ShiftPlate shift={shift} key={shift.id} />
            ))}
          </div>
          <div className={styles.blur} />
        </div>

        <div className={styles.contentColumn}>
          <div
            className={`${common.shadow} ${styles.card} ${styles.smallCard}`}
          >
            <div className={styles.cardTitle}>
              <div className={styles.text}>Personal Items</div>
              <div className={styles.controls}>
                <ControlButton>
                  <SVG.Filters width={17} height={17} fill="#2054d7" />
                </ControlButton>
                <Link to={`${location.pathname}/vacations`}>
                  <Button
                    onClick={() => handleModalToggle(urlModalPath)}
                    className={styles.editBtn}
                  >
                    Edit
                  </Button>
                </Link>
              </div>
            </div>
            <div className={styles.itemsContent}>
              {vacations.map((item, index) => (
                <div key={index} className={styles.parsonalPlate}>
                  <div className={styles.section}>
                    <div
                      className={`${styles.subtitle} ${styles.nameSubtitle}`}
                    >
                      Item
                    </div>
                    <div className={styles.itemName}>{item.name}</div>
                  </div>

                  <div className={`${styles.section} ${styles.secSection}`}>
                    <div className={styles.subtitle}>Consecutive</div>
                    <div className={styles.secSection}>
                      {item.consecutive > 1
                        ? `${item.consecutive} consecutive days`
                        : `${item.consecutive} day only`}
                    </div>
                  </div>

                  <div className={styles.section}>
                    <div className={styles.subtitle}>Count allowed</div>
                    <div className={styles.thSection}>
                      {`${item.allowed} allowed`}
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className={styles.blur} />
          </div>

          <div
            className={`${common.shadow} ${styles.card} ${styles.smallCard}`}
          >
            <div className={styles.cardTitle}>
              <div className={styles.text}>Holidays</div>
              <div className={styles.controls}>
                <ControlButton>
                  <SVG.Filters width={17} height={17} fill="#2054d7" />
                </ControlButton>
                <Link to={`${location.pathname}/holidays`}>
                  <Button
                    onClick={() => handleModalToggle(urlModalPath)}
                    className={styles.editBtn}
                  >
                    Edit
                  </Button>
                </Link>
              </div>
            </div>
            <div className={styles.itemsContent}>
              {holidays.map((holiday) => (
                <div
                  key={holiday.id}
                  className={`${styles.parsonalPlate} ${styles.holidayPlate}`}
                >
                  <div className={styles.section}>
                    <div
                      className={`${styles.subtitle} ${styles.nameSubtitle}`}
                    >
                      Item
                    </div>
                    <div className={styles.itemName}>{holiday.name}</div>
                  </div>

                  <div className={`${styles.section} ${styles.secSection}`}>
                    <div className={styles.subtitle}>Day</div>
                    <div className={styles.secSection}>{holiday.date}</div>
                  </div>

                  <div className={styles.section}>
                    <div className={styles.subtitle}>Type</div>
                    <div
                      className={styles.thSection}
                      style={{
                        color: holiday.type === "major" ? "#2054d7" : "#29c510",
                        textTransform: "capitalize",
                      }}
                    >
                      {holiday.type}
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <div className={styles.blur} />
          </div>
        </div>
      </div>

      <PartnersModal
        list={partners}
        path={urlModalPath}
        uid={urlUid}
        isShown={modalsState.partners}
        onShow={handleModalToggle}
        onAddNav={handleAddModeNav}
        onEditNav={handleEditModeNav}
        onBackNav={handlePrevMode}
      />
      <HolidaysModal
        list={holidays}
        path={urlModalPath}
        uid={urlUid}
        isShown={modalsState.holidays}
        onShow={handleModalToggle}
        onAddNav={handleAddModeNav}
        onEditNav={handleEditModeNav}
        onBackNav={handlePrevMode}
      />

      <VacationsModal
        list={vacations}
        path={urlModalPath}
        uid={urlUid}
        isShown={modalsState.vacations}
        onShow={handleModalToggle}
        onAddNav={handleAddModeNav}
        onEditNav={handleEditModeNav}
        onBackNav={handlePrevMode}
      />

      <ShiftsModal
        list={shifts}
        path={urlModalPath}
        uid={urlUid}
        isShown={modalsState.shifts}
        onShow={handleModalToggle}
        onAddNav={handleAddModeNav}
        onEditNav={handleEditModeNav}
        onBackNav={handlePrevMode}
      />

      <ServicesModal
        list={services}
        path={urlModalPath}
        uid={urlUid}
        isShown={modalsState.services}
        onShow={handleModalToggle}
        onAddNav={handleAddModeNav}
        onEditNav={handleEditModeNav}
        onBackNav={handlePrevMode}
      />
    </div>
  );
}

const partners = [
  {
    name: "Fred Partner",
    level: 2,
    id: 1,
  },
  {
    name: "Alex Partner",
    level: 2,
    id: 2,
  },
  {
    name: "John Partner",
    level: 2,
    id: 3,
  },
  {
    name: "Patty Partner",
    level: 2,
    id: 4,
  },
  {
    name: "Fred Partner",
    level: 2,
    id: 5,
  },
  {
    name: "Alex Partner",
    level: 2,
    id: 6,
  },
  {
    name: "John Partner",
    level: 2,
    id: 7,
  },
  {
    name: "Patty Partner",
    level: 2,
    id: 8,
  },
  {
    name: "Fred Partner",
    level: 2,
    id: 9,
  },
  {
    name: "Alex Partner",
    level: 2,
    id: 10,
  },
  {
    name: "John Partner",
    level: 2,
    id: 11,
  },
  {
    name: "Patty Partner",
    level: 2,
    id: 12,
  },
];
const services = [
  {
    name: "Nights",
    level_1: "2-4",
    level_2: "1-2",
    level_3: "2-3",
    level_4: "1-4",
    id: 1,
  },
  {
    name: "GYN",
    level_1: "3-1",
    level_2: "2-3",
    level_3: "0",
    level_4: "1-1",
    id: 2,
  },
  {
    name: "OB",
    level_1: "2-4",
    level_2: "0",
    level_3: "2-3",
    level_4: "1-4",
    id: 3,
  },
  {
    name: "REI",
    level_1: "3-1",
    level_2: "2-3",
    level_3: "4-1",
    level_4: "1-1",
    id: 4,
  },
];
const shifts = [
  {
    type: "shift",
    name: "Shift 0",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 1,
  },
  {
    type: "special_shift",
    name: "Special Shift 1",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 2,
  },
  {
    type: "special_shift",
    name: "Special shift 2",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 3,
  },
  {
    type: "shift",
    name: "Shift 1",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 4,
  },
  {
    type: "shift",
    name: "Shift 83",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 5,
  },
  {
    type: "special_shift",
    name: "Special shift 3",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 6,
  },
  {
    type: "shift",
    name: "Shift 75",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 7,
  },
  {
    type: "shift",
    name: "Shift 54",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 8,
  },
  {
    type: "special_shift",
    name: "Special shift 4",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 9,
  },
  {
    type: "shift",
    name: "Shift 11",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#6045FF",
    person: { name: "Patty Partner", level: 2 },
    id: 10,
  },
  {
    type: "shift",
    name: "Shift 21",
    start: "8:00 PM",
    finish: "8:00 AM",
    duration: "12h",
    color: "#45C9E4",
    person: { name: "Fred Partner", level: 2 },
    id: 11,
  },
  {
    type: "special_shift",
    name: "Special shift 5",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12h",
    color: "#F0319D",
    person: { name: "James Partner", level: 3 },
    id: 12,
  },
];
const notifications = [
  {
    type: "error",
    person: "Patty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 1,
  },
  {
    type: "trade",
    person: "Bob Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 2,
  },
  {
    type: "error",
    person: "Kitty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 3,
  },
  {
    type: "trade",
    person: "Marry Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 4,
  },
  {
    type: "error",
    person: "John Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 5,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 6,
  },
  {
    type: "error",
    person: "Pat Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 7,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 8,
  },
  {
    type: "error",
    person: "Patty Partner",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 9,
  },
  {
    type: "trade",
    person: "No Name",
    avatar: "",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "24h",
    id: 10,
  },
];
const vacations = [
  {
    name: "Vacation",
    consecutive: 5,
    allowed: 4,
    id: 1,
  },
  {
    name: "Wellness",
    consecutive: 1,
    allowed: 4,
    id: 2,
  },
];
const holidays = [
  {
    name: "Christmas",
    date: "12/25/2021",
    type: "major",
    id: 1,
  },
  {
    name: "New Year",
    date: "01/01/2022",
    type: "major",
    id: 2,
  },
  {
    name: "Mother's day",
    date: "05/08/2022",
    type: "minor",
    id: 3,
  },
  {
    name: "Easter",
    date: "04/19/2022",
    type: "minor",
    id: 4,
  },
];

export default Schedule;
