import React, { useState } from "react";

// ** Third party libraries
import moment from "moment";

// ** Components
import Avatar from "@components/avatar";
import ShiftPlate from "../components/Shift/ShiftPlate";
import Textarea from "@components/Textarea";
import Button from "@components/Button";
import SVG from "@components/SVG";
import { Dropdown } from "react-bootstrap";

// ** Styles
import styles from "./trades.module.scss";
import common from "../../common.module.scss";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-15.jpg";
import defaultAvatar2 from "@src/assets/images/portrait/small/avatar-s-16.jpg";

const ShiftCard = ({ cardTitle, shifts, shiftSelected, handleSelect }) => {
  return (
    <div className={`${common.shadow} ${styles.shiftCard}`}>
      <div className={`${common.titleContainer} ${styles.title}`}>
        <div className={`${common.title} ${styles.cardTitle}`}>
          {cardTitle} Shift
        </div>
        <div className={styles.titleTip}>Select Partner</div>
      </div>

      <div className={styles.shiftsContainer}>
        {shifts.map((shift, index) => (
          <div
            key={index}
            onClick={() => handleSelect(cardTitle, index)}
            className={`${styles.shiftPlate} ${
              shiftSelected === index ? styles[`${cardTitle}Selected`] : ""
            }`}
          >
            <div className={styles.shiftDate}>
              <div>{moment(shift.date).format("dddd")}</div>
              <div>{moment(shift.date).format("D MMMM YYYY")}</div>
            </div>
            <div className={styles.userContainer}>
              <Avatar
                className={styles.avatar}
                img={defaultAvatar}
                imgHeight="50"
                imgWidth="50"
              />
              <div className={styles.shiftInfo}>
                <div>{shift.person}</div>
                <div>{`${shift.start} - ${shift.finish} | (${shift.duration}h)`}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

const WeeksCard = ({ week, daySelected, handleDaySelect }) => {
  const state = {
    first: {
      name: "Shift 1",
      date: "4/6/2021",
      person: {
        name: "John Pantera",
        level: 2,
        avatar: defaultAvatar,
        status: "online",
      },

      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12",
      color: "#6045FF",
    },
    second: {
      name: "Shift 2",
      date: "4/7/2021",
      person: {
        name: "Patty Partner",
        level: 2,
        avatar: defaultAvatar2,
        status: "online",
      },
      start: "8:00 AM",
      finish: "8:00 PM",
      duration: "12",
      color: "#45C9E4",
    },
  };

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={common.customToggle}
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <SVG.Calendar fill="#6B7DAA" />
      {children}
      <SVG.ArrowLeft fill="#6B7DAA" className={common.flipDown} />
    </div>
  ));

  const { first, second } = state;

  return (
    <div className={`${common.shadow} ${styles.weekCard}`}>
      <div
        className={`${common.titleContainer} ${styles.title} ${styles.weekTitle}`}
      >
        <div className={`${common.title}`}>Week {moment().format("W")}</div>
        <div>
          <Dropdown className={common.dropdown}>
            <Dropdown.Toggle
              variant="success"
              id="dropdown-basic"
              as={CustomToggle}
            >
              {moment().format("D MMM")} -{" "}
              {moment().add(14, "days").format("D MMM")}
            </Dropdown.Toggle>
            {/* <Dropdown.Menu></Dropdown.Menu> */}
          </Dropdown>
        </div>
      </div>

      <div className={styles.weekContainer}>
        <div className={styles.head}>
          {week.map((day, index) => (
            <div
              key={index}
              className={`${styles.day} ${
                daySelected === index ? styles.active : ""
              }`}
            >
              {day}
            </div>
          ))}
        </div>
        <div className={styles.dates}>
          {days.map((date, index) => (
            <div
              key={index}
              onClick={() => handleDaySelect(index)}
              className={`${styles.date} ${
                daySelected === index ? styles.active : ""
              }`}
            >
              {index + 1}
            </div>
          ))}
        </div>
      </div>

      <div
        className={`${common.titleContainer} ${styles.title} ${styles.checkerTitle}`}
      >
        <div className={`${common.title} ${styles.cardTitle}`}>
          Schedule Checker
        </div>
        <div className={`${styles.titleTip} ${styles.successTip}`}>
          Trade is okay
        </div>
      </div>

      <div className={styles.checkerContainer}>
        {first && (
          <div className={styles.checkerShift}>
            <div className={styles.shiftDate}>
              <div>{moment(first).format("D MMMM YYYY")}</div>
              <div>{moment(first).format("dddd")}</div>
            </div>
            <ShiftPlate shift={first} />
          </div>
        )}

        {second && (
          <div className={styles.checkerShift}>
            <div className={styles.shiftDate}>
              <div>{moment(second).format("D MMMM YYYY")}</div>
              <div>{moment(second).format("dddd")}</div>
            </div>
            <ShiftPlate shift={second} />
          </div>
        )}
      </div>
    </div>
  );
};

const FormCard = () => {
  return (
    <div className={`${common.shadow} ${styles.formCard}`}>
      <div className={`${common.titleContainer} ${styles.actionTitle}`}>
        Action
      </div>

      <div className={styles.controlsContainer}>
        <Textarea containerClass={styles.field} placeholder={"Write comment"} />
        <div className={styles.btnsContainer}>
          <Button
          // isDisabled={isDeleteDisabled}
          // onClick={deleteHandler}
          // className={styles.button}
          >
            Approve
          </Button>
          <Button variant={"secondary"}>Reject</Button>
        </div>
      </div>
    </div>
  );
};

function Trades() {
  const shiftsCards = ["first", "second"];
  const [shiftSelected, setShift] = useState({
    first: 0,
    second: 2,
  });
  const [daySelected, setDay] = useState(2);

  const handleShiftSelect = (container, id) => {
    setShift({
      ...shiftSelected,
      [container]: id,
    });
  };

  const handleDaySelect = (id) => {
    setDay(id);
  };

  return (
    <div className={`${common.pageContainer} ${styles.tradesPage}`}>
      <div>
        {shiftsCards.map((card, index) => (
          <ShiftCard
            key={index}
            cardTitle={card}
            shifts={shifts}
            shiftSelected={shiftSelected[card]}
            handleSelect={handleShiftSelect}
          />
        ))}
      </div>
      <div>
        <WeeksCard
          week={week}
          days={days}
          daySelected={daySelected}
          handleDaySelect={handleDaySelect}
        />
        <FormCard />
      </div>
    </div>
  );
}

const week = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
const days = new Array(14).fill("");

const shifts = [
  {
    date: "4/6/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/7/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/9/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/16/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/19/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/26/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
  {
    date: "4/29/2021",
    person: "Patty Partner",
    start: "8:00 AM",
    finish: "8:00 PM",
    duration: "12",
  },
];

export default Trades;
