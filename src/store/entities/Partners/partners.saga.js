import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import partnersReducer from "@store/entities/Partners/partners.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

// import * as service from "./partners.service";
// import { makeRequest } from "../../sagas/helpers";

const {
  // get partners
  fetchPartnersRequest,
  fetchPartnersSuccess,
  fetchPartnersFailure,

  // delete partner
  deletePartnerRequest,
  deletePartnerSuccess,
  deletePartnerFailure,

  // save partner
  savePartnerRequest,
  savePartnerSuccess,
  savePartnerFailure,

  // update partner
  updatePartnerRequest,
  updatePartnerSuccess,
  updatePartnerFailure,
} = partnersReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* fetchPartnersWorker() {
  try {
    const response = [
      {
        name: "Jonh Week",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 1,
      },
      {
        name: "Pipin Partner",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 2,
      },
      {
        name: "Patty Partner",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 3,
      },
      {
        name: "Obi One",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 4,
      },
      {
        name: "Obi Two",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 5,
      },
      {
        name: "Obi Three",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 6,
      },
      {
        name: "Obi Four",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 7,
      },
      {
        name: "Obi Five",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 8,
      },
      {
        name: "Obi Six",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 9,
      },
      {
        name: "Obi Seven",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 10,
      },
      {
        name: "Obi Kenobi",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 11,
      },
      {
        name: "Obi One",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 12,
      },
      {
        name: "Obi Two",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 13,
      },
      {
        name: "Obi Three",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 14,
      },
      {
        name: "Obi Four",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 15,
      },
      {
        name: "Obi Five",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 16,
      },
      {
        name: "Obi Six",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 17,
      },
      {
        name: "Obi Seven",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 18,
      },
      {
        name: "Obi Kenobi",
        level: 2,
        photo: "assets/images/user.png",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis",
        id: 19,
      },
    ];

    response.unshift({
      type: "static",
      name: "",
      level: 2,
      photo: "",
      id: 100,
    });
    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchPartnersFailure(response.message));
    } else {
      yield put(fetchPartnersSuccess(response));
    }
  } catch (error) {
    yield put(fetchPartnersFailure({ non_field_errors: "Error" }));
  }
}

function* deletePartnerWorker({ payload: partnersList }) {
  try {
    const response = partnersList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deletePartnerFailure(response.message));
    } else {
      yield put(deletePartnerSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deletePartnerFailure({ non_field_errors: "Error" }));
  }
}

function* savePartnerWorker({ payload: partnersList }) {
  console.log(partnersList);
  try {
    const response = partnersList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(savePartnerFailure(response.message));
    } else {
      yield put(savePartnerSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(savePartnerFailure({ non_field_errors: "Error" }));
  }
}

function* updatePartnerWorker({ payload: partnersList }) {
  // console.log(partnersList);
  try {
    const response = partnersList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updatePartnerFailure(response.message));
    } else {
      yield put(updatePartnerSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updatePartnerFailure({ non_field_errors: "Error" }));
  }
}

// // end-watchers //

function* watchFetchPartners() {
  yield takeEvery(fetchPartnersRequest, fetchPartnersWorker);
}

function* watchDeletePartner() {
  yield takeEvery(deletePartnerRequest, deletePartnerWorker);
}

function* watchSavePartner() {
  yield takeEvery(savePartnerRequest, savePartnerWorker);
}

function* watchUpdatePartner() {
  yield takeEvery(updatePartnerRequest, updatePartnerWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchPartners),
    fork(watchDeletePartner),
    fork(watchSavePartner),
    fork(watchUpdatePartner),
  ]);
}
