import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  partners: [],
  isPartnersLoading: false,
  isDeletePartnerLoading: false,
  isSavePartnerLoading: false,
  isUpdatePartnerLoading: false,
};

const users = createSlice({
  name: "partners",
  initialState,
  reducers: {
    // Partners
    fetchPartnersRequest(state) {
      state.isPartnersLoading = true;
    },
    fetchPartnersSuccess(state, { payload }) {
      state.partners = payload;
      state.isPartnersLoading = false;
    },
    fetchPartnersFailure(state) {
      state.isPartnersLoading = false;
    },

    // Delete partner
    deletePartnerRequest(state) {
      state.isDeletePartnerLoading = true;
    },
    deletePartnerSuccess(state, { payload }) {
      state.partners = payload;
      state.isDeletePartnerLoading = false;
    },
    deletePartnerFailure(state) {
      state.isDeletePartnerLoading = false;
    },

    // Save partner
    savePartnerRequest(state) {
      state.isSavePartnerLoading = true;
    },
    savePartnerSuccess(state, { payload }) {
      state.partners = payload;
      state.isSavePartnerLoading = false;
    },
    savePartnerFailure(state) {
      state.isSavePartnerLoading = false;
    },

    // Update partner
    updatePartnerRequest(state) {
      state.isUpdatePartnerLoading = true;
    },
    updatePartnerSuccess(state, { payload }) {
      state.partners = payload;
      state.isUpdatePartnerLoading = false;
    },
    updatePartnerFailure(state) {
      state.isUpdatePartnerLoading = false;
    },
  },
});

export default users;
