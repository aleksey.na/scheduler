import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import shiftsReducer from "@store/entities/Shifts/shifts.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

// import * as service from "./shifts.service";
// import { makeRequest } from "../../sagas/helpers";

const {
  // get shifts
  fetchShiftsRequest,
  fetchShiftsSuccess,
  fetchShiftsFailure,

  // delete shift
  deleteShiftRequest,
  deleteShiftSuccess,
  deleteShiftFailure,

  // save shift
  saveShiftRequest,
  saveShiftSuccess,
  saveShiftFailure,

  // update shift
  updateShiftRequest,
  updateShiftSuccess,
  updateShiftFailure,

  // Special shifts
  // get special shifts
  fetchSpecialShiftsRequest,
  fetchSpecialShiftsSuccess,
  fetchSpecialShiftsFailure,

  // delete special shift
  deleteSpecialShiftRequest,
  deleteSpecialShiftSuccess,
  deleteSpecialShiftFailure,

  // save special shift
  saveSpecialShiftRequest,
  saveSpecialShiftSuccess,
  saveSpecialShiftFailure,

  // update special shift
  updateSpecialShiftRequest,
  updateSpecialShiftSuccess,
  updateSpecialShiftFailure,
} = shiftsReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* fetchShiftsWorker() {
  try {
    const response = [
      {
        name: "Shift 1",
        duration: "12",
        start: "6",
        finish: "10",
        comments: "hello there",
        id: 1,
      },
      {
        name: "Shift 2",
        duration: "3",
        start: "12",
        finish: "5",
        comments: "some comments here",
        id: 2,
      },
      {
        name: "Shift 3",
        duration: "4",
        start: "12",
        finish: "4",
        comments: "No comments",
        id: 3,
      },
      {
        name: "Shift 4",
        duration: "5",
        start: "1",
        finish: "10",
        comments: "",
        id: 4,
      },
      {
        name: "Shift 5",
        duration: "6",
        start: "1",
        finish: "12",
        comments: "Light shift",
        id: 5,
      },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchShiftsFailure(response.message));
    } else {
      yield put(fetchShiftsSuccess(response));
    }
  } catch (error) {
    yield put(fetchShiftsFailure({ non_field_errors: "Error" }));
  }
}

function* deleteShiftWorker({ payload: shiftsList }) {
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteShiftFailure(response.message));
    } else {
      yield put(deleteShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteShiftFailure({ non_field_errors: "Error" }));
  }
}

function* saveShiftWorker({ payload: shiftsList }) {
  console.log(shiftsList);
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveShiftFailure(response.message));
    } else {
      yield put(saveShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveShiftFailure({ non_field_errors: "Error" }));
  }
}

function* updateShiftWorker({ payload: shiftsList }) {
  // console.log(shiftsList);
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateShiftFailure(response.message));
    } else {
      yield put(updateShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateShiftFailure({ non_field_errors: "Error" }));
  }
}

// Special shifts
function* fetchSpecialShiftsWorker() {
  try {
    const response = [
      {
        name: "Monday 9",
        duration: "12",
        day: "1",
        timeOff: "4",
        start: "6",
        finish: "10",
        comments: "hello there",
        id: 1,
      },
      {
        name: "Sunday 29",
        duration: "3",
        day: "5",
        timeOff: "10",
        start: "12",
        finish: "5",
        comments: "some comments here",
        id: 2,
      },
      {
        name: "Friday 13",
        duration: "4",
        day: "6",
        timeOff: "6",
        start: "12",
        finish: "4",
        comments: "No comments",
        id: 3,
      },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchSpecialShiftsFailure(response.message));
    } else {
      yield put(fetchSpecialShiftsSuccess(response));
    }
  } catch (error) {
    yield put(fetchSpecialShiftsFailure({ non_field_errors: "Error" }));
  }
}

function* deleteSpecialShiftWorker({ payload: shiftsList }) {
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteSpecialShiftFailure(response.message));
    } else {
      yield put(deleteSpecialShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteSpecialShiftFailure({ non_field_errors: "Error" }));
  }
}

function* saveSpecialShiftWorker({ payload: shiftsList }) {
  console.log(shiftsList);
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveSpecialShiftFailure(response.message));
    } else {
      yield put(saveSpecialShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveSpecialShiftFailure({ non_field_errors: "Error" }));
  }
}

function* updateSpecialShiftWorker({ payload: shiftsList }) {
  // console.log(shiftsList);
  try {
    const response = shiftsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateSpecialShiftFailure(response.message));
    } else {
      yield put(updateSpecialShiftSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateSpecialShiftFailure({ non_field_errors: "Error" }));
  }
}

// // end-watchers //

function* watchFetchShifts() {
  yield takeEvery(fetchShiftsRequest, fetchShiftsWorker);
}

function* watchSaveShift() {
  yield takeEvery(saveShiftRequest, saveShiftWorker);
}

function* watchUpdateShift() {
  yield takeEvery(updateShiftRequest, updateShiftWorker);
}

function* watchDeleteShift() {
  yield takeEvery(deleteShiftRequest, deleteShiftWorker);
}

// Special shifts

function* watchFetchSpecialShifts() {
  yield takeEvery(fetchSpecialShiftsRequest, fetchSpecialShiftsWorker);
}

function* watchSaveSpecialShift() {
  yield takeEvery(saveSpecialShiftRequest, saveSpecialShiftWorker);
}

function* watchUpdateSpecialShift() {
  yield takeEvery(updateSpecialShiftRequest, updateSpecialShiftWorker);
}

function* watchDeleteSpecialShift() {
  yield takeEvery(deleteSpecialShiftRequest, deleteSpecialShiftWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchShifts),
    fork(watchSaveShift),
    fork(watchUpdateShift),
    fork(watchDeleteShift),
    // Special shifts
    fork(watchFetchSpecialShifts),
    fork(watchSaveSpecialShift),
    fork(watchUpdateSpecialShift),
    fork(watchDeleteSpecialShift),
  ]);
}
