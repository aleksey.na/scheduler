import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  shifts: [],
  isShiftsLoading: false,
  isSaveShiftLoading: false,
  isUpdateShiftLoading: false,
  isDeleteShiftLoading: false,
  // Special shifts
  specialShifts: [],
  isSpecialShiftsLoading: false,
  isSaveSpecialShiftLoading: false,
  isUpdateSpecialShiftLoading: false,
  isDeleteSpecialShiftLoading: false,
};

const shifts = createSlice({
  name: "shifts",
  initialState,
  reducers: {
    // Shifts
    fetchShiftsRequest(state) {
      state.isShiftsLoading = true;
    },
    fetchShiftsSuccess(state, { payload }) {
      state.shifts = payload;
      state.isShiftsLoading = false;
    },
    fetchShiftsFailure(state) {
      state.isShiftsLoading = false;
    },

    // Delete shift
    deleteShiftRequest(state) {
      state.isDeleteShiftLoading = true;
    },
    deleteShiftSuccess(state, { payload }) {
      state.shifts = payload;
      state.isDeleteShiftLoading = false;
    },
    deleteShiftFailure(state) {
      state.isDeleteShiftLoading = false;
    },

    // Save shift
    saveShiftRequest(state) {
      state.isSaveShiftLoading = true;
    },
    saveShiftSuccess(state, { payload }) {
      state.shifts = payload;
      state.isSaveShiftLoading = false;
    },
    saveShiftFailure(state) {
      state.isSaveShiftLoading = false;
    },

    // Update shift
    updateShiftRequest(state) {
      state.isUpdateShiftLoading = true;
    },
    updateShiftSuccess(state, { payload }) {
      state.shifts = payload;
      state.isUpdateShiftLoading = false;
    },
    updateShiftFailure(state) {
      state.isUpdateShiftLoading = false;
    },

    // Special shifts
    fetchSpecialShiftsRequest(state) {
      state.isSpecialShiftsLoading = true;
    },
    fetchSpecialShiftsSuccess(state, { payload }) {
      state.specialShifts = payload;
      state.isSpecialShiftsLoading = false;
    },
    fetchSpecialShiftsFailure(state) {
      state.isSpecialShiftsLoading = false;
    },

    // Delete special shift
    deleteSpecialShiftRequest(state) {
      state.isDeleteSpecialShiftLoading = true;
    },
    deleteSpecialShiftSuccess(state, { payload }) {
      state.specialShifts = payload;
      state.isDeleteSpecialShiftLoading = false;
    },
    deleteSpecialShiftFailure(state) {
      state.isDeleteSpecialShiftLoading = false;
    },

    // Save special shift
    saveSpecialShiftRequest(state) {
      state.isSaveSpecialShiftLoading = true;
    },
    saveSpecialShiftSuccess(state, { payload }) {
      state.specialShifts = payload;
      state.isSaveSpecialShiftLoading = false;
    },
    saveSpecialShiftFailure(state) {
      state.isSaveSpecialShiftLoading = false;
    },

    // Update special shift
    updateSpecialShiftRequest(state) {
      state.isUpdateSpecialShiftLoading = true;
    },
    updateSpecialShiftSuccess(state, { payload }) {
      state.specialShifts = payload;
      state.isUpdateSpecialShiftLoading = false;
    },
    updateSpecialShiftFailure(state) {
      state.isUpdateSpecialShiftLoading = false;
    },
  },
});

export default shifts;
