import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  vacations: [],
  isVacationsLoading: false,
  isSaveVacationLoading: false,
  isDeleteVacationLoading: false,
  isUpdateVacationLoading: false,
};

const vacations = createSlice({
  name: "vacations",
  initialState,
  reducers: {
    // Vacations
    fetchVacationsRequest(state) {
      state.isVacationsLoading = true;
    },
    fetchVacationsSuccess(state, { payload }) {
      state.vacations = payload;
      state.isVacationsLoading = false;
    },
    fetchVacationsFailure(state) {
      state.isVacationsLoading = false;
    },

    // Delete vacation
    deleteVacationRequest(state) {
      state.isDeleteVacationLoading = true;
    },
    deleteVacationSuccess(state, { payload }) {
      state.vacations = payload;
      state.isDeleteVacationLoading = false;
    },
    deleteVacationFailure(state) {
      state.isDeleteVacationLoading = false;
    },

    // Save vacation
    saveVacationRequest(state) {
      state.isSaveVacationLoading = true;
    },
    saveVacationSuccess(state, { payload }) {
      state.vacations = payload;
      state.isSaveVacationLoading = false;
    },
    saveVacationFailure(state) {
      state.isSaveVacationLoading = false;
    },

    // Update vacation
    updateVacationRequest(state) {
      state.isUpdateVacationLoading = true;
    },
    updateVacationSuccess(state, { payload }) {
      state.vacations = payload;
      state.isUpdateVacationLoading = false;
    },
    updateVacationFailure(state) {
      state.isUpdateVacationLoading = false;
    },
  },
});

export default vacations;
