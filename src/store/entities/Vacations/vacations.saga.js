import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import vacationsReducer from "@store/entities/Vacations/vacations.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

// import * as service from "./vacations.service";
// import { makeRequest } from "../../sagas/helpers";

const {
  // get vacations
  fetchVacationsRequest,
  fetchVacationsSuccess,
  fetchVacationsFailure,

  // delete vacation
  deleteVacationRequest,
  deleteVacationSuccess,
  deleteVacationFailure,

  // save vacation
  saveVacationRequest,
  saveVacationSuccess,
  saveVacationFailure,

  // update vacation
  updateVacationRequest,
  updateVacationSuccess,
  updateVacationFailure,
} = vacationsReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* fetchVacationsWorker() {
  try {
    const response = [
      {
        name: "Vacation",
        duration: 5,
        type: "Week",
        consecutive: "Yes",
        allowed: 4,
        id: 1,
      },
      {
        name: "Wellness",
        duration: 1,
        type: "Day",
        consecutive: "No",
        allowed: 6,
        id: 2,
      },
      // {
      //   name: "Vacation",
      //   duration: 5,
      //   type: "Week",
      //   consecutive: "Yes",
      //   allowed: 4,
      //   id: 3,
      // },
      // {
      //   name: "Wellness",
      //   duration: 1,
      //   type: "Day",
      //   consecutive: "No",
      //   allowed: 6,
      //   id: 4,
      // },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchVacationsFailure(response.message));
    } else {
      yield put(fetchVacationsSuccess(response));
    }
  } catch (error) {
    yield put(fetchVacationsFailure({ non_field_errors: "Error" }));
  }
}

function* deleteVacationWorker({ payload: vacationsList }) {
  try {
    const response = vacationsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteVacationFailure(response.message));
    } else {
      yield put(deleteVacationSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteVacationFailure({ non_field_errors: "Error" }));
  }
}

function* saveVacationWorker({ payload: vacationsList }) {
  console.log(vacationsList);
  try {
    const response = vacationsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveVacationFailure(response.message));
    } else {
      yield put(saveVacationSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveVacationFailure({ non_field_errors: "Error" }));
  }
}

function* updateVacationWorker({ payload: vacationsList }) {
  // console.log(vacationsList);
  try {
    const response = vacationsList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateVacationFailure(response.message));
    } else {
      yield put(updateVacationSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateVacationFailure({ non_field_errors: "Error" }));
  }
}

// end-watchers //

function* watchFetchVacations() {
  yield takeEvery(fetchVacationsRequest, fetchVacationsWorker);
}

function* watchSaveVacation() {
  yield takeEvery(saveVacationRequest, saveVacationWorker);
}

function* watchUpdateVacation() {
  yield takeEvery(updateVacationRequest, updateVacationWorker);
}

function* watchDeleteVacation() {
  yield takeEvery(deleteVacationRequest, deleteVacationWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchVacations),
    fork(watchSaveVacation),
    fork(watchUpdateVacation),
    fork(watchDeleteVacation),
  ]);
}
