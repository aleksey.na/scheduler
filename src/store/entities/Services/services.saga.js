import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import servicesReducer from "@store/entities/Services/services.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

import * as service from "./services.service";
import { makeRequest } from "../../sagas/helpers";

const {
  // get services
  fetchServicesRequest,
  fetchServicesSuccess,
  fetchServicesFailure,

  // delete service
  deleteServiceRequest,
  deleteServiceSuccess,
  deleteServiceFailure,

  // save service
  saveServiceRequest,
  saveServiceSuccess,
  saveServiceFailure,

  // update service
  updateServiceRequest,
  updateServiceSuccess,
  updateServiceFailure,

  // get special services
  fetchSpecialServicesRequest,
  fetchSpecialServicesSuccess,
  fetchSpecialServicesFailure,

  // delete special service
  deleteSpecialServiceRequest,
  deleteSpecialServiceSuccess,
  deleteSpecialServiceFailure,

  // save special service
  saveSpecialServiceRequest,
  saveSpecialServiceSuccess,
  saveSpecialServiceFailure,

  // update special service
  updateSpecialServiceRequest,
  updateSpecialServiceSuccess,
  updateSpecialServiceFailure,
} = servicesReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* fetchServicesWorker() {
  try {
    const response = [
      {
        name: "Nights",
        number: 13,
        id: 1,
        levels: [
          {
            minimum: 2,
            maximum: 4,
            rotation: 5,
            exclusions: "",
          },
          {
            minimum: 1,
            maximum: 2,
            rotation: 4,
            exclusions: "",
          },
          {
            minimum: 1,
            maximum: 2,
            rotation: 1,
            exclusions: "",
          },
          {
            minimum: 4,
            maximum: 6,
            rotation: 3,
            exclusions: "",
          },
        ],
      },
      {
        name: "OB",
        number: 11,
        id: 2,
        levels: [
          {
            minimum: 3,
            maximum: 2,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 2,
            maximum: 1,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 2,
            maximum: 1,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 1,
            maximum: 3,
            rotation: 3,
            exclusions: true,
          },
        ],
      },
      {
        name: "GYN",
        number: 9,
        id: 3,
        levels: [
          {
            minimum: 4,
            maximum: 5,
            rotation: 3,
            exclusions: "",
          },
          {
            minimum: 4,
            maximum: 5,
            rotation: 3,
            exclusions: "",
          },
          {
            minimum: 4,
            maximum: 5,
            rotation: 3,
            exclusions: "",
          },
          {
            minimum: 4,
            maximum: 5,
            rotation: 3,
            exclusions: "",
          },
        ],
      },
      {
        name: "REI",
        number: 10,
        id: 4,
        levels: [
          {
            minimum: 6,
            maximum: 7,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 6,
            maximum: 7,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 6,
            maximum: 7,
            rotation: 3,
            exclusions: true,
          },
          {
            minimum: 6,
            maximum: 7,
            rotation: 3,
            exclusions: true,
          },
        ],
      },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchServicesFailure(response.message));
    } else {
      yield put(fetchServicesSuccess(response));
    }
  } catch (error) {
    yield put(fetchServicesFailure({ non_field_errors: "Error" }));
  }
}

function* deleteServiceWorker({ payload: servicesList }) {
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteServiceFailure(response.message));
    } else {
      yield put(deleteServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteServiceFailure({ non_field_errors: "Error" }));
  }
}

function* saveServiceWorker({ payload: servicesList }) {
  // console.log(servicesList);
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveServiceFailure(response.message));
    } else {
      yield put(saveServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveServiceFailure({ non_field_errors: "Error" }));
  }
}

function* updateServiceWorker({ payload: servicesList }) {
  // console.log(servicesList);
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateServiceFailure(response.message));
    } else {
      yield put(updateServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateServiceFailure({ non_field_errors: "Error" }));
  }
}

function* fetchSpecialServicesWorker() {
  try {
    const response = [
      {
        name: 4,
        number: 13,
        id: 1,
        attachedCore: 2,
        levelsData: [
          {
            valid: "Yes",
            validService: 2,
            exclude: "No",
            excludeServices: "",
          },
          {
            valid: "Yes",
            validService: 2,
            exclude: "Yes",
            excludeServices: 4,
          },
          {
            valid: "No",
            validService: 2,
            exclude: "Yes",
            excludeServices: 1,
          },
          {
            valid: "Yes",
            validService: 4,
            exclude: "No",
            excludeServices: "",
          },
        ],
      },
      {
        name: 2,
        number: 11,
        id: 2,
        attachedCore: 1,
        levelsData: [
          {
            valid: "Yes",
            validService: 2,
            exclude: "No",
            excludeServices: "",
          },
          {
            valid: "Yes",
            validService: 3,
            exclude: "Yes",
            excludeServices: 2,
          },
          {
            valid: "No",
            validService: 2,
            exclude: "",
            excludeServices: "",
          },
          {
            valid: "Yes",
            validService: 4,
            exclude: "No",
            excludeServices: "",
          },
        ],
      },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchSpecialServicesFailure(response.message));
    } else {
      yield put(fetchSpecialServicesSuccess(response));
    }
  } catch (error) {
    yield put(fetchSpecialServicesFailure({ non_field_errors: "Error" }));
  }
}

function* deleteSpecialServiceWorker({ payload: servicesList }) {
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteSpecialServiceFailure(response.message));
    } else {
      yield put(deleteSpecialServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteSpecialServiceFailure({ non_field_errors: "Error" }));
  }
}

function* saveSpecialServiceWorker({ payload: servicesList }) {
  // console.log(servicesList);
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveSpecialServiceFailure(response.message));
    } else {
      yield put(saveSpecialServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveSpecialServiceFailure({ non_field_errors: "Error" }));
  }
}

function* updateSpecialServiceWorker({ payload: servicesList }) {
  // console.log(servicesList);
  try {
    const response = servicesList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateSpecialServiceFailure(response.message));
    } else {
      yield put(updateSpecialServiceSuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateSpecialServiceFailure({ non_field_errors: "Error" }));
  }
}

// end-watchers //

function* watchFetchServices() {
  yield takeEvery(fetchServicesRequest, fetchServicesWorker);
}

function* watchSaveService() {
  yield takeEvery(saveServiceRequest, saveServiceWorker);
}

function* watchUpdateService() {
  yield takeEvery(updateServiceRequest, updateServiceWorker);
}

function* watchDeleteService() {
  yield takeEvery(deleteServiceRequest, deleteServiceWorker);
}

function* watchSpecialFetchServices() {
  yield takeEvery(fetchSpecialServicesRequest, fetchSpecialServicesWorker);
}

function* watchSpecialSaveService() {
  yield takeEvery(saveSpecialServiceRequest, saveSpecialServiceWorker);
}

function* watchSpecialUpdateService() {
  yield takeEvery(updateSpecialServiceRequest, updateSpecialServiceWorker);
}

function* watchSpecialDeleteService() {
  yield takeEvery(deleteSpecialServiceRequest, deleteSpecialServiceWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchServices),
    fork(watchSaveService),
    fork(watchUpdateService),
    fork(watchDeleteService),
    fork(watchSpecialFetchServices),
    fork(watchSpecialSaveService),
    fork(watchSpecialUpdateService),
    fork(watchSpecialDeleteService),
  ]);
}
