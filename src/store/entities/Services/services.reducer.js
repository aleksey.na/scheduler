import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  services: [],
  isServicesLoading: false,
  isDeleteServiceLoading: false,
  isSaveServiceLoading: false,
  isUpdateServiceLoading: false,
  specialServices: [],
  isSpecialServicesLoading: false,
  isDeleteSpecialServiceLoading: false,
  isSaveSpecialServiceLoading: false,
  isUpdateSpecialServiceLoading: false,
};

const services = createSlice({
  name: "Services",
  initialState,
  reducers: {
    // Services
    fetchServicesRequest(state) {
      state.isServicesLoading = true;
    },
    fetchServicesSuccess(state, { payload }) {
      state.services = payload;
      state.isServicesLoading = false;
    },
    fetchServicesFailure(state) {
      state.isServicesLoading = false;
    },

    // Delete service
    deleteServiceRequest(state) {
      state.isDeleteServiceLoading = true;
    },
    deleteServiceSuccess(state, { payload }) {
      state.services = payload;
      state.isDeleteServiceLoading = false;
    },
    deleteServiceFailure(state) {
      state.isDeleteServiceLoading = false;
    },

    // Save service
    saveServiceRequest(state) {
      state.isSaveServiceLoading = true;
    },
    saveServiceSuccess(state, { payload }) {
      state.services = payload;
      state.isSaveServiceLoading = false;
    },
    saveServiceFailure(state) {
      state.isSaveServiceLoading = false;
    },

    // Update service
    updateServiceRequest(state) {
      state.isUpdateServiceLoading = true;
    },
    updateServiceSuccess(state, { payload }) {
      state.services = payload;
      state.isUpdateServiceLoading = false;
    },
    updateServiceFailure(state) {
      state.isUpdateServiceLoading = false;
    },

    // Special services
    fetchSpecialServicesRequest(state) {
      state.isSpecialServicesLoading = true;
    },
    fetchSpecialServicesSuccess(state, { payload }) {
      state.specialServices = payload;
      state.isSpecialServicesLoading = false;
    },
    fetchSpecialServicesFailure(state) {
      state.isSpecialServicesLoading = false;
    },

    // Delete special service
    deleteSpecialServiceRequest(state) {
      state.isDeleteSpecialServiceLoading = true;
    },
    deleteSpecialServiceSuccess(state, { payload }) {
      state.specialServices = payload;
      state.isDeleteSpecialServiceLoading = false;
    },
    deleteSpecialServiceFailure(state) {
      state.isDeleteSpecialServiceLoading = false;
    },

    // Save special service
    saveSpecialServiceRequest(state) {
      state.isSaveSpecialServiceLoading = true;
    },
    saveSpecialServiceSuccess(state, { payload }) {
      state.specialServices = payload;
      state.isSaveSpecialServiceLoading = false;
    },
    saveSpecialServiceFailure(state) {
      state.isSaveSpecialServiceLoading = false;
    },

    // Update special service
    updateSpecialServiceRequest(state) {
      state.isUpdateSpecialServiceLoading = true;
    },
    updateSpecialServiceSuccess(state, { payload }) {
      state.specialServices = payload;
      state.isUpdateSpecialServiceLoading = false;
    },
    updateSpecialServiceFailure(state) {
      state.isUpdateSpecialServiceLoading = false;
    },
  },
});

export default services;
