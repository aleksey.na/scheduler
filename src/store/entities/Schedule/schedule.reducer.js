import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isFirstScheduleComplete: true,
};

const users = createSlice({
  name: "schedule",
  initialState,
  reducers: {
    // Set first schedule complete
    firstScheduleCompleteRequest(state) {
      // #TODO: add loading
    },
    firstScheduleCompleteSuccess(state) {
      state.isFirstScheduleComplete = false;
      // #TODO: add loading
    },

    // Signup
    // signupRequest(state) {
    //   state.isSingupLoading = true;
    // },
    // signupSuccess(state) {
    //   state.isSingupLoading = false;
    // },
    // signupFailure(state) {
    //   state.isSingupLoading = false;
    // },
  },
});

export default users;
