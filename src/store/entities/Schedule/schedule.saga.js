import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import scheduleReducer from "@store/entities/Schedule/schedule.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

// import * as service from "./schedule.service";
// import { makeRequest } from "../../sagas/helpers";

const { firstScheduleCompleteRequest, firstScheduleCompleteSuccess } =
  scheduleReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

// function* getUserWorker() {
//   try {
//     const response = yield call(makeRequest(service.fetchProfile));
//     if (response.message) {
//       if (response.message.logout) {
//         yield put(logoutSuccess(response.message.logout));
//       } else {
//         yield put(getUserFailure(response.message));
//       }
//     } else {
//       localStorage.setItem("uid", response.id);
//       yield put(getUserSuccess(response));
//     }
//   } catch (error) {
//     yield put(getUserFailure({ non_field_errors: "Error" }));
//   }
// }

function* getFirstScheduleWorker() {
  yield put(firstScheduleCompleteSuccess());
  // yield history.push(routes.loginScreen);
}

// // end-watchers //

function* watchGetFirstSettings() {
  yield takeEvery(firstScheduleCompleteRequest, getFirstScheduleWorker);
}

export default function* rootSaga() {
  yield all([fork(watchGetFirstSettings)]);
}
