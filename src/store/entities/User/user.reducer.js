import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userData: null,
  isUserDataLoading: false,
  isSingupLoading: false,
  isLoginLoading: false,
};

const users = createSlice({
  name: "user",
  initialState,
  reducers: {
    // Signup
    signupRequest(state) {
      state.isSingupLoading = true;
    },
    signupSuccess(state) {
      state.isSingupLoading = false;
    },
    signupFailure(state) {
      state.isSingupLoading = false;
    },

    // Login
    loginRequest(state, { payload }) {
      state.isLoginLoading = true;
    },
    loginSuccess(state, { payload }) {
      state.userData = payload.data;
      [payload.config.storageTokenKeyName] =
        payload[payload.config.storageTokenKeyName];
      [payload.config.storageRefreshTokenKeyName] =
        payload[payload.config.storageRefreshTokenKeyName];

      state.isLoginLoading = false;
    },
    loginFailure(state, { payload }) {
      state.isLoginLoading = false;
    },

    // Logout
    logoutRequest(state, { payload }) {},
    logoutSuccess(state, { payload }) {
      const obj = { ...payload };
      delete obj.type;
      state = { userData: {}, ...obj };
    },

    // UserData
    getUserRequest(state) {
      state.isUserDataLoading = true;
    },
    getUserSuccess(state, { payload }) {
      state.userData = payload;
      state.isUserDataLoading = false;
    },
    getUserFailure(state) {
      state.isUserDataLoading = false;
    },
  },
});

export default users;
