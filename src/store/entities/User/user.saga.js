import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import usersReducer from "@store/entities/User/user.reducer";
// import subscriptionReducer from "store/entities/Subscriptions/subscriptions.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

import * as service from "./user.service";
import { makeRequest } from "../../sagas/helpers";

// ** UseJWT import to get config
import useJwt from "@src/auth/jwt/useJwt";

const config = useJwt.jwtConfig;

const {
  // Signup
  signupRequest,
  signupSuccess,
  signupFailure,

  // Login
  loginRequest,
  loginSuccess,
  loginFailure,

  // Logout
  logoutRequest,
  logoutSuccess,

  // Get user data
  getUserRequest,
  getUserSuccess,
  getUserFailure,
} = usersReducer.actions;

// const { subscriptionRequest } = subscriptionReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* getUserWorker() {
  try {
    // const response = yield call(makeRequest(service.fetchProfile));
    const response = {
      first_name: "Sally",
      last_name: "Doe",
      photo: "./assets/images/welcome-screen.png",
      level: 4,
      id: new Date(),
    };
    if (response.message) {
      yield put(getUserFailure(response.message));
    } else {
      localStorage.setItem("uid", response.id);
      yield put(getUserSuccess(response));
    }
  } catch (error) {
    yield put(getUserFailure({ non_field_errors: "Error" }));
  }
}

function* signupWorker({ payload }) {
  const { email, firstName, lastName, phone, password } = payload;
  try {
    const signupResponse = yield call(makeRequest(service.signup), {
      email,
      firstName,
      lastName,
      phone,
      password,
    });

    if (signupResponse.message) {
      if (signupResponse.message.logout) {
        yield put(logoutSuccess(signupResponse.message.logout));
      } else {
        yield put(signupFailure(signupResponse.message));
        yield put(
          addNotificationAction({
            msg: JSON.stringify(signupResponse?.message),
            type: "danger",
          })
        );
      }
    } else {
      yield put(signupSuccess(signupResponse));
      yield put(
        addNotificationAction({
          msg: `Successfully Signed Up!
                Check your email and activate your profile via activation link`,
          type: "success",
        })
      );
    }
  } catch (error) {
    yield put(signupFailure({ non_field_errors: "Error" }));
  }
}

function* signupSuccessWorker() {
  // yield history.push(routes.loginScreen);
}

function* loginWorker({ payload }) {
  try {
    // const { email, password } = payload;
    // const loginResponse = yield call(makeRequest(service.login), {
    //   email,
    //   password,
    // });

    if (loginResponse.message) {
      yield put(
        addNotificationAction({
          msg: loginResponse?.message?.non_field_errors[0],
          type: "danger",
        })
      );
      yield put(loginFailure(loginResponse.message));
    } else {
      // ** Add to user, accessToken & refreshToken to localStorage
      localStorage.setItem("userData", JSON.stringify(data));
      localStorage.setItem(
        config.storageTokenKeyName,
        JSON.stringify(data.accessToken)
      );
      localStorage.setItem(
        config.storageRefreshTokenKeyName,
        JSON.stringify(data.refreshToken)
      );
      yield put(
        loginSuccess({
          data,
          config,
          [config.storageTokenKeyName]: data[config.storageTokenKeyName],
          [config.storageRefreshTokenKeyName]:
            data[config.storageRefreshTokenKeyName],
        })
      );

      // const { key: token } = loginResponse;
      // if (token) {
      //   localStorage.setItem("token", token);
      //   yield put(loginSuccess());
      //   yield put(getUserRequest());
      //   yield put(subscriptionRequest());
      // }
    }
  } catch (error) {
    yield put(loginFailure({ non_field_errors: "Error" }));
  }
}

function* loginSuccessWorker() {
  // yield history.push(routes.loginScreen);
}

function* logoutWorker({ payload }) {
  try {
    yield call(makeRequest(service.logout));
    yield put(logoutSuccess(payload));
  } catch (error) {
    yield put(logoutSuccess(payload));
  }
}

function* logoutSuccessWorker() {
  yield localStorage.clear();
  // yield history.push(routes.loginScreen);
}

// // end-watchers //

function* watchGetUser() {
  yield takeEvery(getUserRequest, getUserWorker);
}

function* watchSignup() {
  yield takeEvery(signupRequest, signupWorker);
}

function* watchSignupSuccess() {
  yield takeEvery(signupSuccess, signupSuccessWorker);
}

function* watchLogin() {
  yield takeEvery(loginRequest, loginWorker);
}

function* watchLoginSuccess() {
  yield takeEvery(loginSuccess, loginSuccessWorker);
}

function* watchLogout() {
  yield takeEvery(logoutRequest, logoutWorker);
}

function* watchLogoutSuccess() {
  yield takeEvery(logoutSuccess, logoutSuccessWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchGetUser),
    fork(watchSignup),
    fork(watchSignupSuccess),
    fork(watchLogin),
    fork(watchLoginSuccess),
    fork(watchLogout),
    fork(watchLogoutSuccess),
  ]);
}
