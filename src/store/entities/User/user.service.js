import { API, authorization, contentTypeJson } from "@src/@core/api/api";
import { get, post, patch, remove } from "@src/@core/api/base";

export const signup = ({ email, firstName, lastName, phone, password }) => {
  return post(
    `${API}/userprofiles/registration/`,
    JSON.stringify({
      user: {
        email,
        first_name: firstName,
        last_name: lastName,
        password,
      },
      phone_number: phone,
    }),
    {
      ...contentTypeJson(),
    }
  );
};

// Login
export const login = (fields) => {
  return post(`${API}/userprofiles/login/`, JSON.stringify(fields));
};

export const logout = () => {
  return post(`${API}/userprofiles/logout/`, JSON.stringify(""), {
    ...authorization(),
  });
};

export const fetchProfile = () => {
  return get(`${API}/userprofiles/userprofile`, { ...authorization() });
};
