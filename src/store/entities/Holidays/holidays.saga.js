import { all, call, fork, put, takeEvery } from "redux-saga/effects";
// import { history, routes } from "router";

import holidaysReducer from "@store/entities/Holidays/holidays.reducer";
import notificationsReducer from "@store/entities/Notification/notifications.reducer";

// import * as service from "./holidays.service";
// import { makeRequest } from "../../sagas/helpers";

const {
  // get holiday
  fetchHolidaysRequest,
  fetchHolidaysSuccess,
  fetchHolidaysFailure,

  // delete holiday
  deleteHolidayRequest,
  deleteHolidaySuccess,
  deleteHolidayFailure,

  // // save holiday
  saveHolidayRequest,
  saveHolidaySuccess,
  saveHolidayFailure,

  // update holiday
  updateHolidayRequest,
  updateHolidaySuccess,
  updateHolidayFailure,
} = holidaysReducer.actions;

const {
  actions: { addNotification: addNotificationAction },
} = notificationsReducer;

// watchers //

function* fetchHolidaysWorker() {
  try {
    const response = [
      {
        holidayName: "Christmas",
        holidayDate: new Date("12/25/2021").toDateString(),
        holidayType: "Major",
        holidayExclusion: "Yes",
        id: 1,
      },
      {
        holidayName: "New Year",
        holidayDate: new Date("01/01/2022").toDateString(),
        holidayType: "Major",
        holidayExclusion: "Yes",
        id: 2,
      },
      {
        holidayName: "Mother’s Day",
        holidayDate: new Date("11/06/2021").toDateString(),
        holidayType: "Minor",
        holidayExclusion: "No",
        id: 3,
      },
      {
        holidayName: "Easter",
        holidayDate: new Date("04/19/2022").toDateString(),
        holidayType: "Minor",
        holidayExclusion: "No",
        id: 4,
      },
    ];

    // const response = yield call(makeRequest(service.fetchProfile));
    if (response.message) {
      yield put(fetchHolidaysFailure(response.message));
    } else {
      yield put(fetchHolidaysSuccess(response));
    }
  } catch (error) {
    yield put(fetchHolidaysFailure({ non_field_errors: "Error" }));
  }
}

function* deleteHolidayWorker({ payload: holidaysList }) {
  try {
    const response = holidaysList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(deleteHolidayFailure(response.message));
    } else {
      yield put(deleteHolidaySuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(deleteHolidayFailure({ non_field_errors: "Error" }));
  }
}

function* saveHolidayWorker({ payload: holidaysList }) {
  console.log(holidaysList);
  try {
    const response = holidaysList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(saveHolidayFailure(response.message));
    } else {
      yield put(saveHolidaySuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(saveHolidayFailure({ non_field_errors: "Error" }));
  }
}

function* updateHolidayWorker({ payload: holidaysList }) {
  // console.log(holidaysList);
  try {
    const response = holidaysList;
    // const response = yield call(makeRequest(service.deletePartner));
    if (response.message) {
      yield put(updateHolidayFailure(response.message));
    } else {
      yield put(updateHolidaySuccess(response));
      // yield put(fetchPartnersRequest(response));
    }
  } catch (error) {
    yield put(updateHolidayFailure({ non_field_errors: "Error" }));
  }
}

// // end-watchers //

function* watchFetchHolidays() {
  yield takeEvery(fetchHolidaysRequest, fetchHolidaysWorker);
}

function* watchDeleteHoliday() {
  yield takeEvery(deleteHolidayRequest, deleteHolidayWorker);
}

function* watchSaveHoliday() {
  yield takeEvery(saveHolidayRequest, saveHolidayWorker);
}

function* watchUpdateHoliday() {
  yield takeEvery(updateHolidayRequest, updateHolidayWorker);
}

export default function* rootSaga() {
  yield all([
    fork(watchFetchHolidays),
    fork(watchDeleteHoliday),
    fork(watchSaveHoliday),
    fork(watchUpdateHoliday),
  ]);
}
