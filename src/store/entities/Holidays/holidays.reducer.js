import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  holidays: [],
  isHolidaysLoading: false,
  isSaveHolidayLoading: false,
  isUpdateHolidayLoading: false,
  isDeleteHolidayLoading: false,
};

const users = createSlice({
  name: "holidays",
  initialState,
  reducers: {
    // Holidays
    fetchHolidaysRequest(state) {
      state.isHolidaysLoading = true;
    },
    fetchHolidaysSuccess(state, { payload }) {
      state.holidays = payload;
      state.isHolidaysLoading = false;
    },
    fetchHolidaysFailure(state) {
      state.isHolidaysLoading = false;
    },

    // Delete holiday
    deleteHolidayRequest(state) {
      state.isDeleteHolidayLoading = true;
    },
    deleteHolidaySuccess(state, { payload }) {
      state.holidays = payload;
      state.isDeleteHolidayLoading = false;
    },
    deleteHolidayFailure(state) {
      state.isDeleteHolidayLoading = false;
    },

    // Save holiday
    saveHolidayRequest(state) {
      state.isSaveHolidayLoading = true;
    },
    saveHolidaySuccess(state, { payload }) {
      state.holidays = payload;
      state.isSaveHolidayLoading = false;
    },
    saveHolidayFailure(state) {
      state.isSaveHolidayLoading = false;
    },

    // Update partner
    updateHolidayRequest(state) {
      state.isUpdateHolidayLoading = true;
    },
    updateHolidaySuccess(state, { payload }) {
      state.holidays = payload;
      state.isUpdateHolidayLoading = false;
    },
    updateHolidayFailure(state) {
      state.isUpdateHolidayLoading = false;
    },
  },
});

export default users;
