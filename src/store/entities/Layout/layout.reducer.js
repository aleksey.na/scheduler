import { createSlice } from "@reduxjs/toolkit";

// ** ThemeConfig Import
import themeConfig from "@configs/themeConfig";

// ** Returns Initial Menu Collapsed State
const initialMenuCollapsed = () => {
  const item = window.localStorage.getItem("menuCollapsed");
  //** Parse stored json or if none return initialValue
  return item ? JSON.parse(item) : themeConfig.layout.menu.isCollapsed;
};

// ** Initial State
const initialState = {
  isRTL: themeConfig.layout.isRTL,
  menuCollapsed: initialMenuCollapsed(),
  menuHidden: themeConfig.layout.menu.isHidden,
  contentWidth: themeConfig.layout.contentWidth,
  firstSettingsLinks: [],
  firstSettingsMenuStep: {
    index: null,
    title: "",
  },
};

const layout = createSlice({
  name: "layout",
  initialState,
  reducers: {
    handleContentWidth(state, { payload }) {
      state.contentWidth = payload;
    },

    handleMenuCollapsed(state, { payload }) {
      window.localStorage.setItem("menuCollapsed", payload);
      state.menuCollapsed = payload;
    },

    handleMenuHidden(state, { payload }) {
      state.menuHidden = payload;
    },

    handleRtl(state, { payload }) {
      state.isRTL = payload;
    },

    handleFirstSettingsMenuStep(state, { payload }) {
      state.firstSettingsMenuStep = payload;
    },

    handleFirstSettingsLinks(state, { payload }) {
      state.firstSettingsLinks = payload;
    },
  },
});

export default layout;
