import { createSelector } from "reselect";

const toastsSelector = (state) => state.notification?.toasts;

export const selectorToasts = createSelector(toastsSelector, (items) => items);
