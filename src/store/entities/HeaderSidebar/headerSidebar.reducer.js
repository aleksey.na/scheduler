import { createSlice } from "@reduxjs/toolkit";
import * as routes from "router/routes";

const initialState = {
  greeting: "Good morning,",
  linkBack: null,
  currentLink: {
    step: 1,
    path: routes.welcome,
  },
  firstSettingsLinks: [
    {
      path: routes.welcome,
      title: "Welcome",
      exact: true,
      current: true,
    },
    {
      path: routes.partners,
      title: "Partners",
      exact: true,
      current: false,
    },
    {
      path: routes.holidays,
      title: "Holidays",
      exact: true,
      current: false,
    },
    {
      path: routes.vacations,
      title: "Personal Items",
      exact: true,
      current: false,
    },
    {
      path: routes.coreServices,
      title: "Core Services",
      exact: true,
      current: false,
    },
    {
      path: routes.specialServices,
      title: "Special Services",
      exact: true,
      current: false,
    },
    {
      path: routes.shifts,
      title: "Shifts",
      exact: true,
      current: false,
    },
    {
      path: routes.specialShifts,
      title: "Special Shifts",
      exact: true,
      current: false,
    },
  ],
};

const header = createSlice({
  name: "headerSidebar",
  initialState,
  reducers: {
    setHeaderTitle(state, { payload }) {
      state.greeting = payload.title;
    },

    // Sidebar step
    setSidebarLink(state, { payload }) {
      state.currentLink = payload;
    },
  },
});

export default header;
