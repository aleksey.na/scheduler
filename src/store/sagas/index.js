import { all, call, spawn } from "redux-saga/effects";

import userSaga from "../entities/User/user.saga";
import scheduleSaga from "../entities/Schedule/schedule.saga";
import partnersSaga from "../entities/Partners/partners.saga";
import holidaysSaga from "../entities/Holidays/holidays.saga";
import vacationsSaga from "../entities/Vacations/vacations.saga";
import servicesSaga from "../entities/Services/services.saga";
import shiftsSaga from "../entities/Shifts/shifts.saga";

export default function* rootSaga() {
  const sagas = [
    userSaga,
    scheduleSaga,
    partnersSaga,
    holidaysSaga,
    vacationsSaga,
    servicesSaga,
    shiftsSaga,
  ];

  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (error) {
            console.error(error, "Saga Failed!");
          }
        }
      })
    )
  );
}
