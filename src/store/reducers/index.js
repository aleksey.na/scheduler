import { combineReducers } from "redux";

import Layout from "../entities/Layout/layout.reducer";
import Notifications from "../entities/Notification/notifications.reducer";
import Modal from "../entities/Modal/modal.reducer";
import User from "../entities/User/user.reducer";
import Schedule from "../entities/Schedule/schedule.reducer";
import Partners from "../entities/Partners/partners.reducer";
import Holidays from "../entities/Holidays/holidays.reducer";
import Vacations from "../entities/Vacations/vacations.reducer";
import Services from "../entities/Services/services.reducer";
import Shifts from "../entities/Shifts/shifts.reducer";

const reducers = combineReducers({
  layout: Layout.reducer,
  notification: Notifications.reducer,
  modal: Modal.reducer,
  user: User.reducer,
  schedule: Schedule.reducer,
  partners: Partners.reducer,
  holidays: Holidays.reducer,
  vacations: Vacations.reducer,
  services: Services.reducer,
  shifts: Shifts.reducer,
});

export default reducers;
