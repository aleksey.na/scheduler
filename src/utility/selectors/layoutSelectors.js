import { createSelector } from "reselect";

// Menu step
const stepSelector = (state) => state.layout.firstSettingsMenuStep;
export const firstSettingsMenuStepSelector = createSelector(
  stepSelector,
  (step) => step
);

// Menu links
const linksSelector = (state) => state.layout.firstSettingsLinks;
export const firstSettingsMenuLinksSelector = createSelector(
  linksSelector,
  (arr) => arr
);
