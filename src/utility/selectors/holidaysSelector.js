import { createSelector } from "reselect";

// Get holidays
const holidaysSelector = (state) => state.holidays.holidays;
export const selectorHolidays = createSelector(
  holidaysSelector,
  (list) => list
);

// Get major holidays
export const selectorMajorHolidays = createSelector(holidaysSelector, (list) =>
  list.filter((h) => h.holidayType === "Major")
);

// Get minor holidays
export const selectorMinorHolidays = createSelector(holidaysSelector, (list) =>
  list.filter((h) => h.holidayType === "Minor")
);
