import { createSelector } from "reselect";

// Get vacations
const vacationsSelector = (state) => state.vacations.vacations;
export const selectorVacations = createSelector(
  vacationsSelector,
  (list) => list
);
