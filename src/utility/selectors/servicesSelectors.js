import { createSelector } from "reselect";

// Get services
const servicesSelector = (state) => state.services.services;
export const selectorServices = createSelector(
  servicesSelector,
  (list) => list
);

// Get Special services
const specialServicesSelector = (state) => state.services.specialServices;
export const selectorSpecialServices = createSelector(
  specialServicesSelector,
  (list) => list
);
