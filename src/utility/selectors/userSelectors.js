import { createSelector } from "reselect";

// User data
const userDataSelector = (state) => state.user.userData;
export const selectorUserData = createSelector(
  userDataSelector,
  (data) => data
);
