import { createSelector } from "reselect";

// Get shifts
const shiftsSelector = (state) => state.shifts.shifts;
export const selectorShifts = createSelector(shiftsSelector, (list) => list);

// Get special shifts
const specialShiftsSelector = (state) => state.shifts.specialShifts;
export const selectorSpecialShifts = createSelector(
  specialShiftsSelector,
  (list) => list
);
