import { createSelector } from "reselect";

// Get partners
const partnersSelector = (state) => state.partners.partners;
export const selectorPartners = createSelector(
  partnersSelector,
  (list) => list
);
