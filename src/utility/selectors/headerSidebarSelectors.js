import { createSelector } from "reselect";

// Header greeting
const headerGreetingSelector = (state) => state.headerSidebar.greeting;
export const selectorHeaderGreeting = createSelector(
  headerGreetingSelector,
  (greeting) => greeting
);

// Sidebar first settings links
const sidebarLinksSelector = (state) => state.headerSidebar.firstSettingsLinks;
export const selectorFirstSettingsLinks = createSelector(
  sidebarLinksSelector,
  (arr) => arr
);

// Sidebar current link
const sidebarLinkSelector = (state) => state.headerSidebar.currentLink;
export const selectorSidebarLink = createSelector(
  sidebarLinkSelector,
  (link) => link
);
