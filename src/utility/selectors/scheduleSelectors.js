import { createSelector } from "reselect";

// Is first settings done
const firstSettingsSelector = (state) => state.schedule.isFirstScheduleComplete;
export const selectorFirstSettings = createSelector(
  firstSettingsSelector,
  (bool) => bool
);
