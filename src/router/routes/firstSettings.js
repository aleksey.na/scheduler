import { lazy } from "react";
import { Redirect } from "react-router-dom";

const firstSettings = [
  {
    path: "/first-settings/welcome",
    exact: true,
    appLayout: true,
    // className: "email-application",
    className: "",
    component: lazy(() => import("../../views/firstSettings/Welcome/Welcome")),
    meta: {
      navLink: "/first-settings/welcome",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/partners",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/Partners/Partners")
    ),
    meta: {
      navLink: "/first-settings/partners",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/holidays",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/Holidays/Holidays")
    ),
    meta: {
      navLink: "/first-settings/holidays",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/vacations",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/Vacations/Vacations")
    ),
    meta: {
      navLink: "/first-settings/vacations",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/core-services",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/CoreServices/CoreServices")
    ),
    meta: {
      navLink: "/first-settings/core-services",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/special-services",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/SpecialServices/SpecialServices")
    ),
    meta: {
      navLink: "/first-settings/special-services",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/shifts",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/firstSettings/Shifts/Shifts")),
    meta: {
      navLink: "/first-settings/shifts",
      authRoute: true,
      role: "first-settings",
    },
  },
  {
    path: "/first-settings/special-shifts",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/firstSettings/SpecialShifts/SpecialShifts")
    ),
    meta: {
      navLink: "/first-settings/special-shifts",
      authRoute: true,
      role: "first-settings",
    },
  },
];

export default firstSettings;
