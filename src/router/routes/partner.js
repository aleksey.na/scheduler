import { lazy } from "react";
import { Redirect } from "react-router-dom";

const partner = [
  {
    path: "/partner/home",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/Home")),
  },
  {
    path: "/partner/home/trades",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/PreTrade/PreTrade")),
  },
  {
    path: "/partner/statistics",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/Statistics/Statistics")),
  },
  {
    path: "/partner/statistics/selected-shifts",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/Statistics/SelectedShifts")),
  },
  {
    path: "/partner/wellness",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/Wellness/Wellness")),
  },
  {
    path: "/partner/settings",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/partner/Settings/Settings")),
  },
];

export default partner;
