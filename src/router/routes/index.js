import { lazy } from "react";

import { getHomeRouteForLoggedInUser, getUserRole } from "@utils";
import firstSettingsRoutes from "./firstSettings";
import schedulerRoutes from "./scheduler";
import partnerRoutes from "./partner";
import buddyRoutes from "./buddy";

// ** Document title
const TemplateTitle = "%s - Scheduler App";

// ** Default Route
const DefaultRoute = getHomeRouteForLoggedInUser();

const getRoleRoutes = (role = getUserRole()) => {
  switch (role) {
    case "first-settings":
      return firstSettingsRoutes;

    case "scheduler":
      return schedulerRoutes;

    case "partner":
      return partnerRoutes;

    case "buddy":
      return buddyRoutes;

    default:
      console.log("Default case in side-bar render");
      return [];
  }
};

// ** Merge Routes
const Routes = [
  ...getRoleRoutes(),
  {
    path: "/login",
    component: lazy(() => import("../../views/Login")),
    layout: "BlankLayout",
    meta: {
      authRoute: true,
    },
  },
  {
    path: "/error",
    component: lazy(() => import("../../views/Error")),
    layout: "BlankLayout",
  },
];

export { DefaultRoute, TemplateTitle, Routes };
