import { lazy } from "react";
import { Redirect } from "react-router-dom";

const scheduler = [
  // home sub-nemu
  {
    path: "/scheduler/home",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/Home/Home")),
  },
  {
    path: "/scheduler/trades",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/Trades/Trades")),
  },
  // schedule sub-nemu
  {
    path: "/scheduler/schedule/:modalPath?/:uid?",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/Schedule/Schedule")),
    meta: {},
  },
  {
    path: "/scheduler/schedule/partners",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/Partners")),
  },
  {
    path: "/scheduler/schedule/core-services",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/CoreServices")),
  },
  {
    path: "/scheduler/schedule/special-services",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/SpecialServices")),
  },
  // rest menu items
  {
    path: "/scheduler/settings",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/scheduler/Settings")),
  },
  {
    path: "/scheduler/statistics",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() =>
      import("../../views/scheduler/Statistics/Statistics")
    ),
  },
];

export default scheduler;
