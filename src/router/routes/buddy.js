import { lazy } from "react";
import { Redirect } from "react-router-dom";

const buddy = [
  {
    path: "/buddy/home",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/buddy/Home")),
  },
  {
    path: "/buddy/home/trades",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/buddy/Trades")),
    meta: {
      navLink: "/buddy/home/trades",
    },
  },
  {
    path: "/buddy/wellness",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/buddy/Wellness/Wellness")),
    meta: {
      navLink: "/buddy/wellness",
    },
  },
  {
    path: "/buddy/statistics",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/buddy/Statistics/Statistics")),
    meta: {
      navLink: "/buddy/statistics",
    },
  },
  {
    path: "/buddy/statistics/selected-shifts",
    exact: true,
    appLayout: true,
    className: "",
    component: lazy(() => import("../../views/buddy/Statistics/SelectedShifts")),
    meta: {
      navLink: "/buddy/statistics/selected-shifts",
    },
  },
];

export default buddy;

