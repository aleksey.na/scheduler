// ** React Imports
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

// ** Third Party Components
// import { Disc, X, Circle } from "react-feather";

// ** Config
import themeConfig from "@configs/themeConfig";

const VerticalMenuHeader = (props) => {

  return (
    <div className="navbar-header">
      <ul className="nav navbar-nav flex-row">
        <li className="nav-item mr-auto">
          <NavLink to="/" className="navbar-brand">
            <span className="brand-logo">
              <img src={themeConfig.app.appLogoImage} alt="logo" />
            </span>
            <h2 className="brand-text mb-0">{themeConfig.app.appName}</h2>
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default VerticalMenuHeader;
