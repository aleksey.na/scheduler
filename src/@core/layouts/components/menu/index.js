// ** React Imports
import { Fragment, useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";

import layoutReducer from "@store/entities/Layout/layout.reducer";
import { firstSettingsMenuStepSelector } from "@selectors/layoutSelectors";

// ** Vertical Menu Items func returns Array
import navigation from "@src/navigation";

// ** Third Party Components
import classnames from "classnames";
import PerfectScrollbar from "react-perfect-scrollbar";

// ** Vertical Menu Components
import VerticalMenuHeader from "./VerticalMenuHeader";
import VerticalNavMenuItems from "./VerticalNavMenuItems";

import SVG from "@components/SVG";

import { getUserRole } from "@utils";

const { handleFirstSettingsMenuStep } = layoutReducer.actions;

const Sidebar = (props) => {
  const dispatch = useDispatch();

  // ** Props
  const {
    menuCollapsed,
    setMenuCollapsed,
    routerProps,
    menu,
    currentActiveItem,
    currentActiveStep,
    skin,
  } = props;

  // ** States
  const currentStep = useSelector(firstSettingsMenuStepSelector);

  const [groupOpen, setGroupOpen] = useState([]);
  const [groupActive, setGroupActive] = useState([]);
  const [activeItem, setActiveItem] = useState(null);
  const [isFirstSettings, setIsFirstSettings] = useState(false);

  useEffect(() => {
    if (getUserRole() === "first-settings") {
      setIsFirstSettings(true);
    }
  }, []);


  const setCurrentStep = (step) => {
    dispatch(handleFirstSettingsMenuStep(step));
  };

  // ** Menu Hover State
  const [menuHover, setMenuHover] = useState(false);

  // ** Ref
  const shadowRef = useRef(null);

  // ** Function to handle Mouse Enter
  const onMouseEnter = () => {
    if (menuCollapsed) {
      setMenuHover(true);
    }
  };

  // #TODO: No need to use on Light theme, or ask a designer (P.S. and add bgr color)
  // ** Scroll Menu
  const scrollMenu = (container) => {
    if (shadowRef && container.scrollTop > 0) {
      if (!shadowRef.current.classList.contains("d-block")) {
        shadowRef.current.classList.add("d-block");
      }
    } else {
      if (shadowRef.current.classList.contains("d-block")) {
        shadowRef.current.classList.remove("d-block");
      }
    }
  };

  // ** Reset Active & Open Group Arrays
  const resetActiveAndOpenGroups = () => {
    setGroupActive([]);
    setGroupOpen([]);
  };

  return (
    <Fragment>
      <div
        className={classnames(
          "main-menu menu-fixed menu-accordion menu-shadow",
          {
            expanded: menuHover || menuCollapsed === false,
            "menu-light": skin !== "semi-dark" && skin !== "dark",
            "menu-dark": skin === "semi-dark" || skin === "dark",
          }
        )}
        onMouseEnter={onMouseEnter}
        onMouseLeave={() => setMenuHover(false)}
      >
        {menu ? (
          menu(props)
        ) : (
          <Fragment>
            {/* Vertical Menu Header */}
            <VerticalMenuHeader
              setGroupOpen={setGroupOpen}
              menuHover={menuHover}
              {...props}
            />
            {/* Vertical Menu Header Shadow */}
            <div className="shadow-bottom" ref={shadowRef}></div>
            {/* Perfect Scrollbar */}
            <PerfectScrollbar
              className="main-menu-content"
              options={{ wheelPropagation: false }}
              onScrollY={(container) => scrollMenu(container)}
            >
              {!isFirstSettings && <div className="position-absolute nav-toggle" onClick={() => setMenuCollapsed(!menuCollapsed)}>
                <SVG.DoubleArrowLeft fill="#fff" className={`${menuCollapsed ? "icon-flipped" : ""}`} />
              </div>}
              <ul className={`navigation navigation-main ${menuCollapsed && "menu-collapsed"}`}>
                <VerticalNavMenuItems
                  items={navigation}
                  groupActive={groupActive}
                  setGroupActive={setGroupActive}
                  activeItem={activeItem}
                  setActiveItem={setActiveItem}
                  groupOpen={groupOpen}
                  setGroupOpen={setGroupOpen}
                  routerProps={routerProps}
                  menuCollapsed={menuCollapsed}
                  menuHover={menuHover}
                  currentActiveItem={currentActiveItem}
                  currentActiveStep={currentActiveStep}
                  setCurrentStep={setCurrentStep}
                  currentStep={currentStep}
                  resetActiveAndOpenGroups={resetActiveAndOpenGroups}
                />
              </ul>
              <div className={`logout-btn ${menuCollapsed ? "pl-1" : ""}`}>
                <SVG.Logout className={menuCollapsed ? "mr-0" : "mr-1"} />
                {!menuCollapsed && <div>Log Out</div>}
              </div>
            </PerfectScrollbar>
          </Fragment>
        )}
      </div>
    </Fragment>
  );
};

export default Sidebar;
