// ** React Imports
import { useEffect } from "react";
import { NavLink, useLocation, matchPath, useParams } from "react-router-dom";

// ** Third Party Components
import { Badge } from "reactstrap";
import classnames from "classnames";

// ** Vertical Menu Array Of Items
import navigation from "@src/navigation";

// ** Utils
import { isNavLinkActive, search, getAllParents } from "@layouts/utils";

const VerticalNavMenuLink = ({
  item,
  itemIndex,
  // groupActive,
  setGroupActive,
  activeItem,
  setActiveItem,
  // groupOpen,
  // setGroupOpen,
  toggleActiveGroup,
  resetActiveAndOpenGroups,
  parentItem,
  routerProps,
  currentActiveItem,
  currentActiveStep,
  setCurrentStep,
  currentStep,
}) => {
  // ** Conditional Link Tag, if item has newTab or externalLink props use <a> tag else use NavLink
  const LinkTag = item.externalLink ? "a" : NavLink;

  // ** URL Vars
  const location = useLocation();
  const currentURL = location.pathname;

  // ** To match path
  const match = matchPath(currentURL, {
    path: `${item.navLink}/:param`,
    exact: true,
    strict: false,
  });

  // ** Search for current item parents
  const searchParents = (navigation, currentURL) => {
    const parents = search(navigation, currentURL, routerProps); // Search for parent object
    const allParents = getAllParents(parents, "id"); // Parents Object to Parents Array
    return allParents;
  };

  // ** URL Vars
  const resetActiveGroup = (navLink) => {
    const parents = search(navigation, navLink, match);
    toggleActiveGroup(item.id, parents);
  };

  // ** Checks url & updates active item
  useEffect(() => {
    if (currentActiveItem !== null) {
      setActiveItem(currentActiveItem);
      const arr = searchParents(navigation, currentURL);
      setGroupActive([...arr]);
    }

    if (currentActiveStep !== null) {
      setCurrentStep({
        index: itemIndex,
        title: item.title,
      });
    }
  }, [location]);

  const isItemActive = item.navLink === activeItem;

  return (
    <li
      className={classnames({
        "nav-item": !item.children,
        disabled: item.disabled,
        active: isItemActive,
        "current-step": item.meta?.step && isItemActive,
        "passed-step": item.meta?.step && itemIndex < currentStep.index,
      })}
    >
      <LinkTag
        className={classnames("d-flex align-items-center", {
          "first-step-link": item.meta?.step,
        })}
        target={item.newTab ? "_blank" : undefined}
        /*eslint-disable */
        {...(item.externalLink === true
          ? {
              href: item.navLink || "/",
            }
          : {
              to: item.navLink || "/",
              isActive: (match, location) => {
                if (!match) {
                  return false;
                }

                if (
                  match.url &&
                  match.url !== "" &&
                  match.url === item.navLink
                ) {
                  currentActiveItem = item.navLink;
                  if (item.meta?.step) {
                    currentActiveStep = itemIndex;
                  }
                }
              },
            })}
        /*eslint-enable */
        onClick={(e) => {
          if (!item.navLink.length) {
            e.preventDefault();
          }

          parentItem
            ? resetActiveGroup(item.navLink)
            : resetActiveAndOpenGroups();
        }}
      >
        {item.icon}
        <span className="menu-item text-truncate">{item.title}</span>

        {item.badge && item.badgeText ? (
          <Badge className="ml-auto mr-1" color={item.badge} pill>
            {item.badgeText}
          </Badge>
        ) : null}
      </LinkTag>
      {item.meta?.step && <hr className="progress-line" />}
    </li>
  );
};

export default VerticalNavMenuLink;
