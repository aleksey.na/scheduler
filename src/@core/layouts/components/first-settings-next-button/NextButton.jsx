import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

// ** Utils
import { nextLinkHandler } from "@utils";

// ** Selectors
import {
  firstSettingsMenuStepSelector,
  firstSettingsMenuLinksSelector,
} from "@selectors/layoutSelectors";

// ** Components
import Button from "@components/Button/Button";

import styles from "./nextButton.module.scss";

function NextButton() {
  const history = useHistory();
  const currentLink = useSelector(firstSettingsMenuStepSelector);
  const menuLinks = useSelector(firstSettingsMenuLinksSelector);

  const nextLinkIndex = currentLink.index + 1;
  const disabled = false;

  const nextMenuStepHandler = () => {
    nextLinkHandler(history, currentLink, menuLinks);
  };

  return (
    <>
      {currentLink?.title !== "Welcome" && (
        <div className={styles.container}>
          <div className={styles.title}>Add {currentLink?.title}</div>
          <Button
            isDisabled={disabled}
            className={styles.titleBtn}
            onClick={nextMenuStepHandler}
          >
            {nextLinkIndex < menuLinks.length ? "Next" : "Done"}
          </Button>
        </div>
      )}
    </>
  );
}

export default NextButton;
