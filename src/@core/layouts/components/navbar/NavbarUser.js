// ** Dropdowns Imports
import { Fragment, useEffect, useState } from "react";

import UserDropdown from "./UserDropdown";

// ** Third Party Components
import { Menu } from "react-feather";
import { NavItem, NavLink } from "reactstrap";

import { getUserRole } from "@utils";

const NavbarUser = (props) => {
  // ** Props
  const { skin, setSkin, setMenuVisibility } = props;
  const [isBuddy, setIsBuddy] = useState(null);

  useEffect(() => {
    if (getUserRole() === "buddy") {
      setIsBuddy(true);
    }
  }, []);

  // ** Function to toggle Theme (Light/Dark)
  // const ThemeToggler = () => {
  //   if (skin === 'dark') {
  //     return <Sun className='ficon' onClick={() => setSkin('light')} />
  //   } else {
  //     return <Moon className='ficon' onClick={() => setSkin('dark')} />
  //   }
  // }

  return (
    <Fragment>
      <ul className="navbar-nav d-xl-none d-flex align-items-center">
        <NavItem className="mobile-menu mr-auto">
          <NavLink
            className="nav-menu-main menu-toggle hidden-xs is-active"
            onClick={() => setMenuVisibility(true)}
          >
            <Menu className="ficon" />
          </NavLink>
        </NavItem>
      </ul>
      <div className="bookmark-wrapper d-flex align-items-center">
        <NavItem className="title-container">
          {/* <div className="greet-title">{headerTitle}</div> */}
          <div className="greet-title">Good morning, </div>
          {/* <div className="greet-name">{userData?.first_name}!</div> */}
          <div className="greet-name">{ isBuddy ? "Buddy!" : "Sally!"}</div>
        </NavItem>
      </div>
      <ul className="nav navbar-nav align-items-center ml-auto">
        <UserDropdown />
      </ul>
    </Fragment>
  );
};
export default NavbarUser;
