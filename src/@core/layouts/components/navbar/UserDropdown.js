// ** React Imports
import { useEffect, useState } from "react";

// ** Custom Components
import Avatar from "@components/avatar";
import MenuDropdown from "@components/MenuDropdown";
import SVG from "@components/SVG";

// ** Utils
import { isUserLoggedIn, getUserData } from "@utils";

// ** Store & Actions
import { useDispatch } from "react-redux";
import userReducer from "@store/entities/User/user.reducer";

const { logoutRequest } = userReducer.actions;

// import { handleLogout } from "@store/actions/auth";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-22.jpg";

const UserDropdown = () => {
  // ** Store Vars
  const dispatch = useDispatch();

  // ** State
  const [userData, setUserData] = useState(null);
  const [isBuddy, setIsBuddy] = useState(null);

  //** ComponentDidMount
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      setUserData(getUserData());
    }
  }, []);

  useEffect(() => {
    if (userData && userData.role === "buddy") {
      setIsBuddy(true);
    }
  }, [userData]);

  //** Vars
  const userAvatar = (userData && userData.avatar) || defaultAvatar;

  const menuItems = [
    {
      name: "Profile",
      img: <SVG.Profile />,
      to: "#",
    },
    {
      name: "Log Out",
      img: <SVG.Logout />,
      to: "/login",
      onClick: () => dispatch(logoutRequest()),
    },
  ];

  return (
    <li className="dropdown-user nav-item">
      {isBuddy && (
        <div className="user-buddy">
          You view
          <br />
          schedule:
        </div>
      )}
      <a className="nav-link dropdown-user-link">
        <Avatar img={userAvatar} imgHeight="40" imgWidth="40" />
        <div className="user-nav d-sm-flex d-none">
          <span className="user-name font-weight-bold">
            {(userData && userData["username"]) || "Sally Doe"}
          </span>
          <span className="user-status">
            {(userData && userData.level) || "Level 4"}
          </span>
        </div>
      </a>
      <MenuDropdown items={menuItems} />
    </li>
  );
};

export default UserDropdown;
