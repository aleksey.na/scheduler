// ** React Imports
import { Fragment, useEffect } from "react";
import { useLocation } from "react-router-dom";

// ** Third Party Components
import classnames from "classnames";

// ** Components
import NextButton from "../first-settings-next-button/NextButton";

// ** Utils
import { getUserRole } from "@utils";

// ** Store & Actions
import { useSelector, useDispatch } from "react-redux";
import layoutReducer from "@store/entities/Layout/layout.reducer";

const { handleContentWidth, handleMenuCollapsed, handleMenuHidden } =
  layoutReducer.actions;

// ** Styles
import "animate.css/animate.css";

const LayoutWrapper = (props) => {
  // ** Props
  const { children, appLayout, wrapperClass, transition, routeMeta } = props;

  const location = useLocation();

  // ** Store Vars
  const dispatch = useDispatch();
  const store = useSelector((state) => state);
  // const navbarStore = store.navbar;
  const contentWidth = store.layout.contentWidth;
  const firstSettingsRole = getUserRole() === "first-settings";

  // ** Clean Up Function
  const cleanUp = () => {
    if (routeMeta) {
      if (routeMeta.contentWidth) {
        dispatch(handleContentWidth("full"));
      }
      if (routeMeta.menuCollapsed) {
        dispatch(handleMenuCollapsed(!routeMeta.menuCollapsed));
      }
      if (routeMeta.menuHidden) {
        dispatch(handleMenuHidden(!routeMeta.menuHidden));
      }
    }
  };

  // ** ComponentDidMount
  useEffect(() => {
    if (routeMeta) {
      if (routeMeta.contentWidth) {
        dispatch(handleContentWidth(routeMeta.contentWidth));
      }
      if (routeMeta.menuCollapsed) {
        dispatch(handleMenuCollapsed(routeMeta.menuCollapsed));
      }
      if (routeMeta.menuHidden) {
        dispatch(handleMenuHidden(routeMeta.menuHidden));
      }
    }
    return () => cleanUp();
  }, []);

  return (
    <div
      className={classnames("app-content content overflow-hidden", {
        [wrapperClass]: wrapperClass,
        // "show-overlay": navbarStore.query.length,
      })}
    >
      <div className="content-overlay"></div>
      <div
        className={classnames({
          "content-wrapper": !appLayout,
          "content-area-wrapper": appLayout,
          "container p-0": contentWidth === "boxed",
          [`animate__animated animate__${transition}`]:
            transition !== "none" && transition.length,
        })}
      >
        {firstSettingsRole && location?.pathname !== "/login" && <NextButton />}
        <Fragment>{children}</Fragment>
      </div>
    </div>
  );
};

export default LayoutWrapper;
