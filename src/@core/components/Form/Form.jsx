import React from "react";

import Button from "../Button";

import styles from "./form.module.scss";

const Form = ({
  children,
  containerClass,
  buttonsContainerClass,
  saveHandler,
  deleteHandler,
  isSaveDisabled,
  isDeleteDisabled,
}) => {
  return (
    <form className={`${styles.container} ${containerClass}`}>
      <div>{children}</div>

      <div
        className={`${styles.buttons} ${
          buttonsContainerClass ? buttonsContainerClass : ""
        }`}
      >
        <Button
          variant={"secondary"}
          isDisabled={isDeleteDisabled}
          onClick={deleteHandler}
          className={styles.button}
        >
          Delete
        </Button>
        <Button
          variant={"primary"}
          onClick={saveHandler}
          className={styles.button}
          isDisabled={isSaveDisabled}
        >
          Save
        </Button>
      </div>
    </form>
  );
};

export default Form;
