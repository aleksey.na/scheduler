import React from "react";

import BootstrapModal from "react-bootstrap/Modal";

import styles from "./modal.module.scss";

function Modal({
  children,
  name,
  show,
  onHide,
  containerStyles,
  contentStyles,
  centered,
  ...props
}) {
  return (
    <BootstrapModal
      show={show}
      onHide={() => onHide(name)}
      dialogClassName={`filters-modal ${styles.wrapper} ${containerStyles}`}
      contentClassName={`${contentStyles} ${styles.content}`}
      centered={centered}
      {...props}
    >
      <BootstrapModal.Body className={styles.body}>
        {children}
      </BootstrapModal.Body>
    </BootstrapModal>
  );
}

Modal.defaultProps = {
  show: false,
  containerStyles: "",
  contentStyles: "",
  onHide: () => {},
};

export default Modal;
