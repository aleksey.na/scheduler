import React, { useState } from "react";
import PropTypes from "prop-types";

import { Link } from "react-router-dom";

// ** Components
import SVG from "@components/SVG";

// ** Third Party Components
import {
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from "reactstrap";

import styles from "./menuDropdown.module.scss";

function MenuDropdown({ items, className, isDisabled }) {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const clickHandler = (handler) => {
    if (handler) handler();
  };

  return (
    <Dropdown
      isOpen={dropdownOpen}
      toggle={toggle}
      className={`${styles.dropdownContainer} ${className} ${
        isDisabled ? styles.disabled : ""
      }`}
    >
      <DropdownToggle
        tag="div"
        onClick={(e) => e.preventDefault()}
        className={`${styles.dropdownClosed} ${
          dropdownOpen ? styles.dropdownOpen : ""
        }`}
      >
        <SVG.ArrowDown className={styles.arrow} />
      </DropdownToggle>
      <DropdownMenu right className={styles.menu}>
        {items.map((item, index) => (
          <DropdownItem
            key={index}
            tag={Link}
            to={item.to}
            onClick={() => clickHandler(item?.onClick)}
          >
            {item.img}
            <span className="align-middle">{item.name}</span>
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  );
}

MenuDropdown.propTypes = {
  children: PropTypes.node,
  items: PropTypes.array,
  isDisabled: PropTypes.bool,
  className: PropTypes.string,
  variant: PropTypes.string,
  isLoading: PropTypes.bool,
};

MenuDropdown.defaultProps = {
  children: null,
  items: [],
  isDisabled: false,
  className: "",
  variant: "",
  isLoading: false,
};

export default MenuDropdown;
