import React from "react";

import styles from "./dataItem.module.scss";

const DataItem = ({
  containerClass,
  onClickHandler,
  id,
  isActive,
  isBlured,
  children,
}) => {
  return (
    <div
      onClick={(e) => onClickHandler(e, id)}
      className={`${containerClass ? containerClass : ""} ${styles.container}
      ${isActive ? styles.active : ""}
      ${isBlured ? styles.blured : ""}`}
    >
      {children}
    </div>
  );
};

export default DataItem;
