import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export default function Breadcrumbs(props) {
  const { className = "", items, ...breadcrumbsProps } = props;

  return (
    <Breadcrumb {...breadcrumbsProps}>
      {items.map((item) => item && <Breadcrumb.Item>{item}</Breadcrumb.Item>)}
    </Breadcrumb>
  );
}
