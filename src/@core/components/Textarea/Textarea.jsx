import React, { useState, useEffect, useRef } from "react";
import { Form /* Fade */ /* FormFeedback */ } from "react-bootstrap";
import PropTypes from "prop-types";

// import Button from "../Button";
// import Error from "../Error";

// import SVG from "components/SVG";

import styles from "./textarea.module.scss";

const Textarea = ({
  containerClass,
  inputClass = "",
  error,
  type = "",
  onChange,
  invalid,
  fieldError,
  onClick,
  onBlur,
  setFocus,
  label,
  labelClass,
  placeholder,
  rows,
  ...inputProps
}) => {
  // const [showLabel, handleLabelAppearance] = useState(!!props.value);
  // const [isVisible /* handleVisibleChange */] = useState(false);
  const [isFocused, setIsFocused] = useState(!!inputProps.value);

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  const handleChange = ({ target: { name, value, files } }) => {
    // handleLabelAppearance(!!value);
    onChange(name, value, files);
  };

  const input = useRef(null);

  useEffect(() => {
    if (setFocus) {
      input.current.focus();
    }
  }, [setFocus]);

  // eslint-disable-next-line no-nested-ternary
  // const activeType =
  //   type === "password" ? (isVisible ? "text" : "password") : type;

  // const { value } = inputProps;

  return (
    <div className={containerClass}>
      {label && (
        <Form.Label
          className={`${styles["input-label"]} ${labelClass} 
        `}
        >
          {label}
        </Form.Label>
      )}

      <div className={styles["input-container"]}>
        {/* <Fade in={!!invalid} tag="span" className="invalid-dot" /> */}

        <div className={styles["input-wrap"]}>
          <Form.Control
            as="textarea"
            rows={rows}
            type={type}
            // innerRef={input}
            className={`${styles.input} ${inputClass}`}
            // invalid={invalid}
            {...inputProps}
            onChange={handleChange}
            onClick={onClick}
            onBlur={() => {
              // handleBlur();
              onBlur();
            }}
            onFocus={handleFocus}
            placeholder={placeholder}
          />
          <Form.Text className={styles.error}>{error}</Form.Text>

          {/* {type === "password" && (
          <div className={styles["password-eye-wrapper"]}>
            <Button
              variant="transparent"
              onClick={() => handleVisibleChange(!isVisible)}
              className={`${styles["password-eye"]} ${
                isVisible ? styles.visible : ""
              }`}
            >
              <SVG.Eye className={`${isVisible ? "d-block" : "d-none"}`} />
              <SVG.EyeStroke
                className={`${isVisible ? "d-none" : "d-block"}`}
              />
            </Button>
          </div>
        )} */}
        </div>
        {/* <Fade in={!!showLabel} tag="span" className={`input-label ${labelClass}`}>
        {label}
      </Fade> */}
        {/* <Error component={FormFeedback} error={fieldError} /> */}
        {/* <div className={styles["custom-input-invalid-feedback"]}>{error}</div> */}
      </div>
    </div>
  );
};

Textarea.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  type: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  containerClass: PropTypes.string,
  labelClass: PropTypes.string,
  inputClass: PropTypes.string,
  fieldError: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.shape({}),
  ]),
  invalid: PropTypes.bool,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  setFocus: PropTypes.bool,
};

Textarea.defaultProps = {
  value: "",
  label: "",
  error: "",
  containerClass: "",
  type: "text",
  labelClass: "",
  inputClass: "",
  fieldError: "",
  invalid: false,
  onClick: () => {},
  onBlur: () => {},
  onChange: () => {},
  setFocus: false,
};

export default Textarea;
