import React, { useState } from "react";
import styles from "./calendartable.module.scss";
import moment from "moment";
import { Calendar, momentLocalizer } from "react-big-calendar";
import Toolbar from "react-big-calendar/lib/Toolbar";
import SVG from "@components/SVG";
import {
  Dropdown,
  OverlayTrigger,
  Popover,
  PopoverHeader,
} from "react-bootstrap";

const CalendarTable = ({ shifts, isTradeButton = true }) => {
  moment.locale("ko", {
    week: {
      dow: 1,
      doy: 1,
    },
  });
  const localizer = momentLocalizer(moment);

  //mock events
  const events = [
    {
      title: "Shift 1",
      start: moment("2021-10-10"),
      end: moment("2021-10-10").add(1, "day"),
      color: "#F0319D",
      tradeAccepted: true,
    },
    {
      title: "Shift 2",
      start: moment("2021-10-11"),
      end: moment("2021-10-11").add(3, "day"),
      color: "#6045FF",
      tradeAccepted: true,
    },
    {
      title: "Shift 1",
      start: moment("2021-10-14"),
      end: moment("2021-10-14").add(2, "day"),
      color: "#F0319D",
      tradeAccepted: false,
    },
    {
      title: "Shift 2",
      start: new Date(),
      end: moment().add(1, "day"),
      color: "#6045FF",
    },
    {
      title: "Shift 1",
      start: moment().add(2, "day"),
      end: moment().add(2, "day"),
      color: "#F0319D",
      tradeAccepted: "pending",
    },
  ];

  const Event = (props) => (
    <div className={styles.shift}>
      {props.event["tradeAccepted"] !== undefined && (
        <OverlayTrigger
          trigger={["hover", "focus"]}
          placement="top"
          overlay={
            <Popover id={`popover-positioned-top`}>
              <PopoverHeader>
                Dates which are requested as Trades
              </PopoverHeader>
            </Popover>
          }
        >
          <div className={styles.tradeMarker}>
            {props.event.tradeAccepted === true && <SVG.Like />}
            {props.event.tradeAccepted === false && <SVG.LikeContour />}
          </div>
        </OverlayTrigger>
      )}
      <div
        className={styles.shiftColor}
        style={{ backgroundColor: props.event.color }}
      ></div>
      {props.event.title}
    </div>
  );

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <div
      ref={ref}
      className={styles.customToggle}
      // TODO: add OnClick event
      // onClick={(e) => {
      //   e.preventDefault();
      //   onClick(e);
      // }}
    >
      {children}
      <SVG.ArrowDown color="#6B7DAA" />
    </div>
  ));

  const CustomMenu = React.forwardRef((props, ref) => {
    const [value, setValue] = useState("");

    return (
      <div
        ref={ref}
        className={styles.dropdownMenu}
        aria-labelledby={props.labeledBy}
      >
        <ul>{React.Children.toArray(props.children)}</ul>
      </div>
    );
  });

  const PopoverTitl = React.forwardRef((props, ref) => {
    const [value, setValue] = useState("");

    return (
      <>
        <button
          ref={ref}
          className={styles.popoverTitle}
          //   onClick={() => setActiveItem("Trades")}
        >
          Trade
        </button>
        <br />
        <button
          ref={ref}
          className={styles.popoverTitle}
          //   onClick={() => console.log("trade directly")}
        >
          Trade directly
        </button>
      </>
    );
  });

  class CalendarToolbar extends Toolbar {
    render() {
      const itemStyle = {
        color: "#b3c1e3",
        paddingLeft: 5,
        fontSize: "14px",
      };

      return (
        <div className={styles.headerContainer}>
          <div className={styles.buttonGroup} style={{ flexGrow: "8" }}>
            <div
              className={styles.arrowButton}
              onClick={() => this.navigate("PREV")}
            >
              <SVG.DoubleArrowLeft color="#6B7DAA" />
            </div>
            <div
              className={styles.arrowButton}
              onClick={() => this.navigate("PREV")}
            >
              <SVG.ArrowLeft color="#6B7DAA" />
            </div>
            <div className={styles.calendarPicker}>
              <div>{moment(this.props.date).format("MMMM, YYYY")}</div>
              <SVG.Calendar color="#6b7daa" />
            </div>
            <div
              className={styles.arrowButton}
              onClick={() => this.navigate("NEXT")}
            >
              <SVG.ArrowLeft
                color="#6B7DAA"
                style={{ transform: "rotate(-180deg)" }}
              />
            </div>
            <div
              className={styles.arrowButton}
              onClick={() => this.navigate("NEXT")}
            >
              <SVG.DoubleArrowLeft
                color="#6B7DAA"
                style={{ transform: "rotate(-180deg)" }}
              />
            </div>
          </div>
          <div className={`${styles.buttonGroup} flex-grow-1`}>
            <div className={styles.sortBy}>Sort by: </div>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle
                variant="success"
                id="dropdown-basic"
                as={CustomToggle}
              >
                All
              </Dropdown.Toggle>
              <Dropdown.Menu as={CustomMenu}>
                {shifts.map((shift, index) => (
                  <Dropdown.Item
                    style={itemStyle}
                    href={`#/action-${index}`}
                    key={index}
                  >
                    <div>
                      <div className={styles.checkbox}></div>
                      {shift}
                    </div>
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
            <div className={`${styles.arrowButton} bg-transparent`}>
              <SVG.TileView fill="#6B7DAA" />
            </div>
            <div className={`${styles.arrowButton} bg-transparent`}>
              <SVG.ListView fill="#6B7DAA" />
            </div>
          </div>
        </div>
      );
    }
  }

  return (
    <>
      <Calendar
        className={styles.calendar}
        localizer={localizer}
        defaultView="month"
        events={events}
        startAccessor="start"
        endAccessor="end"
        components={{
          toolbar: CalendarToolbar,
          month: {
            header: ({ date }) => {
              return <div>{moment(date).format("dddd")}</div>;
            },
            dateHeader: ({ date, isOffRange }) => {
              return (
                <div className={styles.dateHeader}>
                  <div className={isOffRange ? "rbc-offrange" : ""}>
                    {moment(date).format("D")}
                  </div>
                  {!isOffRange && isTradeButton && (
                    <OverlayTrigger
                      trigger="click"
                      placement="right"
                      overlay={
                        <Popover id={`popover-positioned-right`}>
                          <PopoverHeader as={PopoverTitl}></PopoverHeader>
                        </Popover>
                      }
                    >
                      <button className={styles.tradeShiftButton}>
                        <div>...</div>
                      </button>
                    </OverlayTrigger>
                  )}
                </div>
              );
            },
          },
          eventWrapper: Event,
        }}
      />
    </>
  );
};

export default CalendarTable;
