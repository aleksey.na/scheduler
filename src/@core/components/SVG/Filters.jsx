import React from "react";
import PropTypes from "prop-types";

export default function Filters(props) {
  return (
    <svg width={props.width} height={props.height} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={props.className}>
        <path d="M4 21V14" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M4 10V3" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M12 21V12" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M12 8V3" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M20 21V16" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M20 12V3" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M1 14H7" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M9 8H15" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
        <path d="M17 16H23" stroke={props.fill} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
    </svg>

  )}

  Filters.propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        height: PropTypes.number,
        fill: PropTypes.string,
    };

    Filters.defaultProps = {
      className: "",
      width: 24,
      height: 24,
      fill: "#B3C1E3",
    };
