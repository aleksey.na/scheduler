import React from "react";
import PropTypes from "prop-types";

export default function Exclamation(props) {
  return (
    <svg
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox={`0 0 ${props.width} ${props.height}`}
      fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="15" width="15" height="15" rx="7.5" transform="rotate(90 15 0)" fill={props.fill}/>
        <rect x="8" y="9" width="1" height="5" transform="rotate(-180 8 9)" fill="white"/>
        <rect width="1" height="1" transform="matrix(-1 -8.74228e-08 -8.74228e-08 1 8 10)" fill="white"/>
      </svg>
      
  );
}

Exclamation.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  fill: PropTypes.string,
};

Exclamation.defaultProps = {
  className: "",
  width: 15,
  height: 15,
  fill: "#2054D7",
};
