import React from "react";
import PropTypes from "prop-types";

export default function FlipArrows (props) {
  return (
    <svg width={props.width} height={props.height} viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" className={props.className}>
        <rect x="29.501" y="29.5" width="29" height="29" rx="4.5" transform="rotate(-180 29.501 29.5)" fill="white" stroke={props.fill}/>
        <path d="M17.501 14L17.501 20M17.501 20L20.001 17.75M17.501 20L15.001 17.75" stroke={props.fill} strokeLinecap="round"/>
        <path d="M11.501 17L11.501 11M11.501 11L14.001 13.25M11.501 11L9.00098 13.25" stroke={props.fill} strokeLinecap="round"/>
    </svg>
  )}

    FlipArrows.propTypes = {
        className: PropTypes.string,
        width: PropTypes.number,
        height: PropTypes.number,
        fill: PropTypes.string,
    };

    FlipArrows.defaultProps = {
      className: "",
      width: 30,
      height: 30,
      fill: "#B3C1E3",
    };
