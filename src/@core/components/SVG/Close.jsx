import React from "react";
import PropTypes from "prop-types";

export default function CloseX(props) {
  return (
    <svg
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox={`0 0 ${props.width} ${props.height}`}
      // fill={props.fill}
      fill="none"
      viewport="0 0 12 12"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line
        x1="1"
        y1="11"
        x2="11"
        y2="1"
        stroke={props.fill}
        strokeWidth="2.5"
      />
      <line
        x1="1"
        y1="1"
        x2="11"
        y2="11"
        stroke={props.fill}
        strokeWidth="2.5"
      />
    </svg>
  );
}

CloseX.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  fill: PropTypes.string,
};

CloseX.defaultProps = {
  className: "",
  width: 16,
  height: 16,
  fill: "#5f7fd3",
};
