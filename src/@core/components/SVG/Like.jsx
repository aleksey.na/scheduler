import React from "react";
import PropTypes from "prop-types";

export default function Like(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clip-path="url(#clip0_1336:12228)">
        <path
          d="M1.09375 14.375H2.65625C3.25938 14.375 3.75 13.8844 3.75 13.2812V6.09375C3.75 5.49062 3.25938 5 2.65625 5H1.09375C0.490625 5 0 5.49062 0 6.09375V13.2812C0 13.8844 0.490625 14.375 1.09375 14.375Z"
          fill={props.fill}
        />
        <path
          d="M7.98812 0.46875C7.36312 0.46875 7.05062 0.78125 7.05062 2.34375C7.05062 3.82875 5.6125 5.02375 4.6875 5.63937V13.3819C5.68812 13.845 7.69125 14.5312 10.8006 14.5312H11.8006C13.0194 14.5312 14.0569 13.6562 14.2631 12.4563L14.9631 8.39375C15.2256 6.8625 14.0506 5.46875 12.5006 5.46875H9.55062C9.55062 5.46875 10.0194 4.53125 10.0194 2.96875C10.0194 1.09375 8.61312 0.46875 7.98812 0.46875Z"
          fill={props.fill}
        />
      </g>
      <defs>
        <clipPath id="clip0_1336:12228">
          <rect width="15" height="15" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
}

Like.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Like.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 15,
  height: 15,
};
