import React from "react";
import PropTypes from "prop-types";

export default function Information(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="14.5"
        y="0.5"
        width="14"
        height="14"
        rx="7"
        transform="rotate(90 14.5 0.5)"
        stroke={props.fill}
      />
      <rect x="7" y="6" width="1" height="5" fill="#6B7DAA" />
      <rect
        width="1"
        height="1"
        transform="matrix(1 0 0 -1 7 5)"
        fill={props.fill}
      />
    </svg>
  );
}

Information.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Information.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 15,
  height: 15,
};
