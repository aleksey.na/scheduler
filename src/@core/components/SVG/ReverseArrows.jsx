import React from "react";
import PropTypes from "prop-types";

export default function ReverseArrows(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 11 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M4 3.5H10M10 3.5L7.75 1M10 3.5L7.75 6"
        stroke="#B3C1E3"
        strokeLinecap="round"
        fill={props.fill}
      />
      <path
        d="M7 9.5H1M1 9.5L3.25 7M1 9.5L3.25 12"
        stroke="#B3C1E3"
        strokeLinecap="round"
        fill={props.fill}
      />
    </svg>
  );
}

ReverseArrows.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

ReverseArrows.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 9,
  height: 11,
};
