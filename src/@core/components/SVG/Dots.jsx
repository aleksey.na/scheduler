import React from "react";
import PropTypes from "prop-types";

export default function Dots(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 21 5"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        x="21"
        width="5"
        height="5"
        rx="2.5"
        transform="rotate(90 21 0)"
        fill={props.fill}
      />
      <rect
        x="13"
        width="5"
        height="5"
        rx="2.5"
        transform="rotate(90 13 0)"
        fill={props.fill}
      />
      <rect
        x="5"
        width="5"
        height="5"
        rx="2.5"
        transform="rotate(90 5 0)"
        fill={props.fill}
      />
    </svg>
  );
}

Dots.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Dots.defaultProps = {
  className: "",
  fill: "#B3C1E3",
  width: 25,
  height: 5,
};
