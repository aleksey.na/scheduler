import React from "react";
import PropTypes from "prop-types";

export default function DoubleArrowLeft(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 12 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M6 1L2 5L6 9"
        stroke={props.fill}
        strokeWidth="1.5"
        strokeLinecap="round"
      />
      <path
        d="M11 1L7 5L11 9"
        stroke={props.fill}
        strokeWidth="1.5"
        strokeLinecap="round"
      />
    </svg>
  );
}

DoubleArrowLeft.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

DoubleArrowLeft.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 12,
  height: 10,
};
