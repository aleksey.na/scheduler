import React from "react";
import PropTypes from "prop-types";

export default function Search(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M14.5214 13.6626L10.9514 9.94962C11.8693 8.85844 12.3723 7.48552 12.3723 6.05624C12.3723 2.71688 9.65537 0 6.31601 0C2.97665 0 0.259766 2.71688 0.259766 6.05624C0.259766 9.3956 2.97665 12.1125 6.31601 12.1125C7.56965 12.1125 8.76431 11.7344 9.78571 11.0166L13.3829 14.7578C13.5332 14.9139 13.7354 15 13.9521 15C14.1573 15 14.3519 14.9218 14.4996 14.7796C14.8134 14.4776 14.8235 13.9768 14.5214 13.6626ZM6.31601 1.57989C8.78432 1.57989 10.7924 3.58793 10.7924 6.05624C10.7924 8.52456 8.78432 10.5326 6.31601 10.5326C3.8477 10.5326 1.83966 8.52456 1.83966 6.05624C1.83966 3.58793 3.8477 1.57989 6.31601 1.57989Z" fill={props.fill}/>
      </svg>
      
  );
}

Search.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Search.defaultProps = {
  className: "",
  fill: "#B3C1E3",
  width: 15,
  height: 15,
};
