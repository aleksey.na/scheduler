import React from "react";
import PropTypes from "prop-types";

export default function Clock(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.8846 11.1777L10.2354 9.19082V5.14359C10.2354 4.73665 9.90649 4.40771 9.49955 4.40771C9.09261 4.40771 8.76367 4.73665 8.76367 5.14359V9.55879C8.76367 9.79058 8.87259 10.0092 9.05802 10.1475L12.0015 12.3551C12.1339 12.4544 12.2885 12.5023 12.4423 12.5023C12.6667 12.5023 12.8875 12.4014 13.0317 12.2072C13.2761 11.8826 13.2098 11.4212 12.8846 11.1777Z"
        fill={props.fill}
      />
      <path
        d="M9.5 0C4.26138 0 0 4.26138 0 9.5C0 14.7386 4.26138 19 9.5 19C14.7386 19 19 14.7386 19 9.5C19 4.26138 14.7386 0 9.5 0ZM9.5 17.5283C5.07378 17.5283 1.47172 13.9262 1.47172 9.5C1.47172 5.07378 5.07378 1.47172 9.5 1.47172C13.927 1.47172 17.5283 5.07378 17.5283 9.5C17.5283 13.9262 13.9262 17.5283 9.5 17.5283Z"
        fill={props.fill}
      />
    </svg>
  );
}

Clock.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Clock.defaultProps = {
  className: "",
  fill: "#B3C1E3",
  width: 19,
  height: 19,
};
