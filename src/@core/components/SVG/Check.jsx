import React from "react";
import PropTypes from "prop-types";

export default function Check(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1 1L5 5L9 1" stroke="#2054D7" strokeWidth="1.5" strokeLinecap="round"/>
    </svg>

  );
}

Check.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

Check.defaultProps = {
  className: "",
  fill: "#2054D7",
  width: 10,
  height: 7,
};