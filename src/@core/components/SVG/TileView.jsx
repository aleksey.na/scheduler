import React from "react";
import PropTypes from "prop-types";

export default function TileView(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        width="9.08696"
        height="9.08696"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 0 0)"
        fill="#6B7DAA"
      />
      <rect
        width="9.08696"
        height="9.08696"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 0 9.91309)"
        fill="#6B7DAA"
      />
      <rect
        width="9.08696"
        height="9.08696"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 9.91309 0)"
        fill="#6B7DAA"
      />
      <rect
        width="9.08696"
        height="9.08696"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 9.91309 9.91309)"
        fill="#6B7DAA"
      />
    </svg>
  );
}

TileView.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

TileView.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 19,
  height: 19,
};
