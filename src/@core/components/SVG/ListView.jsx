import React from "react";
import PropTypes from "prop-types";

export default function ListView(props) {
  return (
    <svg
      style={props.style}
      className={props.className}
      width={props.width}
      height={props.height}
      viewBox="0 0 19 19"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect
        width="4.13043"
        height="19"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 0 0.826172)"
        fill="#DDE6FC"
      />
      <rect
        width="4.13043"
        height="19"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 0 7.43481)"
        fill="#DDE6FC"
      />
      <rect
        width="4.13043"
        height="19"
        rx="1"
        transform="matrix(4.37114e-08 1 1 -4.37114e-08 0 14.0435)"
        fill="#DDE6FC"
      />
    </svg>
  );
}

ListView.propTypes = {
  className: PropTypes.string,
  fill: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

ListView.defaultProps = {
  className: "",
  fill: "#6b7daa",
  width: 19,
  height: 19,
};
