import Logout from "./Logout";
import Profile from "./Profile";
import ArrowDown from "./ArrowDown";
import ArrowLeft from "./ArrowLeft";
import DoubleArrowLeft from "./DoubleArrowLeft";
import Home from "./Home";
import Calendar from "./Calendar";
import Settings from "./Settings";
import Statistics from "./Statistics";
import Suitcase from "./Suitcase";
import TileView from "./TileView";
import ListView from "./ListView";
import Close from "./Close";
import FlipArrows from "./FlipArrows";
import Filters from "./Filters";
import Pencil from "./Pencil";
import Bin from "./Bin";
import Check from "./Check";
import Search from "./Search";
import Link from "./Link";
import ReverseArrows from "./ReverseArrows";
import Exclamation from "./Exclamation";
import Like from "./Like";
import LikeContour from "./LikeContour";
import Clock from "./Clock";
import Dots from "./Dots";
import Information from "./Information";

const icons = {
  Logout,
  Profile,
  ArrowDown,
  ArrowLeft,
  Home,
  Calendar,
  Settings,
  Statistics,
  Suitcase,
  DoubleArrowLeft,
  TileView,
  ListView,
  Close,
  FlipArrows,
  Filters,
  Pencil,
  Bin,
  Check,
  Search,
  Link,
  ReverseArrows,
  Exclamation,
  Like,
  LikeContour,
  Clock,
  Dots,
  Information,
};

export default icons;
