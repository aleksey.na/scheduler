import React from "react";

import styles from "./contentContainer.module.scss";

const ContentContainer = ({ children, className }) => {
  return <div className={`${styles.container} ${className}`}>{children}</div>;
};

export default ContentContainer;
