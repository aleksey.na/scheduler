import React, { useState, useEffect, useRef } from "react";
import { Form /* Fade */ /* FormFeedback */ } from "react-bootstrap";
import PropTypes from "prop-types";

import Select, { components } from "react-select";

// import Button from "../Button";
// import Error from "../Error";

import SVG from "@components/SVG";

import styles from "./select-input.module.scss";
import { customStyles } from "./index";

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <SVG.ArrowDown
        style={{ transform: "rotate(90deg)" }}
        width={24}
        height={17}
      />
    </components.DropdownIndicator>
  );
};

const SelectInput = ({
  containerClass,
  className,
  view,
  inputClass = "",
  error,
  type = "",
  onChange,
  name,
  value,
  invalid,
  fieldError,
  onClick,
  onBlur,
  setFocus,
  label,
  labelClass,
  labelDescription,
  placeholder,
  options,
  noOptionsMessage,
  isDisabled,
  ...inputProps
}) => {
  const [isFocused, _] = useState(!!inputProps.value);
  const [defaultValue, setDefaultValue] = useState("");

  const handleChange = ({ value }) => {
    onChange(name, value);
  };

  useEffect(() => {
    if (options?.length) {
      const defaultValue = options?.find(
        (o) => o.value.toString() === value?.toString()
      );
      if (defaultValue) {
        setDefaultValue(defaultValue);
      } else {
        setDefaultValue("");
      }
    }
  }, [options]);

  return (
    <div className={`${containerClass} ${styles[view]}`}>
      <div>
        {label && (
          <Form.Label
            className={`${styles["input-label"]} ${labelClass}  
        ${isFocused ? styles["custom-input-label-focus"] : ""} 
        `}
          >
            {label}
          </Form.Label>
        )}
        {labelDescription && (
          <div className={styles["label-description"]}>{labelDescription}</div>
        )}
      </div>
      <div
        className={`${styles["input-container"]} ${className} ${
          isDisabled ? styles["disabled"] : ""
        }`}
      >
        <div className={styles["input-wrap"]}>
          <Select
            // menuPlacement={"auto"}
            components={{ DropdownIndicator }}
            name={name}
            value={defaultValue}
            options={options}
            noOptionsMessage={noOptionsMessage}
            onChange={handleChange}
            onBlur={onBlur}
            styles={customStyles}
            isFocused={isFocused}
            isDisabled={isDisabled}
            placeholder={placeholder ? placeholder : ""}
            // menuIsOpen={true}
          />
          <Form.Text className={styles.error}>{error}</Form.Text>
        </div>
        {/* <Fade in={!!showLabel} tag="span" className={`input-label ${labelClass}`}>
        {label}
      </Fade> */}
        {/* <Error component={FormFeedback} error={fieldError} /> */}
        {/* <div className={styles["custom-input-invalid-feedback"]}>{error}</div> */}
      </div>
    </div>
  );
};

SelectInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  type: PropTypes.string,
  view: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  containerClass: PropTypes.string,
  labelClass: PropTypes.string,
  inputClass: PropTypes.string,
  fieldError: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.shape({}),
  ]),
  invalid: PropTypes.bool,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  setFocus: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape()),
};

SelectInput.defaultProps = {
  view: "column",
  value: "",
  label: "",
  error: "",
  containerClass: "",
  type: "text",
  labelClass: "",
  inputClass: "",
  fieldError: "",
  invalid: false,
  onClick: () => {},
  onBlur: () => {},
  onChange: () => {},
  setFocus: false,
  options: [],
};

export default SelectInput;
