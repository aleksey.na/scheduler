export { default } from "./SelectInput";

export const customStyles = {
  control: (styles) => ({
    ...styles,
    border: "none",
    boxShadow: "none",
    width: "100%",
    height: "61px",
    backgroundColor: "transparent",
    zIndex: 999,

    ":hover": {
      border: "none",
      boxShadow: "none",
    },
  }),

  menu: (styles) => ({
    ...styles,
    backgroundColor: "#ffff",
    paddingTop: "28px",
    marginTop: "5px",
    border: "1px solid #B3C1E3",
    borderTop: "none",
    borderTopLeftRadius: "0",
    borderTopRightRadius: "0",
    borderBottomRightRadius: "10px",
    borderBottomLeftRadius: "10px",
    boxShadow: "none",
    left: "-1px",
    width: "calc(100% + 2px)",
    zIndex: "9999",

    ":before": {
      content: '""',
      width: "15px",
      height: "1px",
      backgroundColor: "#B3C1E3",
      position: "absolute",
      right: "-8px",
      top: "-5px",
      transform: "rotate(90deg)",
    },

    ":after": {
      content: '""',
      width: "15px",
      height: "1px",
      backgroundColor: "#B3C1E3",
      position: "absolute",
      left: "-8px",
      top: "-5px",
      transform: "rotate(90deg)",
    },
  }),

  menuList: (styles) => ({
    ...styles,
    backgroundColor: "#ffff",
    top: "-20px",
    zIndex: "-1",
    marginBottom: "0",
    paddingBottom: "0",
  }),

  option: (styles, { isDisabled, isFocused, isSelected }) => {
    return {
      ...styles,
      height: "48px",
      cursor: isDisabled ? "not-allowed" : "default",
      backgroundColor: isFocused ? "#F0F4FF" : "#FFFF",
      color: isFocused ? "#2054D7" : isSelected ? "#2054D7" : "#B3C1E3",
      fontSize: "18px",
      lineHeight: "21.6px",

      ":active": {
        // ...styles[":active"],
      },
    };
  },
  input: (styles) => ({
    ...styles,
    padding: "0 25px",
    width: "100%",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    fontSize: "18px",
    lineHeight: "21.6px",
    color: "#151b2b",
  }),
  singleValue: (styles) => ({
    ...styles,
    width: "100%",
    textOverflow: "ellipsis",
    position: "absolute",
    overflow: "hidden",
    whiteSpace: "nowrap",
    fontSize: "18px",
    lineHeight: "21.6px",
    color: "#151b2b",
    padding: "0 5px 0 25px",
  }),
  placeholder: (styles) => ({
    ...styles,
    fontFamily: "GothamLight",
    fontSize: "18px",
    lineHeight: "21.6px",
    textOverflow: "ellipsis",
    overflow: "hidden",
    whiteSpace: "nowrap",
    color: "#b3c1e3",
    padding: "0 25px",
    margin: "0",
  }),
  valueContainer: (styles) => ({ ...styles, padding: "0" }),
  indicatorSeparator: () => ({
    backgroundColor: "transparent",
  }),
  noOptionsMessage: (styles) => ({
    ...styles,
    fontFamily: "GothamLight",
    fontSize: "14px",
    lineHeight: "21.6px",
    color: "#b3c1e3",
  }),

  dropdownIndicator: (styles, { selectProps }) => ({
    ...styles,
    transition: "0.5s",
    transform: selectProps.menuIsOpen ? "rotate(360deg)" : "rotate(180deg)",
    marginRight: "15px",
    paddingTop: "4px",
  }),
};
