import React, { useState, forwardRef } from "react";
import { Calendar as CalendarPicker } from "react-calendar";
import { Modal } from "react-bootstrap";
import SVG from "@components/SVG";
import "react-calendar/dist/Calendar.css";
import styles from "./calendar.module.scss";
import moment from "moment";

const Calendar = (props) => {
  const [startDate, setStartDate] = useState(new Date());
  const [isShowModal, setShowModal] = useState(false);

  return (
    <div className="calendar-small">
      <CalendarPicker
        onChange={setStartDate}
        value={startDate}
        className={styles.calendarWrapper}
        tileClassName={styles.tile}
        formatShortWeekday={(locale, date) => moment(date).format("dd")}
        navigationLabel={({ date, label, locale, view }) => (
          <div
            className={`${styles.monthLabel}`}
          >
            <div>{moment(date).format("MMMM, yyyy")}</div>
          </div>
        )}
        nextLabel={
          <div className={styles.arrow}>
            <SVG.ArrowLeft className={styles.flip} />
          </div>
        }
        next2Label={
          <div className={styles.arrow}>
            <SVG.DoubleArrowLeft className={styles.flip} />
          </div>
        }
        prevLabel={
          <div className={styles.arrow}>
            <SVG.ArrowLeft />
          </div>
        }
        prev2Label={
          <div className={styles.arrow}>
            <SVG.DoubleArrowLeft />
          </div>
        }
        calendarType="US"
        locale="en-US"
      />
    </div>
  );
};

export default Calendar;
