import React, { useState, forwardRef } from "react";
import Calendar from "react-calendar";
import { Modal } from "react-bootstrap";
import SVG from "@components/SVG";
import "react-calendar/dist/Calendar.css";
import styles from "./datepicker.module.scss";
import moment from "moment";


const DatePickerInput = (props) => {
  const [startDate, setStartDate] = useState(new Date());
  const [isShowModal, setShowModal] = useState(false);

  return (
    <>
      <div className={`${styles.calendar} mr-1`} onClick={setShowModal}>
        <SVG.Calendar fill="#6B7DAA" width={19} height={19} />
        {startDate.length && moment(startDate[0]).format("YYYY/MM/DD")}
      </div>
      <div className={styles.calendar} onClick={setShowModal}>
        <SVG.Calendar fill="#6B7DAA" width={19} height={19} />
        {startDate.length > 1 && moment(startDate[1]).format("YYYY/MM/DD")}
      </div>
      <Modal
        show={isShowModal}
        setShow={setShowModal}
        centered
        className="calendar-modal"
      >
        <Modal.Body>
          <div className={styles.modalHeader}>
            <div className={styles.close} onClick={() => setShowModal(false)}>
              <SVG.Close fill="#b3c1e3" />
            </div>
            <div>
              <h4>Calendar</h4>
              <div>Select a period of days</div>
            </div>
          </div>
          <div className={styles.modalBody}>
            <Calendar
              onChange={setStartDate}
              value={startDate}
              selectRange={true}
              className={styles.calendarWrapper}
              tileClassName={styles.tile}
              navigationLabel={({ date, label, locale, view }) => (
                <div className={`${styles.monthLabel} d-flex justify-content-between mx-auto`}>
                  <div>{moment(date).format("MMMM, yyyy")}</div>
                  <SVG.Calendar fill="#6B7DAA" width={19} height={19} />
                </div>
              )}
              nextLabel={<div className={styles.arrow}><SVG.ArrowLeft className={styles.flip}/></div>}
              next2Label={<div className={styles.arrow}><SVG.DoubleArrowLeft className={styles.flip}/></div>}
              prevLabel={<div className={styles.arrow}><SVG.ArrowLeft/></div>}
              prev2Label={<div className={styles.arrow}><SVG.DoubleArrowLeft/></div>}
              calendarType="US"
              locale="en-US"
            />
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default DatePickerInput;
