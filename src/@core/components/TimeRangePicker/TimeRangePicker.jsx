import React from "react";

// ** Third party libraries
import PropTypes from "prop-types";

// ** Components
import TimeInput from "./TimeInput";
import SVG from "@components/SVG";

import styles from "./timeRangePicker.module.scss";

function TimeRangePicker({
  state,
  onStateChange,
  onChange,
  mainLabel,
  containerClass,
  btnDisabled,
}) {
  const handleTimeReverse = () => {
    if (btnDisabled) return;
    onStateChange({
      ...state,
      start: state.finish,
      finish: state.start,
    });
  };

  return (
    <div className={`${styles.wrapper} ${containerClass}`}>
      <div className={`${styles.mainLabel}`}>{mainLabel}</div>
      <div className={styles.inputsContainer}>
        <TimeInput
          name={"start"}
          label={"Start"}
          placeholder={"2021-11-04T03:00:05.879Z"}
          value={state.start}
          onChange={onChange}
        />
        <div onClick={handleTimeReverse} className={styles.reverser}>
          <SVG.ReverseArrows />
        </div>
        <TimeInput
          name={"finish"}
          label={"Finish"}
          placeholder={"2021-11-04T18:00:05.879Z"}
          value={state.finish}
          onChange={onChange}
        />
      </div>
    </div>
  );
}

TimeRangePicker.propTypes = {
  state: PropTypes.shape({}),
  mainLabel: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date),
  ]),
  onChange: PropTypes.func,
  containerClass: PropTypes.string,
};

TimeRangePicker.defaultProps = {
  state: {},
  value: "",
  mainLabel: "Label",
  containerClass: "",
  invalid: false,
  onChange: () => {},
};

export default TimeRangePicker;
