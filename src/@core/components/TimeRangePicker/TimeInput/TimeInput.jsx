import React, { useState, useEffect, useRef } from "react";

// Third party libraries
import PropTypes from "prop-types";
import moment from "moment";

// ** Components
import { Form } from "react-bootstrap";
import SVG from "@components/SVG";

import styles from "./timeInput.module.scss";

const formatTimeForState = (time) => {
  const format = "LT";
  const timeStr = moment(time).format(format);
  const parts = timeStr.split(" ");
  const timeArr = parts[0].split(":");
  return [...timeArr, parts[1]];
};

const formatToHumanTime = (value, hours, minutes, period) => {
  if (hours === "12") {
    hours = "00";
  }
  if (period.toLowerCase() === "pm") {
    hours = parseInt(hours, 10) + 12;
  }
  const date = new Date(value ? value : null).setHours(hours);
  const next = new Date(date).setMinutes(minutes);

  return new Date(next).toISOString();
};

const TimeInput = ({
  containerClass,
  className,
  inputClass = "",
  error,
  type = "",
  onChange,
  name,
  value,
  invalid,
  fieldError,
  onClick,
  setFocus,
  label,
  labelClass,
  labelDescription,
  placeholder,
  options,
  noOptionsMessage,
  isDisabled,
  ...inputProps
}) => {
  const [state, setState] = useState("");
  const [isDirty, setDirty] = useState(false);
  const containerRef = useRef(null);

  const hoursContRef = useRef(null);
  const minutesContRef = useRef(null);
  const hourRef = useRef(null);
  const minuteRef = useRef(null);

  const [menuOpen, setMenuOpen] = useState(false);

  const handleOpenMenu = () => {
    setMenuOpen(!menuOpen);
  };

  const handleOptionClick = (e, option) => {
    e.preventDefault();
    const newState = [...state];
    newState[option.index] = option.value;
    setDirty(true);
    const [hours, minutes, period] = newState;
    onChange(name, formatToHumanTime(value, hours, minutes, period));
  };

  useEffect(() => {
    if (placeholder && !value) {
      setState(formatTimeForState(placeholder));
    } else if (value) {
      setState(formatTimeForState(value));
    }
  }, []);

  useEffect(() => {
    // Close menu on outside click
    const ref = containerRef;
    function handleClickOutside(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        setMenuOpen(false);
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  useEffect(() => {
    if (value) {
      setState(formatTimeForState(value));
    }
  }, [value]);

  useEffect(() => {
    if (menuOpen) {
      hourRef.current.scrollIntoView({ block: "center" });
      minuteRef.current.scrollIntoView({ block: "center" });
    }
  }, [menuOpen]);

  const maxHoursNumber = 12;
  const maxMinutesNumber = 60;
  const periods = ["am", "pm"];
  const hoursOptions = new Array(maxHoursNumber).fill("").map((_, index) => ({
    value: `${index + 1}`,
    label: `${index + 1}`,
    index: 0,
  }));
  const minutesOptions = new Array(maxMinutesNumber)
    .fill("")
    .map((_, index) => ({
      value: `${index}`,
      label: `${index}`,
      index: 1,
    }));
  const periodsOptions = periods.map((period, index) => ({
    value: `${period}`,
    label: `${period}`,
    index: 2,
  }));

  if (state) {
    var [hours, minutes, period] = state;
  }

  return (
    <div className={`${containerClass} ${styles.container}`}>
      <div>
        {label && (
          <Form.Label
            className={`${styles.inputLabel} ${labelClass}
        `}
          >
            {label}
          </Form.Label>
        )}
        {labelDescription && (
          <div className={styles.labelDescription}>{labelDescription}</div>
        )}
      </div>
      <div ref={containerRef} className={styles.positionWrapper}>
        <div className={`${styles.inputContainer}`}>
          <div
            onClick={handleOpenMenu}
            className={`${styles.wrapper} ${
              menuOpen ? styles.wrapperOpen : ""
            } ${isDirty ? styles.filledValue : ""}`}
          >
            <div className={styles.inputSection}>
              <span>{+hours < 10 && +hours !== 0 ? `0${hours}` : hours}</span>
              <SVG.ArrowDown
                width={15}
                height={10}
                className={`${styles.chevron} ${
                  menuOpen ? styles.chevronOpen : ""
                }`}
              />
            </div>
            <div className={styles.inputSection}>
              <span>
                {+minutes < 10 && +minutes !== 0 ? `${minutes}` : minutes}
              </span>
              <SVG.ArrowDown
                width={15}
                height={10}
                className={`${styles.chevron} ${
                  menuOpen ? styles.chevronOpen : ""
                }`}
              />
            </div>
            <div className={styles.inputSection}>
              <span>{period?.toLowerCase()}</span>
              <SVG.ArrowDown
                width={15}
                height={10}
                className={`${styles.chevron} ${
                  menuOpen ? styles.chevronOpen : ""
                }`}
              />
            </div>
          </div>
          <ul className={`${styles.list} ${menuOpen ? styles.openList : ""}`}>
            <div className={`${styles.collapse} ${styles.show}`}>
              {menuOpen && (
                <div ref={hoursContRef} className={styles.optionsContainer}>
                  {hoursOptions.map((option, index) => (
                    <li
                      key={index}
                      onClick={(e) => handleOptionClick(e, option)}
                      className={`${
                        +option.value === +hours ? styles.optionSelected : ""
                      }`}
                      ref={+option.value === +hours ? hourRef : null}
                    >
                      {option.label}
                    </li>
                  ))}
                </div>
              )}

              {menuOpen && (
                <div ref={minutesContRef} className={styles.optionsContainer}>
                  {minutesOptions.map((option, index) => (
                    <li
                      key={index}
                      onClick={(e) => handleOptionClick(e, option)}
                      className={`${
                        +option.value === +minutes ? styles.optionSelected : ""
                      }`}
                      ref={+option.value === +minutes ? minuteRef : null}
                    >
                      {option.label}
                    </li>
                  ))}
                </div>
              )}

              {menuOpen && (
                <div
                  className={`${styles.optionsContainer} ${styles.noScroll}`}
                >
                  {periodsOptions.map((option, index) => (
                    <li
                      key={index}
                      onClick={(e) => handleOptionClick(e, option)}
                      className={`${
                        option.value.toLowerCase() === period.toLowerCase()
                          ? styles.optionSelected
                          : ""
                      }`}
                    >
                      {option.label}
                    </li>
                  ))}
                </div>
              )}
            </div>
          </ul>
        </div>
      </div>
    </div>
  );
};

TimeInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date),
    PropTypes.arrayOf(PropTypes.string),
  ]),
  type: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func,
  containerClass: PropTypes.string,
  labelClass: PropTypes.string,
  inputClass: PropTypes.string,
  fieldError: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.shape({}),
  ]),
  invalid: PropTypes.bool,
  onClick: PropTypes.func,
  setFocus: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.shape()),
};

TimeInput.defaultProps = {
  value: "",
  label: "",
  error: "",
  containerClass: "",
  type: "text",
  labelClass: "",
  inputClass: "",
  fieldError: "",
  invalid: false,
  onClick: () => {},
  onChange: () => {},
  setFocus: false,
  options: [],
};

export default TimeInput;
