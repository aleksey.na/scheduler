import React, { useState } from "react";

// ** Thirs Party libraries
import { Range, getTrackBackground } from "react-range";
import PropTypes from "prop-types";

// ** Styles
import styles from "./timeRangeLine.module.scss";

const TimeRangeLine = () => {
  const STEP = 1;
  const MIN = 0;
  const MAX = 24;
  const [values, setValues] = useState([8, 16]);
  const [isDirty, setDirty] = useState(false);

  const selectedRangeColor = isDirty ? "#6b7daa" : "#b3c1e3";
  const duration = values[1] - values[0];

  return (
    <div>
      <Range
        values={values}
        step={STEP}
        min={MIN}
        max={MAX}
        onChange={(values) => {
          setDirty(true);
          setValues(values);
        }}
        renderTrack={({ props, children }) => (
          <div
            onMouseDown={props.onMouseDown}
            onTouchStart={props.onTouchStart}
            className={styles.trackContainer}
            style={{
              ...props.style,
            }}
          >
            <div
              className={`${styles.durationText} ${
                isDirty ? styles.active : ""
              }`}
            >
              <div>Duration:</div>
              <span>{duration} h</span>
            </div>
            <div
              ref={props.ref}
              className={styles.track}
              style={{
                background: getTrackBackground({
                  values,
                  colors: ["#dde6fc", selectedRangeColor, "#dde6fc"],
                  min: MIN,
                  max: MAX,
                }),
              }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props, isDragged }) => {
          const { key } = props;
          const time = values[key];
          const thumbValue =
            time < 12
              ? `${time === 0 ? 12 : time}:00 am`
              : `${time === 24 || time === 12 ? 12 : time - 12}:00 pm`;
          return (
            <div
              {...props}
              className={styles.thumb}
              style={{
                ...props.style,
              }}
            >
              <div
                className={`${styles.plate} ${isDirty ? styles.active : ""}`}
              >
                {thumbValue}
                <div className={styles.triangle} />
              </div>
            </div>
          );
        }}
      />
      <div className={styles.marks}>
        {new Array(MAX + 1).fill("").map((mark, index, arr) => {
          const outlined = index % 6 === 0 || index + 1 === arr.length;
          return (
            <div key={index} className={styles.markContainer}>
              <div
                className={`${styles.mark} ${outlined ? styles.outlined : ""}`}
              />
              <div className={styles.time}>{outlined && index}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

TimeRangeLine.propTypes = {
  state: PropTypes.shape({}),
  mainLabel: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date),
  ]),
  onChange: PropTypes.func,
  containerClass: PropTypes.string,
};

TimeRangeLine.defaultProps = {
  state: {},
  value: "",
  mainLabel: "Label",
  containerClass: "",
  invalid: false,
  onChange: () => {},
};

export default TimeRangeLine;
